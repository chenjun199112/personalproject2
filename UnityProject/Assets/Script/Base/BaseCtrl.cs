﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Manager;

public abstract class BaseCtrl
{
    public string uiName;
    public virtual void Initialize() { }

    public virtual void ShowUI(Action OnShowOK)
    {
        var panelMgr = ManagementCenter.GetManager<PanelManager>();
        panelMgr.CreatePanel(uiName, OnShowOK);
    }

    public virtual void CloseUI()
    {
        var panelMgr = ManagementCenter.GetManager<PanelManager>();
        panelMgr.DestoryPanel(uiName);
    }

    public virtual void OnDispose() { }

}
