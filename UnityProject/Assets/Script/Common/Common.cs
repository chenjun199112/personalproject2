﻿using UnityEngine;
using System;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

public static class Common
{
    //Function
    public static bool GetVec3ByString(string str, out Vector3 vec3)
    {
        vec3 = Vector3.zero;
        bool res = false;

        if (str.Length <= 0)
            return false;

        string[] tmpSValues = str.Trim(' ').Split(' ');
        if (tmpSValues.Length == 3)
        {
            float tmp_fX;
            float tmp_fY;
            float tmp_fZ;
            if (float.TryParse(tmpSValues[0], out tmp_fX) && float.TryParse(tmpSValues[1], out tmp_fY) &&
                float.TryParse(tmpSValues[2], out tmp_fZ))
            {
                res = true;
                vec3 = new Vector3(tmp_fX, tmp_fY, tmp_fZ);
            }

            return res;
        }
        return false;
    }

    public static Color HexToColor(int color)
    {
        string hex = color.ToString("X").PadLeft(6, (char)'0');

        int r = Convert.ToInt32(hex.Substring(0, 2), 16);
        int g = Convert.ToInt32(hex.Substring(2, 2), 16);
        int b = Convert.ToInt32(hex.Substring(4, 2), 16);
        return new Color(r / 255f, g / 255f, b / 255f);
    }

    public static Color Darken(Color color, float amount)
    {
        return new Color(color.r * amount, color.g * amount, color.b * amount);
    }


    public static string Base64Decode(string str)
    {
        byte[] bytes = Convert.FromBase64String(str);
        return Encoding.UTF8.GetString(bytes);
    }

    public static string Base64Encode(string str)
    {
        byte[] b = Encoding.UTF8.GetBytes(str);
        return Convert.ToBase64String(b);
    }

    public static string Md5(string toHash)
    {
        MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider();
        byte[] bytes = Encoding.UTF7.GetBytes(toHash);
        bytes = crypto.ComputeHash(bytes);
        StringBuilder sb = new StringBuilder();
        foreach (byte num in bytes)
        {
            sb.AppendFormat("{0:x2}", num);
        }
        return sb.ToString();        //32位
        //return sb.ToString().Substring(8, 16);        //16位
    }

    public static long ConvertDateTimeToLong(System.DateTime time)
    {
        System.DateTime startTime = System.TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
        return (long)(time - startTime).TotalSeconds;
    }

    public static IntPtr BytesToIntptr(byte[] bytes)
    {
        int size = bytes.Length;
        IntPtr buffer = Marshal.AllocHGlobal(size);
        try
        {
            Marshal.Copy(bytes, 0, buffer, size);
            return buffer;
        }
        finally
        {
            Marshal.FreeHGlobal(buffer);
        }
    }

    private static UTF8Encoding _utf8;
    public static UTF8Encoding GetUTF8Encoding()
    {
        if (_utf8 == null)
            _utf8 = new UTF8Encoding();
        return _utf8;
    }

    private static GameSettings _gameSettings;
    public static GameSettings gameSettings
    {
        get
        {
            if (_gameSettings == null)
            {
                _gameSettings = Utility.Util.LoadGameSettings();
            }
            return _gameSettings;
        }
    }
}
