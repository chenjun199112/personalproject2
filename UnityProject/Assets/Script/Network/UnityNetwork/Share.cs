﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Net.Sockets;

namespace UnityNetwork
{
    public static class Share
    {
        public static string IP { set; get; } = "127.0.0.1"; // 主机IP
        public static int Port { set; get; } = 18000;// 端口

        private static int _packageSize = -1;        // 数据包大小
        public static int PackageSize
        {
            get
            {
                if (_packageSize < 0)
                {
                    _packageSize = GetPackageSize();
                }
                return _packageSize;
            }
        }

        // 获取包大小
        public static int GetPackageSize()
        {
            Package p = new Package();
            byte[] b;
            Serialize(p, out b);
            return b.Length;
        }

        // 序列化数据包
        public static bool Serialize(object obj, out byte[] result)
        {
            bool ret = false;
            result = null;

            try
            {
                MemoryStream ms = new MemoryStream();
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                result = ms.ToArray();

                ret = true;
            }
            catch (Exception e)
            {
                ret = false;
                Debug.Log(e.Message);
            }

            return ret;
        }

        // 反序列化数据包
        public static bool Deserialize(byte[] data, out object result)
        {
            bool ret = false;
            result = new object();

            try
            {
                MemoryStream ms = new MemoryStream(data);
                BinaryFormatter bf = new BinaryFormatter();
                result = bf.Deserialize(ms);

                ret = true;
            }
            catch (Exception e)
            {
                ret = false;
                Debug.Log(e.Message);
            }

            return ret;
        }
    }

    // 数据包
    [Serializable]
    public struct Package
    {
        public int id;
        public Vector3Serializer pos;   // 人物位置
        public Vector3Serializer rot;   // 人物旋转角度
        public Vector3Serializer cameraRot;     // 摄像头旋转角度
        public Vector3Serializer rightHandRot;  // 右手旋转角度
        public bool isShooted;      // 判断是否有开枪
        public int hp;      // 血量
    }

    // 可序列化的Vector3
    [Serializable]
    public struct Vector3Serializer
    {
        public float x;
        public float y;
        public float z;

        public void Fill(Vector3 v3)
        {
            x = v3.x;
            y = v3.y;
            z = v3.z;
        }

        public Vector3 V3
        { get { return new Vector3(x, y, z); } }
    }

    // 储存已连接客户的结构体
    public struct Client
    {
        public TcpClient client;
        public byte[] buffer;               // 接收缓冲区
        public List<byte> pendingData;    // 还未处理的数据
    }
}
