﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net.Sockets;

namespace UnityNetwork
{
    public class ClientNetwork : MonoBehaviour
    {
#if Test
    private int Hp = -1;
#endif
        // 网络设置UI
        void OnGUI()
        {
            GUI.Box(new Rect(0, 0, 800, 60), "网络设置");

            if (_connected)
            {
                GUI.Label(new Rect(10, 25, 100, 25), "已连接服务端");
            }
            else
            {
                GUI.Label(new Rect(10, 25, 100, 25), "未连接服务端");
            }

            GUI.Label(new Rect(130, 25, 20, 25), "IP:");
            GUI.Label(new Rect(270, 25, 40, 25), "端口:");
            GUI.Label(new Rect(380, 25, 40, 25), "ID:");

            if (!_connected)
            {
                Share.IP = GUI.TextField(new Rect(150, 25, 100, 25), Share.IP, 100);
                Share.Port = System.Convert.ToInt32(GUI.TextField(new Rect(310, 25, 50, 25), Share.Port.ToString(), 100));
                _id = System.Convert.ToInt32(GUI.TextField(new Rect(420, 25, 100, 25), _id.ToString(), 100));
            }
            else
            {
                GUI.TextField(new Rect(150, 25, 100, 25), Share.IP, 100);
                GUI.TextField(new Rect(310, 25, 50, 25), Share.Port.ToString(), 100);
                GUI.TextField(new Rect(420, 25, 100, 25), _id.ToString(), 100);
            }

            if (!_connected)
            {
                if (GUI.Button(new Rect(660, 25, 100, 25), "连接至服务端"))
                {
                    ConnectServer();
                }
            }
            else
            {
                if (GUI.Button(new Rect(540, 25, 100, 25), "取消连接"))
                {
                    DisconnectServer();
                }
            }
#if Test
        if (GUI.Button(new Rect(340, 75, 100, 25), "发送消息"))
        {
            SendStatus(Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, false, 1);
        }
        
        if (GUI.Button(new Rect(480, 75, 100, 25), "收到消息"))
        {
            Package p;
            bool bSucc = NextPackage(out p);
            if (bSucc)
            {
                Hp = p.hp;
            }
                
        }
        GUI.Label(new Rect(620, 75, 100, 25), "HP:" + Hp);
#endif
        }

        // 客户端变量
        Client _server;
        public int _id = 1;           // 人物id
        private bool _connected = false;  // 判断是否已经连接服务端
        List<Package> _packages = new List<Package>();        // 数据包


        // 连接至服务端
        private void ConnectServer()
        {
            try
            {
                TcpClient tcpServer = new TcpClient();
                tcpServer.Connect(Share.IP, Share.Port);
                _server = new Client();
                _server.client = tcpServer;
                _server.buffer = new byte[tcpServer.ReceiveBufferSize];
                _server.pendingData = new List<byte>();

                // 异步接收服务端数据
                tcpServer.GetStream().BeginRead(
                    _server.buffer,
                    0,
                    tcpServer.ReceiveBufferSize,
                    HandleServerDataReceived,
                    _server);

                _connected = true;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        // 从服务端断开
        private void DisconnectServer()
        {
            try
            {
                lock (_server.client)
                {
                    _server.client.Client.Close();
                }

                // 清空数据包
                lock (_packages)
                {
                    _packages.Clear();
                }

                _connected = false;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        // 从服务端读取数据的回调函数
        private void HandleServerDataReceived(IAsyncResult iar)
        {
            if (_connected)
            {
                Client server = (Client)iar.AsyncState;
                NetworkStream ns = server.client.GetStream();
                int bytesRead = ns.EndRead(iar);

                // 连接中断
                if (bytesRead == 0)
                {
                    DisconnectServer();
                    return;
                }

                // 保存数据
                for (int i = 0; i < bytesRead; ++i)
                {
                    server.pendingData.Add(server.buffer[i]);
                }

                // 把数据解析成包
                while (server.pendingData.Count >= Share.PackageSize)
                {
                    byte[] bp = server.pendingData.GetRange(0, Share.PackageSize).ToArray();
                    server.pendingData.RemoveRange(0, Share.PackageSize);

                    // 把数据转换成包然后再储存包列表
                    object obj;
                    Share.Deserialize(bp, out obj);

                    lock (_packages)
                    {
#if Test
                    Debug.LogError("receive" + ((Package)obj).hp);
#endif
                        _packages.Add((Package)obj);
                    }
                }

                // 异步接收服务端数据
                server.client.GetStream().BeginRead(
                    server.buffer,
                    0,
                    server.client.ReceiveBufferSize,
                    HandleServerDataReceived,
                    server);
            }
        }

        // 发送自己的当前的状态包给服务端
        public void SendStatus(Vector3 pos, Vector3 rot, Vector3 cameraRot,
            Vector3 rightHandRot, bool isShooted, int hp)
        {
            try
            {
                if (_connected)
                {
                    Package p = new Package();
                    p.id = _id;
                    p.pos.Fill(pos);
                    p.rot.Fill(rot);
                    p.cameraRot.Fill(cameraRot);
                    p.rightHandRot.Fill(rightHandRot);
                    p.isShooted = isShooted;
                    p.hp = hp;

                    // 发送包到服务端
                    byte[] bp;
                    Share.Serialize(p, out bp);

                    lock (_server.client)
                    {
#if Test
                    Debug.LogError("send");
#endif
                        _server.client.GetStream().Write(bp, 0, Share.PackageSize);
                        _server.client.GetStream().Flush();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);

                // 断开服务端
                DisconnectServer();
            }
        }

        // 获取包
        public bool NextPackage(out Package p)
        {
            lock (_packages)
            {
                if (_packages.Count == 0)
                {
                    p = new Package();
                    return false;
                }

                p = _packages[0];
                _packages.RemoveAt(0);
            }

            return true;
        }
    }
}
