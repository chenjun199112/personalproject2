﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

namespace UnityNetwork
{
    public class ServerNetwork : MonoBehaviour
    {
        // 网络设置UI
        void OnGUI()
        {
            GUI.Box(new Rect(0, 0, 800, 60), "网络设置");


            GUI.Label(new Rect(10, 25, 200, 25), _isServer ? "已建立服务端：" + _clients.Count + "个连接" : "未建立服务端");


            GUI.Label(new Rect(270, 25, 20, 25), "IP:");
            GUI.Label(new Rect(420, 25, 40, 25), "端口:");

            if (!_isServer)
            {
                Share.IP = GUI.TextField(new Rect(310, 25, 100, 25), Share.IP, 100);
                Share.Port = System.Convert.ToInt32(GUI.TextField(new Rect(470, 25, 50, 25), Share.Port.ToString(), 100));
            }
            else
            {
                GUI.TextField(new Rect(310, 25, 100, 25), Share.IP, 100);
                GUI.TextField(new Rect(470, 25, 50, 25), Share.Port.ToString(), 100);
            }

            if (!_isServer)
            {
                if (GUI.Button(new Rect(540, 25, 100, 25), "开启服务端"))
                {
                    StartServer();
                }
            }
            else
            {
                if (GUI.Button(new Rect(540, 25, 100, 25), "关闭服务端"))
                {
                    StopServer();
                }
            }
        }


        // 服务端变量
        TcpListener _listener;
        private bool _isServer = false;   // 判断本程序是否已建立服务端

        // 客户列表
        List<Client> _clients = new List<Client>();

        // 开启服务端
        private void StartServer()
        {
            try
            {
                _listener = new TcpListener(IPAddress.Any, Share.Port);
                _listener.Start();
                _listener.BeginAcceptSocket(HandleAccepted, _listener);

                _isServer = true;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        // 停止停止服务端
        private void StopServer()
        {
            try
            {
                _listener.Stop();

                // 清空客户列表
                lock (_clients)
                {
                    foreach (Client c in _clients)
                    {
                        RemoveClient(c);
                    }
                    _clients.Clear();
                }

                _isServer = false;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        // 处理客户端连接的回调函数
        private void HandleAccepted(IAsyncResult iar)
        {
            if (_isServer)
            {
                TcpClient tcpClient = _listener.EndAcceptTcpClient(iar);
                Client client = new Client();
                client.client = tcpClient;
                client.buffer = new byte[tcpClient.ReceiveBufferSize];
                client.pendingData = new List<byte>();

                // 把客户加入到客户列表
                lock (_clients)
                {
                    AddClient(client);
                }

                // 开始异步接收从客户端收到的数据
                tcpClient.GetStream().BeginRead(
                        client.buffer,
                        0,
                        client.buffer.Length,
                        HandleClientDataReceived,
                        client);

                // 开始异步接收连接
                _listener.BeginAcceptSocket(HandleAccepted, _listener);
            }
        }

        // 从客户端接收数据的回调函数
        private void HandleClientDataReceived(IAsyncResult iar)
        {
            try
            {
                if (_isServer)
                {
                    Client client = (Client)iar.AsyncState;
                    NetworkStream ns = client.client.GetStream();
                    int bytesRead = ns.EndRead(iar);

                    // 连接中断
                    if (bytesRead == 0)
                    {
                        lock (_clients)
                        {
                            _clients.Remove(client);
                        }
                        return;
                    }

                    // 保存数据
                    for (int i = 0; i < bytesRead; ++i)
                    {
                        client.pendingData.Add(client.buffer[i]);
                    }

                    // 把数据解析成包
                    while (client.pendingData.Count >= Share.PackageSize)
                    {
                        byte[] bp = client.pendingData.GetRange(0, Share.PackageSize).ToArray();
                        client.pendingData.RemoveRange(0, Share.PackageSize);

#if Test
                    //Test
                    object obj;
                    Share.Deserialize(bp, out obj);
                    Package package = (Package)obj;
                    package.hp = 999;
                    Array.Clear(bp, 0, bp.Length);
                    Share.Serialize(package, out bp);
#endif

                        // 把数据包分发给所有客户
                        lock (_clients)
                        {
                            foreach (Client c in _clients)
                            {
                                c.client.GetStream().Write(bp, 0, Share.PackageSize);
                                c.client.GetStream().Flush();
                            }
                        }
                    }

                    // 开始异步接收从客户端收到的数据
                    client.client.GetStream().BeginRead(
                            client.buffer,
                            0,
                            client.buffer.Length,
                            HandleClientDataReceived,
                            client);
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        // 加入客户
        private void AddClient(Client c)
        {
            _clients.Add(c);
        }

        // 删除客户
        private void RemoveClient(Client c)
        {
            c.client.Client.Disconnect(false);
        }
    }
}
