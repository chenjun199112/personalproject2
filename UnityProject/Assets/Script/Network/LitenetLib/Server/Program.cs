﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();

            Console.ReadKey();
        }
    }

    class Manager
    {

        public Manager()
        {
            EventBasedNetListener listener = new EventBasedNetListener();
            NetManager server = new NetManager(listener);
            server.Start(4096 /* port */);
            Console.WriteLine("启动成功");
            listener.ConnectionRequestEvent += request =>
            {
                if (server.PeersCount < 10 /* max connections */)
                    request.AcceptIfKey("SomeConnectionKey");
                else
                    request.Reject();
            };

            listener.PeerConnectedEvent += peer =>
            {
                Console.WriteLine("We got connection: {0}", peer.EndPoint); // Show peer ip
                NetDataWriter writer = new NetDataWriter();                 // Create writer class
                writer.Put((ushort)(1001));
                writer.Put("Hello client!");                                // Put some string
                peer.Send(writer, DeliveryMethod.ReliableOrdered);             // Send with reliability

            };
            listener.NetworkReceiveEvent += Listener_NetworkReceiveEvent;
            while (!Console.KeyAvailable)
            {
                server.PollEvents();
                Thread.Sleep(15);
            }
            server.Stop();
        }

        private void Listener_NetworkReceiveEvent(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            //throw new NotImplementedException();
            ushort head = reader.GetUShort();
            string msg = reader.GetString(100);
            Console.WriteLine("recv: " + head + ":" + msg);
            NetDataWriter write = new NetDataWriter();
            write.Put((ushort)1002);
            write.Put("unity 我收到你发的消息了！！！");
            peer.Send(write, DeliveryMethod.ReliableOrdered);
        }
    }
}
