﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using System.Net;
using System.Net.Sockets;
using LiteNetLib.Utils;

public class LiteNetLibManager : MonoBehaviour, INetEventListener
{
    private static LiteNetLibManager instance;
    public static LiteNetLibManager Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        instance = this;
    }
    public void OnConnectionRequest(ConnectionRequest request)
    {
        Debug.Log("OnConnectionRequest " + request.ToString());
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
    {
        Debug.Log("OnNetworkError " + socketError.ToString());
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        Debug.Log("OnNetworkLatencyUpdate " + peer.ConnectionState + ": " + latency);
    }

    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        ushort head = reader.GetUShort();
        string msg = reader.GetString(100);
        Debug.Log("Recv " + head + ":" + msg);
        reader.Recycle();
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
    {
        //throw new System.NotImplementedException();
    }
    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("OnPeerConnected " + peer.EndPoint.ToString());
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("OnPeerDisconnected " + disconnectInfo.ToString());
    }

    LiteNetLib.NetManager client;
    void Start()
    {
        client = new LiteNetLib.NetManager(this);
        client.Start();
        client.Connect("127.0.0.1", 4096, "SomeConnectionKey");
    }
    public void SendMsg(ushort id, string msg)
    {
        if (client.ConnectedPeerList.Count < 1) return;
        NetDataWriter w = new NetDataWriter();
        w.Put(id);
        w.Put(msg);
        client.ConnectedPeerList[0].Send(w, DeliveryMethod.ReliableOrdered);
        Debug.Log("ConnectedPeerList 的长度 ：" + client.ConnectedPeerList.Count);

    }
    // Update is called once per frame
    private void FixedUpdate()
    {
        client.PollEvents();
    }

    void OnGUI()
    {
        ushort id = 0;
        string msg = "xxxx";
        if (Input.GetKeyDown(KeyCode.Alpha0))
            LiteNetLibManager.Instance.SendMsg(id, msg);
    }
}
