﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BlendShapeSystem : MonoBehaviour
{
    public void SetValue(int index,
                         float blendValue)
    {
        if (BlendMap == null)
        {
            return;
        }
        if (BlendMap.Count <= index || index < 0)
        {
            return;
        }
        for (int i = 0; i < BlendMap[index].Data.Count; i++)
        {
            BlendShapeMap.BlendShapeData data = BlendMap[index].Data[i];
            data.BlendRender.SetBlendShapeWeight(data.BlendIndex,
                                                 blendValue);
        }
    }

    public void SetValue(string blendName,
                         float blendValue)
    {
        if (BlendMap == null)
        {
            return;
        }
        int index = IndexOfName(blendName);
        if (index < 0)
        {
            return;
        }
        for (int i = 0; i < BlendMap[index].Data.Count; i++)
        {
            BlendShapeMap.BlendShapeData data = BlendMap[index].Data[i];
            data.BlendRender.SetBlendShapeWeight(data.BlendIndex,
                                                 blendValue);
        }
    }

    public int IndexOfName(string name)
    {
        if (BlendMap != null)
        {
            for (int i = 0; i < BlendMap.Count; i++)
            {
                if (BlendMap[i].Name == name)
                {
                    return i;
                }
            }
        }
        return -1;
    }

    public string NameOfIndex(int index)
    {
        if (BlendMap != null)
        {
            if (BlendMap.Count > index && index >= 0)
            {
                return BlendMap[index].Name;
            }
        }
        return null;
    }

    private static bool SetIncreaseWeightValue(SkinnedMeshRenderer render,
                                               int blendIndex,
                                               float deltaValue,
                                               float maxValue)
    {
        float weight = render.GetBlendShapeWeight(blendIndex);
        weight += deltaValue;
        if (weight > maxValue + 0.003f)
        {
            return false;
        }
        render.SetBlendShapeWeight(blendIndex,
                                   weight);
        return true;
    }

    private static bool SetDecreaseWeightValue(SkinnedMeshRenderer render,
                                               int blendIndex,
                                               float deltaValue)
    {
        float weight = render.GetBlendShapeWeight(blendIndex);
        if (weight <= 0)
        {
            return false;
        }
        weight -= deltaValue;
        render.SetBlendShapeWeight(blendIndex,
                                   weight);
        return true;
    }

    public bool IncreaseValue(int index,
                              float deltaValue,
                              float maxValue)
    {
        if (BlendMap == null)
        {
            return false;
        }
        if (BlendMap != null)
        {
            if (BlendMap.Count <= index || index < 0)
            {
                return false;
            }
        }
        for (int i = 0; i < BlendMap[index].Data.Count; i++)
        {
            SetIncreaseWeightValue(BlendMap[index].Data[i].BlendRender,
                                   BlendMap[index].Data[i].BlendIndex,
                                   deltaValue,
                                   maxValue);
        }
        return true;
    }

    public bool DecreaseValue(int index,
                              float deltaValue)

    {
        if (BlendMap == null)
        {
            return false;
        }
        if (BlendMap.Count <= index)
        {
            return false;
        }
        for (int i = 0; i < BlendMap[index].Data.Count; i++)
        {
            SetDecreaseWeightValue(BlendMap[index].Data[i].BlendRender,
                                   BlendMap[index].Data[i].BlendIndex,
                                   deltaValue);
        }
        return false;
    }

    public bool IncreaseValue(string blendName,
                              float deltaValue,
                              float maxValue)
    {
        if (BlendMap == null)
        {
            return false;
        }
        int index = IndexOfName(blendName);
        if (index < 0)
        {
            return false;
        }
        for (int i = 0; i < BlendMap[index].Data.Count; i++)
        {
            SetIncreaseWeightValue(BlendMap[index].Data[i].BlendRender,
                                   BlendMap[index].Data[i].BlendIndex,
                                   deltaValue,
                                   maxValue);
        }
        return true;
    }

    public bool DecreaseValue(string blendName,
                              float deltaValue)

    {
        if (BlendMap == null)
        {
            return false;
        }
        int index = IndexOfName(blendName);
        if (index < 0)
        {
            return false;
        }
        for (int i = 0; i < BlendMap[index].Data.Count; i++)
        {
            SetDecreaseWeightValue(BlendMap[index].Data[i].BlendRender,
                                   BlendMap[index].Data[i].BlendIndex,
                                   deltaValue);
        }
        return false;
    }

#region
    [Serializable]
    public class BlendShapeMap
    {
        [Serializable]
        public class BlendShapeData
        {
            public int BlendIndex;
            public SkinnedMeshRenderer BlendRender;
        }

#region
        //前提 如果blendshape 的名字相同，也就表示 设置一个的时候也要同时设置另一个blendshape
        public string Name;
        public List<BlendShapeData> Data;
#endregion
    }

    public List<SkinnedMeshRenderer> BlendRender;
    public List<BlendShapeMap> BlendMap;
#endregion
}