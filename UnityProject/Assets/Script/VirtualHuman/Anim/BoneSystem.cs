﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BoneSystem : MonoBehaviour
{
    private Quaternion _cachedValue;
    public List<BoneMap> Data;

    public int IndexOfName(string name)
    {
        if (Data != null)
        {
            for (int i = 0; i < Data.Count; i++)
            {
                if (string.CompareOrdinal(Data[i].Name,
                                          name) ==
                    0)
                {
                    return i;
                }
            }
        }
        return -1;
    }

    public string NameOfIndex(int index)
    {
        if (Data != null)
        {
            if (Data.Count > index && index >= 0)
            {
                return Data[index].Name;
            }
        }
        return null;
    }

    public Transform GetBone(string name)
    {
        if (Data != null)
        {
            for (int i = 0; i < Data.Count; i++)
            {
                if (string.CompareOrdinal(Data[i].Name,
                                          name) ==
                    0)
                {
                    return Data[i].BoneTrans;
                }
            }
        }
        return null;
    }

    public Transform GetBone(int index)
    {
        if (Data != null)
        {
            if (Data.Count > index && index >= 0)
            {
                return Data[index].BoneTrans;
            }
        }
        return null;
    }

    public void SetValue(int index,
                         float[] blendValue)
    {
        if (Data == null)
        {
            return;
        }
        if (Data.Count <= index || index < 0)
        {
            return;
        }
        _cachedValue.x = blendValue[0];
        _cachedValue.y = blendValue[1];
        _cachedValue.z = blendValue[2];
        _cachedValue.w = blendValue[3];
        Data[index].BoneTrans.localRotation = _cachedValue;
    }

    public void SetValue(string blendName,
                         float[] blendValue)
    {
        if (Data == null)
        {
            return;
        }
        int index = IndexOfName(blendName);
        if (index < 0)
        {
            return;
        }
        _cachedValue.x = blendValue[0];
        _cachedValue.y = blendValue[1];
        _cachedValue.z = blendValue[2];
        _cachedValue.w = blendValue[3];
        Data[index].BoneTrans.localRotation = _cachedValue;
    }

    [Serializable]
    public class BoneMap
    {
#region
        public string Name;
        public Transform BoneTrans;
#endregion
    }
}