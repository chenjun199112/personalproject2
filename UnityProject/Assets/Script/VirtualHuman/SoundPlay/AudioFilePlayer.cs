
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class AudioFilePlayer : ABSAudioOperator
{
    private readonly AudioType _audioType;
    private readonly string _filePath;
    private int _buffID;
    private AudioClipPlayer _clipPlayer;
    private UnityWebRequest _compressedAudioLoader;
    private AudioClip _extractAudioClip;
    private int _extractDataID;
    private float[] _samples;

    public bool ExtrackDataEnd { get; private set; }

    public override int TotalSampleCount
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            if (!ExtrackDataEnd)
            {
                return 0;
            }
            return _clipPlayer.TotalSampleCount;
        }
    }

    public override int PlayedSampleCount
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            if (!ExtrackDataEnd)
            {
                return 0;
            }
            return _clipPlayer.PlayedSampleCount;
        }
    }
    public override float TotalTime
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            if (!ExtrackDataEnd)
            {
                return 0;
            }
            return _clipPlayer.TotalTime;
        }
    }
    public override float PlayedTime
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            if (!ExtrackDataEnd)
            {
                return 0;
            }
            return _clipPlayer.PlayedTime;
        }
    }
    public override bool IsPlaying
    {
        get
        {
            if (Disposed)
            {
                return false;
            }
            if (!ExtrackDataEnd)
            {
                return true;
            }
            return _clipPlayer.IsPlaying;
        }
    }

    public float[] Samples
    {
        get { return _samples; }
    }

    protected override void OnPlayAction()
    {
        if (!ExtrackDataEnd)
        {
            return;
        }
        _clipPlayer.Play();
    }

    private IEnumerator ExtractDataEnumerator()
    {
        _compressedAudioLoader = UnityWebRequestMultimedia.GetAudioClip(_filePath,
                                                                        _audioType);
        yield return _compressedAudioLoader.SendWebRequest();
        if (_compressedAudioLoader.isNetworkError || _compressedAudioLoader.isHttpError)
        {
            ExtrackDataEnd = true;
            Debug.LogError("[ERROR] play audio file:" +
                           _compressedAudioLoader.error +
                           ":" +
                           _compressedAudioLoader.responseCode +
                           "\n" +
                           _filePath);
            yield break;
        }
        _extractAudioClip = DownloadHandlerAudioClip.GetContent(_compressedAudioLoader);
        _clipPlayer = new AudioClipPlayer(_extractAudioClip,
                                          false,
                                          null) {
                                                        PauseHosting = true
                                                };
        ExtrackDataEnd = true;
        if (IsAutoPlay)
        {
            Play();
        }
        if (extrackPcmf32BuffDelegate != null)
        {
            _buffID = VariableLengthArray<float>.Cache.AccurateAlloc(_extractAudioClip.samples,
                                                                 out _samples);
            _extractAudioClip.GetData(_samples,
                                      0);
            extrackPcmf32BuffDelegate(_samples);
        }
    }

#region
    public delegate void ExtrackPCMF32BuffDelegate(float[] samples);

    private readonly ExtrackPCMF32BuffDelegate extrackPcmf32BuffDelegate;

    public AudioFilePlayer(string filePath,
                           AudioType audioType,
                           bool autoPlay,
                           VoidDelegate endDelegate,
                           ExtrackPCMF32BuffDelegate extraDelegate = null) : base(autoPlay,
                                                                                  endDelegate)
    {
        if (!filePath.Contains("file:///"))
        {
            _filePath = "file:///" + filePath;
        }
        else
        {
            _filePath = filePath;
        }
        extrackPcmf32BuffDelegate = extraDelegate;
        _audioType = audioType;
    }

    protected override void OnPauseAction(bool state)
    {
        if (!ExtrackDataEnd)
        {
            Debug.LogWarning("[WARN]PlayUnityAudioFile In Extract");
            return;
        }
        _clipPlayer.Pause(state);
    }

    protected override void OnInitAction()
    {
        _extractDataID = Pump.Instance.SubscribeCoroutine(ExtractDataEnumerator());
    }

    protected override void OnUninitAction()
    {
        Pump.Instance.UnsubscribeCoroutine(_extractDataID);
        if (_extractAudioClip != null)
        {
            Object.Destroy(_extractAudioClip);
            _extractAudioClip = null;
        }
        if (_compressedAudioLoader != null)
        {
            _compressedAudioLoader.Dispose();
            _compressedAudioLoader = null;
        }
        if (_clipPlayer != null)
        {
            _clipPlayer.Dispose();
            _clipPlayer = null;
        }
        VariableLengthArray<float>.Cache.Free(_buffID);
        _buffID = -1;
    }
#endregion
}