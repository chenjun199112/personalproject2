using UnityEngine;

public class AudioClipPlayer : ABSAudioOperator
{
    private int _buffID;
    protected GameObject _cachedGo;
    private float[] _pcmF32Samples;

    public AudioClipPlayer(AudioClip innerClip,
                           bool autoPlay,
                           VoidDelegate endDelegate) : base(autoPlay,
                                                            endDelegate)
    {
        AudioInnerClip = innerClip;
        _cachedGo = new GameObject("[go]play_audioclip" + AudioInnerClip.name);
        Object.DontDestroyOnLoad(_cachedGo);
        AudioInnerSource = _cachedGo.AddComponent<AudioSource>();
        AudioInnerSource.clip = AudioInnerClip;
        AudioInnerSource.velocityUpdateMode = AudioVelocityUpdateMode.Dynamic;
        if (IsAutoPlay)
        {
            Play();
        }
        _buffID = VariableLengthArray<float>.Cache.AccurateAlloc(AudioInnerClip.samples,
                                                             out _pcmF32Samples);
        AudioInnerClip.GetData(_pcmF32Samples,
                               0);
    }

    public float[] PcmF32Samples
    {
        get { return _pcmF32Samples; }
    }

    public AudioClip AudioInnerClip { get; private set; }

    public AudioSource AudioInnerSource { get; private set; }

    public override int PlayedSampleCount
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            return AudioInnerSource.timeSamples;
        }
    }
    public override int TotalSampleCount
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            return AudioInnerSource.clip.samples;
        }
    }
    public override float TotalTime
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            return AudioInnerSource.clip.length;
        }
    }

    public override float PlayedTime
    {
        get
        {
            if (Disposed)
            {
                return 0;
            }
            return AudioInnerSource.time;
        }
    }

    public override bool IsPlaying
    {
        get
        {
            if (Disposed)
            {
                return false;
            }
            return AudioInnerSource.isPlaying;
        }
    }

    protected override void OnPlayAction()
    {
        InstanceID = AudioInnerSource.GetInstanceID();
        AudioInnerSource.Play();
    }

    protected override void OnUninitAction()
    {
        if (AudioInnerSource != null)
        {
            AudioInnerSource.clip = null;
        }
        AudioInnerSource = null;
        if (_cachedGo != null)
        {
            Object.Destroy(_cachedGo);
        }
        VariableLengthArray<float>.Cache.Free(_buffID);
        _buffID = -1;
        _pcmF32Samples = null;
        _cachedGo = null;
    }

    protected override void OnPauseAction(bool state)
    {
        if (state)
        {
            AudioInnerSource.Pause();
        }
        else
        {
            AudioInnerSource.UnPause();
        }
    }
}