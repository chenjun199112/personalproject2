using UnityEngine;

public abstract class ABSAudioOperator : DisposableScriptWithUpdate
{
    protected readonly VoidDelegate _endDelegate;

    protected ABSAudioOperator(bool autoPlay, VoidDelegate endDelegate)
    {
        _endDelegate = endDelegate;
        IsAutoPlay = autoPlay;
    }

    public abstract int TotalSampleCount { get; }
    public abstract int PlayedSampleCount { get; }
    public abstract float TotalTime { get; }
    public abstract float PlayedTime { get; }
    public abstract bool IsPlaying { get; }
    public bool IsAutoPlay { get; private set; }
    public int InstanceID { get; protected set; }
    public bool StartPlaying { get; private set; }

    protected override void OnUpdateAction()
    {
        if (StartPlaying)
        {
            if (PauseState)
            {
                return;
            }
            if (IsPlaying)
            {
                return;
            }
            _endDelegate?.Invoke();
            Dispose();
        }
    }

    public int Play()
    {
        if (Disposed)
        {
            return 0;
        }
        if (StartPlaying)
        {
            return 0;
        }
        StartPlaying = true;
        OnPlayAction();
        return InstanceID;
    }

    protected abstract void OnPlayAction();
}