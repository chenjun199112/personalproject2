﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BaseBlendCurveMaker : DisposableObject
{
    protected readonly BetterList<BlendMarker> _blendMarkList = new BetterList<BlendMarker>();

    private readonly float threshold = 400f; //暂定
    public Dictionary<string, AnimationCurve> BlendCurve = new Dictionary<string, AnimationCurve>();

    private readonly bool bLoose = true;
    protected override void OnDisposeManagedData() { _blendMarkList.Release(); }

    protected void ClearBlendMarkList()
    {
        for (int i = 0; i < _blendMarkList.size; i++)
        {
            ClassInstance<BlendMarker>.Cache.Free(_blendMarkList[i].InstanceID);
        }
        _blendMarkList.Clear();
    }

    protected void AddBlendMarker(CompactBlendData data,
                                  float timeStamp,
                                  float percentage,
                                  List<EmotionRatioData> emotions)
    {
        BlendMarker phonemeMarker;
        int id = ClassInstance<BlendMarker>.Cache.Alloc(out phonemeMarker);
        phonemeMarker.InstanceID = id;
        phonemeMarker.Percentage = percentage;
        phonemeMarker.KeyTimeStamp = timeStamp;
        phonemeMarker.CompactBlendConfData = data;
        phonemeMarker.Emotions = emotions;
        _blendMarkList.Add(phonemeMarker);
    }

    Dictionary<DefinedEmotionEnum , Dictionary<string, float>> blendableNeutralValues = new Dictionary<DefinedEmotionEnum, Dictionary<string, float>>();

    public void GenerateBlendableNeutralValues(ZHBlendDataMapping zhMapping)
    {
        for (int j = 0; j < Enum.GetNames(typeof(DefinedEmotionEnum)).Length - 1; j++)
        {
            blendableNeutralValues[(DefinedEmotionEnum)j] = new Dictionary<string, float>();
            CompactBlendData compactBlendConfData = zhMapping.Phoneme[ZH_PHONEME_ENUM.NONE.ToString()].Dic[(DefinedEmotionEnum)j];

            if (compactBlendConfData != null)
            {
                List<CompactBlendData.BlendConfData> blendShapes = compactBlendConfData.ConfList;
                for (int i = 0; i < blendShapes.Count; i++)
                {
                    if (!blendableNeutralValues[(DefinedEmotionEnum)j].ContainsKey(blendShapes[i].Name))
                    {
                        blendableNeutralValues[(DefinedEmotionEnum)j][blendShapes[i].Name] = blendShapes[i].ConfWeight;
                    }
                }
            }
        }

    }

    protected void DumpCurve()
    {
        for (int m = 0; m < _blendMarkList.size; m++)
        {
            if (_blendMarkList[m].CompactBlendConfData != null)
            {
                List<CompactBlendData.BlendConfData> blendShapes = _blendMarkList[m].CompactBlendConfData.ConfList;
                for (int i = 0; i < blendShapes.Count; i++)
                {
                    if (!BlendCurve.ContainsKey(blendShapes[i].Name))
                    {
                        BlendCurve[blendShapes[i].Name] = new AnimationCurve {postWrapMode = WrapMode.Once};
                    }
                }
            }
        }
        //////////////////////////////////////////////////
        for (int m = 0; m < _blendMarkList.size; m++)
        {
            BlendMarker marker = _blendMarkList[m];
            if (marker.CompactBlendConfData != null)
            {
                //遍历所有出现的blendershape
                foreach (KeyValuePair<string, AnimationCurve> animationCurve in BlendCurve)
                {
                    //枚举blendershape，在当前的marker有没有出现
                    CompactBlendData.BlendConfData confData =
                            marker.CompactBlendConfData.ConfDataOfName(animationCurve.Key);
                    if (confData == null ||
                        Math.Abs(confData.ConfWeight) < 0.0001f ||
                        Math.Abs(marker.Percentage) < 0.0001f)
                    {
                        //没有出现，那就需要在blendershape 对应的曲线里面 归零 关键帧
                        if (BlendMarkerContainsKey(marker, animationCurve.Key))
                        {
                            if (bLoose)
                            {
                                animationCurve.Value.AddKey(marker.KeyTimeStamp,
                                    GetBlendMarkerKeyNeutralValues(marker, animationCurve.Key));
                            }
                            else
                            {
                                animationCurve.Value.AddKey(new Keyframe(marker.KeyTimeStamp,
                                    GetBlendMarkerKeyNeutralValues(marker, animationCurve.Key),
                                    0,
                                    0));
                            }
                        }
                        else
                        {
                            if (bLoose)
                            {
                                animationCurve.Value.AddKey(marker.KeyTimeStamp,
                                    0);
                            }
                            else
                            {
                                animationCurve.Value.AddKey(new Keyframe(marker.KeyTimeStamp,
                                    0,
                                    0,
                                    0));
                            }
                        }
                        
                    }
                    else
                    {
                        //出现了，那就需要在blendershape 对应的曲线里面加上 高峰 关键帧
                        if (bLoose)
                        {
                            animationCurve.Value.AddKey(marker.KeyTimeStamp,
                                                        confData.ConfWeight * marker.Percentage);
                        }
                        else
                        {
                            animationCurve.Value.AddKey(new Keyframe(marker.KeyTimeStamp,
                                                                     confData.ConfWeight * marker.Percentage,
                                                                     0,
                                                                     0));
                        }
                    }
                }
            }
            else
            {
                

                //当前的marker位，强制表示 要所有的 blender曲线归零
                    foreach (KeyValuePair<string, AnimationCurve> animationCurve in BlendCurve)
                {
                    if (BlendMarkerContainsKey(marker, animationCurve.Key))
                    {
                        if (bLoose)
                        {
                            animationCurve.Value.AddKey(marker.KeyTimeStamp,
                                GetBlendMarkerKeyNeutralValues(marker, animationCurve.Key));
                        }
                        else
                        {
                            animationCurve.Value.AddKey(new Keyframe(marker.KeyTimeStamp,
                                GetBlendMarkerKeyNeutralValues(marker, animationCurve.Key),
                                0,
                                0));
                        }
                    }
                    else
                    {
                        if (bLoose)
                        {
                            animationCurve.Value.AddKey(marker.KeyTimeStamp,
                                0);
                        }
                        else
                        {
                            animationCurve.Value.AddKey(new Keyframe(marker.KeyTimeStamp,
                                0,
                                0,
                                0));
                        }
                    }
                    
                }
            }
        }
        //////////////////////////////////////////////////
        foreach (KeyValuePair<string, AnimationCurve> animationCurve in BlendCurve)
        {
            Keyframe[] keys = animationCurve.Value.keys;
            for (int i = 1; i < keys.Length; i++)
            {
                float y = keys[i].value - keys[i - 1].value;
                float x = keys[i].time - keys[i - 1].time;
                float thresholdValue = x * threshold;
                if (Mathf.Abs(x) > 0.00001f && Mathf.Abs(y) > 0.00001f && Mathf.Abs(y) > thresholdValue)
                {
                    keys[i].value = keys[i - 1].value + thresholdValue * y / Mathf.Abs(y);
                }
            }
            animationCurve.Value.keys = keys;
        }
    }

    public bool BlendMarkerContainsKey(BlendMarker marker, string Key)
    {
        bool result = false;
        if (marker != null && marker.Emotions != null)
        {
            for (int i = 0; i < marker.Emotions.Count; i++)
            {
                if (blendableNeutralValues[marker.Emotions[i].emotion].ContainsKey(Key))
                {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    public float GetBlendMarkerKeyNeutralValues(BlendMarker marker, string Key)
    {
        float value = 0.0f;
        if (!BlendMarkerContainsKey(marker, Key))
           return value;

        if (marker != null)
        {
            for (int i = 0; i < marker.Emotions.Count; i++)
            {
                if (blendableNeutralValues[marker.Emotions[i].emotion].ContainsKey(Key))
                {
                    value += blendableNeutralValues[marker.Emotions[i].emotion][Key] * marker.Emotions[i].ratio;
                }
            }
        }

        return value;
    }
}