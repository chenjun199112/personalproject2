﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ZHBlendDataMapping : DisposableObject
{
    public readonly Dictionary<string, string[]> CompactPhoneme = new Dictionary<string, string[]>();
    public readonly Dictionary<string, EmotionCompactBlendDataDic> Phoneme = new Dictionary<string, EmotionCompactBlendDataDic>();

    public readonly Dictionary<string, uint> PhonemeTime = new Dictionary<string, uint>();

    private readonly string phonemeRatioTxtPath = Application.streamingAssetsPath + "/phonemeratio.txt";

    public ZHBlendDataMapping(ZHEmotionCompactBSConfDataDic compactBlendData)
    {
        //
        Phoneme[ZH_PHONEME_ENUM.NONE.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_REST);
        //
        Phoneme[ZH_PHONEME_ENUM.b.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_BPM);
        Phoneme[ZH_PHONEME_ENUM.p.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_BPM);
        Phoneme[ZH_PHONEME_ENUM.m.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_BPM);
        //
        Phoneme[ZH_PHONEME_ENUM.f.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_FU);
        //
        Phoneme[ZH_PHONEME_ENUM.d.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_DTN);
        Phoneme[ZH_PHONEME_ENUM.t.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_DTN);
        Phoneme[ZH_PHONEME_ENUM.n.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_DTN);
        //
        Phoneme[ZH_PHONEME_ENUM.l.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_L);
        //
        Phoneme[ZH_PHONEME_ENUM.g.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_GKH);
        Phoneme[ZH_PHONEME_ENUM.k.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_GKH);
        Phoneme[ZH_PHONEME_ENUM.h.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_GKH);
        //
        Phoneme[ZH_PHONEME_ENUM.j.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_JQX);
        Phoneme[ZH_PHONEME_ENUM.q.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_JQX);
        Phoneme[ZH_PHONEME_ENUM.x.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_JQX);
        //
        Phoneme[ZH_PHONEME_ENUM.zh.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZHCHSH);
        Phoneme[ZH_PHONEME_ENUM.ch.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZHCHSH);
        Phoneme[ZH_PHONEME_ENUM.sh.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZHCHSH);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.ix.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZHCHSH);
        //
        Phoneme[ZH_PHONEME_ENUM.z.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZCS);
        Phoneme[ZH_PHONEME_ENUM.c.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZCS);
        Phoneme[ZH_PHONEME_ENUM.s.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZCS);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.iy.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ZCS);
        //
        Phoneme[ZH_PHONEME_ENUM.r.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_R);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.iz.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_R);

        ////////////////////////////////////////////////////////////
        /////ZH_SM_ENUM
        Phoneme[ZH_PHONEME_ENUM.a.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_A);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.aa.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_A);
        Phoneme[ZH_PHONEME_ENUM.ai.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_AI);
        Phoneme[ZH_PHONEME_ENUM.ao.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_AO);
        Phoneme[ZH_PHONEME_ENUM.an.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_AN);
        Phoneme[ZH_PHONEME_ENUM.ang.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ANG);
        Phoneme[ZH_PHONEME_ENUM.o.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_O);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.oo.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_O);
        Phoneme[ZH_PHONEME_ENUM.ou.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_OU);
        Phoneme[ZH_PHONEME_ENUM.ong.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_ONG);
        Phoneme[ZH_PHONEME_ENUM.e.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_E);
        Phoneme[ZH_PHONEME_ENUM.er.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_E);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.ee.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_E);
        Phoneme[ZH_PHONEME_ENUM.ei.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_EI);
        Phoneme[ZH_PHONEME_ENUM.en.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_EN);
        Phoneme[ZH_PHONEME_ENUM.eng.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_EN);
        Phoneme[ZH_PHONEME_ENUM.i.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_I);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.ii.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_I);
        Phoneme[ZH_PHONEME_ENUM.u.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_WU);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.uu.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_WU);
        Phoneme[ZH_PHONEME_ENUM.v.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_YU);
        Phoneme[ZH_SHENGMU_QINGHUA_ENUM.vv.ToString()] = compactBlendData.Get(DefinedZHCompactBlendEnum.PHONEME_YU);

        /////////////////////////////////////////////////////////////////////////////


        CompactPhoneme[ZH_PHONEME_ENUM.ia.ToString()] = new[] {
                                                                      ZH_PHONEME_ENUM.i.ToString(),
                                                                      ZH_PHONEME_ENUM.a.ToString()
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.iao.ToString()] = new[] {
                                                                      ZH_PHONEME_ENUM.i.ToString(),
                                                                      ZH_PHONEME_ENUM.ao.ToString(),
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.ian.ToString()] = new[] {
                                                                      ZH_PHONEME_ENUM.i.ToString(),
                                                                      ZH_PHONEME_ENUM.an.ToString(),
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.iang.ToString()] = new[] {
                                                                      ZH_PHONEME_ENUM.i.ToString(),
                                                                      ZH_PHONEME_ENUM.ang.ToString(),
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.iu.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.i.ToString(),
                                                                     ZH_PHONEME_ENUM.ou.ToString()
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.iong.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.i.ToString(),
                                                                     ZH_PHONEME_ENUM.ong.ToString(),
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.ie.ToString()] = new[] {
                                                                      ZH_PHONEME_ENUM.i.ToString(),
                                                                      ZH_PHONEME_ENUM.e.ToString()
                                                              };
       
        CompactPhoneme[ZH_PHONEME_ENUM.IN.ToString().ToLower()] = new[] {
                                                                     ZH_PHONEME_ENUM.i.ToString(),
                                                                     ZH_PHONEME_ENUM.en.ToString()
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.ing.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.i.ToString(),
                                                                     ZH_PHONEME_ENUM.eng.ToString()
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.ua.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.a.ToString()
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.uai.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.ai.ToString(),
                                                              };

        CompactPhoneme[ZH_PHONEME_ENUM.uan.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.an.ToString(),
                                                             };

        CompactPhoneme[ZH_PHONEME_ENUM.uang.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.ang.ToString(),
                                                             };

        CompactPhoneme[ZH_PHONEME_ENUM.uo.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.o.ToString()
                                                             };

        CompactPhoneme[ZH_PHONEME_ENUM.ui.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.ei.ToString()
                                                             };
       
        CompactPhoneme[ZH_PHONEME_ENUM.un.ToString()] = new[] {
                                                                      ZH_PHONEME_ENUM.u.ToString(),
                                                                      ZH_PHONEME_ENUM.en.ToString()
                                                              };

        CompactPhoneme[ZH_YUNMU_QINGHUA_ENUM.ueng.ToString()] = new[] {
                                                                     ZH_PHONEME_ENUM.u.ToString(),
                                                                     ZH_PHONEME_ENUM.eng.ToString()
                                                              };

        CompactPhoneme[ZH_YUNMU_QINGHUA_ENUM.van.ToString()] = new[] {
                                                                    ZH_PHONEME_ENUM.v.ToString(),
                                                                    ZH_PHONEME_ENUM.an.ToString()
                                                              };

        CompactPhoneme[ZH_YUNMU_QINGHUA_ENUM.ve.ToString()] = new[] {
                                                                    ZH_PHONEME_ENUM.v.ToString(),
                                                                    ZH_PHONEME_ENUM.e.ToString()
                                                              };

        CompactPhoneme[ZH_YUNMU_QINGHUA_ENUM.vn.ToString()] = new[] {
                                                                    ZH_PHONEME_ENUM.v.ToString(),
                                                                    ZH_PHONEME_ENUM.en.ToString()
                                                              };

        SetPhonemeTime();
    }

    private void SetPhonemeTime()
    {

        if (!File.Exists(phonemeRatioTxtPath))
        {
            return;
        }
        string[] lines = File.ReadAllLines(phonemeRatioTxtPath);

        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i];
            string[] temp = line.Split(new string[] { "'",":",","}, StringSplitOptions.RemoveEmptyEntries);
            if (temp.Length == 3)
            {
                uint time = 0;
                UInt32.TryParse(temp[2], out time);
                PhonemeTime[temp[1]] = time;
            }
        }
    }

    protected override void OnDisposeManagedData()
    {
        Phoneme.Clear();
        CompactPhoneme.Clear();
    }
}