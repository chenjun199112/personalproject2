﻿
using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class ZHWordCurveMaker : BaseBlendCurveMaker
{
    private const int EmphasizeTone1 = 3;
    private const int EmphasizeTone2 = 4;
    private readonly ZHBlendDataMapping _zhMapping;
    private BoolDelegate _endDelegate;
    private float[] _pcmF32Buff;
    private int _pcmF32BuffSize;
    private EmotionMarker _emotionMarker;

    private int _startTick;

    public ZHWordCurveMaker(ZHEmotionCompactBSConfDataDic bsConfData) { _zhMapping = new ZHBlendDataMapping(bsConfData); }

    public CharacterID ID { get; private set; }
    public string WordString { get; private set; }
    public BetterList<int> WordSplitIndexList { get; private set; }

    public bool IsInProcess { get; private set; }

    protected override void OnDisposeManagedData()
    {
        _zhMapping.Dispose();
        WordDataList.Release();
        BlendCurve.Clear();
        base.OnDisposeManagedData();
    }

    private void OnProcess()
    {
        _startTick = Environment.TickCount;
        Debug.Log("Start Align Process");
        bool state = Make();
        if (!state)
        {
            throw new Exception("[Error] ZHWordCurveMaker");
        }
    }

    private bool Make()
    {
        WordDataList.Clear();
        BlendCurve.Clear();
        WordSplitIndexList = null;
        BetterList<int> splitIndex;
        bool state = WordAlign.AlignPCMF32N(WordString,
                                            _pcmF32Buff,
                                            _pcmF32BuffSize,
                                            ref WordDataList,
                                            out splitIndex);
        if (!state)
        {
            Debug.LogError("[ERROR] ZHWordCurveMaker Make: \n" + WordString);
            return false;
        }
        WordSplitIndexList = splitIndex;
        DumpEmotion();
        DumpZHPhoneme();
        DumpEmphasizeToneList();
        DumpGestureList();
        return true;
    }

    private void OnProcessEnd(bool state)
    {
        Debug.Log("Align Process ms:" + (Environment.TickCount - _startTick));
        IsInProcess = false;
        if (_endDelegate != null)
        {
            _endDelegate(state);
        }
    }

#region
    public readonly BetterList<WordAlign.ZHWordData> EmphasizeTonePositionList = new BetterList<WordAlign.ZHWordData>();
    public readonly BetterList<WordAlign.ZHWordData> GesturePositionList = new BetterList<WordAlign.ZHWordData>();
    public BetterList<WordAlign.ZHWordData> WordDataList = new BetterList<WordAlign.ZHWordData>();
    private float _totalTime = 0;
    public bool Asyn(string wordString,
                     float[] pcmF32Buff,
                     int pcmF32BuffSize,
                     float totalTime,
                     BoolDelegate endDelegate)
    {
        if (IsInProcess)
        {
            return false;
        }
        IsInProcess = true;
        WordString = wordString;
        _endDelegate = endDelegate;
        _pcmF32Buff = pcmF32Buff;
        _pcmF32BuffSize = pcmF32BuffSize;
        _totalTime = totalTime;
        Pump.Instance.PushToThreadProcess(OnProcess,
                                          null,
                                          OnProcessEnd);
        return true;
    }

    private void DumpZHPhoneme()
    {
        ClearBlendMarkList();
        AddNoneBlendMarker(0f);

        for (int i = 0; i < WordDataList.size; i++)
        {
            float nextStart = 0;
            if (i != WordDataList.size - 1)
            {
                nextStart = WordDataList[i + 1].StartTime;
            }
            BetterList<BlendMarkerData> lsDumpMarker = ZHWordDumpMarker.DumpMarker(WordDataList[i],
                                                                                   nextStart,
                                                                                   _zhMapping,
                                                                                   _emotionMarker);
            for (int j = 0; j < lsDumpMarker.size; j++)
            {
                if (i + j == 0 && lsDumpMarker[j].timeStamp > Math.Min(_totalTime / 100, 0.1f))
                    AddBlendMarker(lsDumpMarker[j].data,
                        lsDumpMarker[j].timeStamp - Math.Min(_totalTime / 100, 0.1f),
                        lsDumpMarker[j].percentage * 0.01f,
                        lsDumpMarker[j].emotions);
                AddBlendMarker(lsDumpMarker[j].data,
                               lsDumpMarker[j].timeStamp,
                               lsDumpMarker[j].percentage,
                               lsDumpMarker[j].emotions);
            }
        }

        GenerateBlendableNeutralValues(_zhMapping);
        DumpCurve();
    }

    private void DumpEmotion()
    {
        _emotionMarker = new EmotionMarker(_totalTime);
    }

    private void AddNoneBlendMarker(float timeStamp)
    {
        CompactBlendData data = ZHWordDumpMarker.GetCompactBlendDataByTime(_emotionMarker, timeStamp, _zhMapping.Phoneme[ZH_PHONEME_ENUM.NONE.ToString()]);
        AddBlendMarker(data,
            timeStamp,
            0f,
            ZHWordDumpMarker.GetCurEmotion(_emotionMarker, timeStamp));
    }

    private void DumpGestureList()
    {
        GesturePositionList.Clear();
        for (int i = 0; i < WordDataList.size; i++)
        {
            int count = GesturePositionList.size;
            if (count > 0)
            {
                WordAlign.ZHWordData gesture = GesturePositionList[count - 1];
                System.Random random = new System.Random();
                int randomNumber = random.Next(5, 7);
                if (WordDataList[i].StartTime > gesture.StartTime + randomNumber)
                {
                    GesturePositionList.Add(WordDataList[i]);
                }
            }
            else
            {
                GesturePositionList.Add(WordDataList[i]);
            }
        }
    }

    private void DumpEmphasizeToneList()
    {
        EmphasizeTonePositionList.Clear();
        for (int i = 0; i < WordSplitIndexList.size; i++)
        {
            int setenceWordCount;
            int setenceStartIndex;
            if (i == 0)
            {
                setenceWordCount = WordSplitIndexList[0] + 1;
                setenceStartIndex = 0;
            }
            else
            {
                setenceWordCount = WordSplitIndexList[i] - WordSplitIndexList[i - 1];
                setenceStartIndex = WordSplitIndexList[i - 1] + 1;
            }
            if (setenceWordCount <= 1)
            {
                continue;
            }
            for (int j = 0; j < setenceWordCount - 2; j++)
            {
                if (WordDataList[j + setenceStartIndex].ShengDiao == EmphasizeTone1 ||
                    WordDataList[j + setenceStartIndex].ShengDiao == EmphasizeTone2)
                {
                    EmphasizeTonePositionList.Add(WordDataList[j + setenceStartIndex]);
                    j += 6;
                }
            }
        }
    }
#endregion
}