﻿using System.Collections.Generic;

public class BlendMarker : DisposableObject
{
    public int InstanceID { get; set; }
    public CompactBlendData CompactBlendConfData { get; set; }
    public float KeyTimeStamp { get; set; }
    public float Percentage { get; set; }
    public List<EmotionRatioData> Emotions { get; set; }

    protected override void OnDisposeManagedData() { Clear(); }

    public override void Clear() { CompactBlendConfData = null; }
}