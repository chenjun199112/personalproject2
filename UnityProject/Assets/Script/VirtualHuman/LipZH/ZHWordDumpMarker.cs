﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

public static class ZHWordDumpMarker
{
    private static readonly BetterList<EmotionCompactBlendDataDic> _ymBlendCache = new BetterList<EmotionCompactBlendDataDic>();
    private static EmotionCompactBlendDataDic _smBlendCache;
    public static float[] CompactPhonemeRate = new float[] {0f, 0.2f};
    private static readonly float _percentage = 0.8f;

    public static BetterList<BlendMarkerData> DumpMarker(WordAlign.ZHWordData zhWord,
                            float nextStartTime,
                            ZHBlendDataMapping zhMapping,
                            EmotionMarker emotionMarker)
    {
        BetterList<BlendMarkerData> result = new BetterList<BlendMarkerData>();

        _smBlendCache = null;
        _ymBlendCache.Clear();
        if (!string.IsNullOrEmpty(zhWord.ShengMu))
        {
            _smBlendCache = zhMapping.Phoneme[zhWord.ShengMu];
        }
        if (zhMapping.Phoneme.ContainsKey(zhWord.YunMu))
        {
            _ymBlendCache.Add(zhMapping.Phoneme[zhWord.YunMu]);
        }
        else
        {
            string[] yunmus = zhMapping.CompactPhoneme[zhWord.YunMu];
            for (int i = 0; i < yunmus.Length; i++)
            {
                _ymBlendCache.Add(zhMapping.Phoneme[yunmus[i]]);
            }
        }

        uint shengMuTime = 0;
        zhMapping.PhonemeTime.TryGetValue(zhWord.ShengMu, out shengMuTime);
        
        uint yunMuTime = 0;
        zhMapping.PhonemeTime.TryGetValue(zhWord.YunMu + zhWord.ShengDiao, out yunMuTime);


        float shengMuDuration = 0;
        float yunMuDuration = 0;
        if (shengMuTime + yunMuTime != 0)
        {
            shengMuDuration = (zhWord.EndTime - zhWord.StartTime) * shengMuTime / (shengMuTime + yunMuTime);
            yunMuDuration = (zhWord.EndTime - zhWord.StartTime) * shengMuTime / (shengMuTime + yunMuTime);
        }

        if (_smBlendCache != null)
        {
            float shengMuStart = zhWord.StartTime;
            CompactBlendData data = GetCompactBlendDataByTime(emotionMarker, shengMuStart, _smBlendCache);
            result.Add(new BlendMarkerData(data,
                shengMuStart,
                _percentage,
                GetCurEmotion(emotionMarker, shengMuStart)));
        }
        for (int i = 0; i < _ymBlendCache.size; i++)
        {
            float yunMuStart = zhWord.StartTime + shengMuDuration + yunMuDuration * CompactPhonemeRate[i];
            CompactBlendData data = GetCompactBlendDataByTime(emotionMarker, yunMuStart, _ymBlendCache[i]);
            result.Add(new BlendMarkerData(data,
                yunMuStart,
                _percentage,
                GetCurEmotion(emotionMarker, yunMuStart)));
        }

        if (Math.Abs(zhWord.EndTime - nextStartTime) > 0.1)
        {
            CompactBlendData data = GetCompactBlendDataByTime(emotionMarker, zhWord.EndTime + 0.1f, zhMapping.Phoneme[ZH_PHONEME_ENUM.NONE.ToString()]);
            result.Add(new BlendMarkerData(data,
                zhWord.EndTime + 0.1f,
                0,
                GetCurEmotion(emotionMarker, zhWord.EndTime + 0.1f)));
        }

        return result;
    }

    public static List<EmotionRatioData> GetCurEmotion(EmotionMarker emotionMarker, float time)
    {

        List<EmotionRatioData> erDatas = emotionMarker.GetEmotionRatioDataByCurTime(time);

        return erDatas;
    }

    //根据时间，获取表情数据
    public static CompactBlendData GetCompactBlendDataByTime(EmotionMarker emotionMarker, float time, EmotionCompactBlendDataDic dataDic)
    {
        CompactBlendData data;
        List<EmotionRatioData> erDatas = emotionMarker.GetEmotionRatioDataByCurTime(time);
        if (erDatas.Count == 1)
        {
            data = dataDic.Dic[erDatas[0].emotion] * erDatas[0].ratio;
        }
        else if (erDatas.Count == 2)
        {
            data = dataDic.Dic[erDatas[0].emotion] * erDatas[0].ratio + dataDic.Dic[erDatas[1].emotion] * erDatas[1].ratio;
        }
        else
        {
            data = new CompactBlendData();
            data.ConfList = new List<CompactBlendData.BlendConfData>();
        }
        return data;
    }
}

public struct BlendMarkerData
{
    public CompactBlendData data;
    public float timeStamp;
    public float percentage;
    public List<EmotionRatioData> emotions;

    public BlendMarkerData(CompactBlendData data,
        float timeStamp,
        float percentage,
        List<EmotionRatioData> emotions)
    {
        this.data = data;
        this.timeStamp = timeStamp;
        this.percentage = percentage;
        this.emotions = emotions;
    }
}
