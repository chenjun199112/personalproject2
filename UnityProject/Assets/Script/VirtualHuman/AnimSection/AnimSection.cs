﻿
using System.Collections.Generic;
using ProtoBuf;

[ProtoContract]
public class AnimSection : IClear
{
    public void Clear()
    {
        if (BlendShapeNames != null)
        {
            BlendShapeNames.Clear();
        }
        if (BlendShapeFrames != null)
        {
            for (int i = 0; i < BlendShapeFrames.Count; i++)
            {
                BlendShapeFrames[i].Clear();
            }
            BlendShapeFrames.Clear();
        }
        if (RotBoneNames != null)
        {
            RotBoneNames.Clear();
        }
        if (RotBoneFrames != null)
        {
            for (int i = 0; i < RotBoneFrames.Count; i++)
            {
                RotBoneFrames[i].Clear();
            }
            RotBoneFrames.Clear();
        }
    }

    public BlendShapeKeyFrames GetBlendShapeCurve(string blendName)
    {
        if (BlendShapeNames == null)
        {
            return null;
        }
        int index = BlendShapeNames.IndexOf(blendName);
        if (index >= 0)
        {
            return BlendShapeFrames[index];
        }
        return null;
    }

    public BoneRotKeyFrames GetBoneCurve(string boneName)
    {
        if (RotBoneNames == null)
        {
            return null;
        }
        int index = RotBoneNames.IndexOf(boneName);
        if (index >= 0)
        {
            return RotBoneFrames[index];
        }
        return null;
    }

    public static float H3(float x,
                           float sTime,
                           float sValue,
                           float sTo,
                           float eTime,
                           float eValue,
                           float eTi)
    {
        float dt = eTime - sTime;
        float dt2 = dt * dt;
        float dt3 = dt2 * dt;
        float tp1 = x - sTime;
        float tp2 = tp1 * tp1;
        float tp3 = tp2 * tp1;
        /////////////////////
        float dv = eValue - sValue;
        float p3 = ((dv - sTo * dt) / dt2 - (eTi - sTo) / (2 * dt)) / (dt3 / dt2 - 3 * dt2 / (2 * dt));
        float p2 = (eTi - sTo) / (2 * dt) - 3 * dt2 / (2 * dt) * p3;
        float v = 0 + sTo * tp1 + p2 * tp2 + p3 * tp3;
        return v + sValue;
    }

    [ProtoContract]
    public class Keyframe : IClear
    {
        public void Clear() { }

#region
        [ProtoMember(1)]
        public float Time { get; set; }
        [ProtoMember(2)]
        public float Value { get; set; }
        [ProtoMember(3)]
        public float InTangent { get; set; }
        [ProtoMember(4)]
        public float OutTangent { get; set; }
#endregion
    }

    [ProtoContract]
    public class BoneRotKeyFrames : IClear
    {
        public void Clear()
        {
            if (XFrames != null)
            {
                XFrames.Clear();
            }
            if (YFrames != null)
            {
                YFrames.Clear();
            }
            if (ZFrames != null)
            {
                ZFrames.Clear();
            }
            if (WFrames != null)
            {
                WFrames.Clear();
            }
        }

        public void ValueAt(float time,
                            ref float[] values)
        {
            values[0] = ValueAt(XFrames,
                                time,
                                0);
            values[1] = ValueAt(YFrames,
                                time,
                                0);
            values[2] = ValueAt(ZFrames,
                                time,
                                0);
            values[3] = ValueAt(WFrames,
                                time,
                                1);
        }

        private float ValueAt(IList<Keyframe> data,
                              float time,
                              float defaultValue)
        {
            if (data == null || data.Count == 0)
            {
                return defaultValue;
            }
            float max = data[data.Count - 1].Time;
            float min = data[0].Time;
            if (time >= max)
            {
                return defaultValue;
            }
            if (time <= min)
            {
                return defaultValue;
            }
            if (time - min < max - time)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].Time > time)
                    {
                        return H3(time,
                                  data[i - 1].Time,
                                  data[i - 1].Value,
                                  data[i - 1].OutTangent,
                                  data[i].Time,
                                  data[i].Value,
                                  data[i].InTangent);
                    }
                }
            }
            else
            {
                for (int i = data.Count - 1; i >= 0; i--)
                {
                    if (data[i].Time < time)
                    {
                        return H3(time,
                                  data[i].Time,
                                  data[i].Value,
                                  data[i].OutTangent,
                                  data[i + 1].Time,
                                  data[i + 1].Value,
                                  data[i + 1].InTangent);
                    }
                }
            }
            return defaultValue;
        }

#region
        [ProtoMember(1)]
        public IList<Keyframe> XFrames { get; set; }
        [ProtoMember(2)]
        public IList<Keyframe> YFrames { get; set; }
        [ProtoMember(3)]
        public IList<Keyframe> ZFrames { get; set; }
        [ProtoMember(4)]
        public IList<Keyframe> WFrames { get; set; }
#endregion
    }

    [ProtoContract]
    public class BlendShapeKeyFrames : IClear
    {
        [ProtoMember(1)]
        public IList<Keyframe> Frames;

        public void Clear()
        {
            if (Frames != null)
            {
                Frames.Clear();
            }
        }

        public float ValueAt(float time)
        {
            if (Frames == null || Frames.Count == 0)
            {
                return 0;
            }
            float max = Frames[Frames.Count - 1].Time;
            float min = Frames[0].Time;
            if (time >= max)
            {
                return 0;
            }
            if (time <= min)
            {
                return 0;
            }
            if (time - min < max - time)
            {
                for (int i = 0; i < Frames.Count; i++)
                {
                    if (Frames[i].Time >= time)
                    {
                        return H3(time,
                                  Frames[i - 1].Time,
                                  Frames[i - 1].Value,
                                  Frames[i - 1].OutTangent,
                                  Frames[i].Time,
                                  Frames[i].Value,
                                  Frames[i].InTangent);
                    }
                }
            }
            else
            {
                for (int i = Frames.Count - 1; i >= 0; i--)
                {
                    if (Frames[i].Time <= time)
                    {
                        return H3(time,
                                  Frames[i].Time,
                                  Frames[i].Value,
                                  Frames[i].OutTangent,
                                  Frames[i + 1].Time,
                                  Frames[i + 1].Value,
                                  Frames[i + 1].InTangent);
                    }
                }
            }
            return 0;
        }
    }

#region
    [ProtoMember(1)]
    public string Name;
    [ProtoMember(2)]
    public float Length;
    [ProtoMember(3)]
    public IList<string> BlendShapeNames;
    [ProtoMember(4)]
    public IList<BlendShapeKeyFrames> BlendShapeFrames;
    [ProtoMember(5)]
    public IList<string> RotBoneNames;
    [ProtoMember(6)]
    public IList<BoneRotKeyFrames> RotBoneFrames;
#endregion
}