﻿

public enum AnimSectionMaskEnum
{
    MuscleFace = 1 << 0,
    MuscleEyebrow = 1 << 1,
    MuscleEye = 1 << 2,
    MuscleTongue = 1 << 3,
    MuscleLowerJaw = 1 << 4,
    BoneEye = 1 << 5,
    BoneHead = 1 << 6,
    BoneUpBody = 1 << 7,
    BoneLeftGesture = 1 << 8,
    BoneRightGesture = 1 << 9,
}

public class AnimSectionMaskEnumOperation
{
    public static int GetAllMask()
    {
        int mask = GetBodyAllMask();
        mask |= GetHeadAllMask();
        return mask;
    }

    public static int GetBodyAllMask()
    {
        int mask = 0;
        mask |= (int) AnimSectionMaskEnum.BoneUpBody;
        mask |= (int) AnimSectionMaskEnum.BoneLeftGesture;
        mask |= (int) AnimSectionMaskEnum.BoneRightGesture;
        return mask;
    }

    public static int GetHeadAllMask()
    {
        int mask = GetHeadBoneMask();
        mask |= GetHeadFaceMask();
        mask |= GetHeadEyeMask();
        mask |= GetHeadEyebrowMask();
        return mask;
    }

    public static int GetHeadBoneMask()
    {
        int mask = 0;
        mask |= (int) AnimSectionMaskEnum.BoneHead;
        return mask;
    }

    public static int GetHeadEyeMask()
    {
        int mask = 0;
        mask |= (int) AnimSectionMaskEnum.MuscleEye;
        mask |= (int) AnimSectionMaskEnum.BoneEye;
        return mask;
    }

    public static int GetHeadEyebrowMask()
    {
        int mask = 0;
        mask |= (int) AnimSectionMaskEnum.MuscleEyebrow;
        return mask;
    }

    public static int GetHeadFaceMask()
    {
        int mask = 0;
        mask |= (int) AnimSectionMaskEnum.MuscleFace;
        mask |= (int) AnimSectionMaskEnum.MuscleTongue;
        mask |= (int) AnimSectionMaskEnum.MuscleLowerJaw;
        return mask;
    }
}