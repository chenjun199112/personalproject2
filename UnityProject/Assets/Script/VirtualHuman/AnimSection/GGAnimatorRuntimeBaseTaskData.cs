﻿public abstract class GGAnimatorRuntimeBaseTaskData : DisposableObject
{
    public bool InFadeIn { get; private set; }
    public bool InFadeOut { get; private set; }
    public bool IsEnd { get; private set; }
    public float FadeInDuration { get; private set; }
    public float StartFadeOutTime { get; private set; }
    public float FadeOutDuration { get; private set; }
    public float CurrentTime { get; private set; }
    public float Weight { get; private set; }

    protected override void OnDisposeManagedData() { Clear(); }

    public override void Clear()
    {
        InFadeIn = false;
        InFadeOut = false;
        IsEnd = false;
    }

    protected abstract float GeTaskLength();

    public bool Update(float currentTime)
    {
        if (IsEnd)
        {
            return false;
        }
        CurrentTime = currentTime;
        if (InFadeIn)
        {
            UpdateFadeInWeight();
            return true;
        }
        if (InFadeOut)
        {
            UpdateFadeOutWeight();
            return true;
        }
        UpdateNormal();
        return true;
    }

    public abstract float RetriveBlendShapeCurveValue(string name);

    public abstract void RetriveRotBoneCurveValue(string name,
                                                  float[] returnedValues);

    private void UpdateNormal()
    {
        float delta = CurrentTime - StartTime;
        if (delta >= GeTaskLength() && !Loop)
        {
            IsEnd = true;
        }
    }

    private void UpdateFadeInWeight()
    {
        float delta = CurrentTime - StartTime;
        if (delta >= GeTaskLength() && !Loop)
        {
            IsEnd = true;
            InFadeIn = false;
            Weight = 0;
            return;
        }
        float weight = delta / FadeInDuration;
        if (weight >= 1)
        {
            InFadeIn = false;
            Weight = 1;
            return;
        }
        Weight = weight;
    }

    private void UpdateFadeOutWeight()
    {
        float delta = CurrentTime - StartTime;
        if (delta >= GeTaskLength() && !Loop)
        {
            IsEnd = true;
            InFadeOut = false;
            Weight = 0;
            return;
        }
        float weight = (CurrentTime - StartFadeOutTime) / FadeOutDuration;
        if (weight >= 1)
        {
            IsEnd = true;
            InFadeOut = false;
            Weight = 0;
            return;
        }
        Weight = 1 - weight;
    }

    public void StartFadeIn(float duration)
    {
        if (!InFadeIn)
        {
            Weight = 0;
            InFadeIn = true;
            InFadeOut = false;
            FadeInDuration = duration;
        }
    }

    public void StartFadeOut(float duration,
                             float currentTime)
    {
        if (!InFadeOut)
        {
            Weight = 1;
            InFadeOut = true;
            InFadeIn = false;
            StartFadeOutTime = currentTime;
            FadeOutDuration = duration;
        }
    }

    public void StartNormal()
    {
        InFadeOut = false;
        InFadeIn = false;
        IsEnd = false;
        Weight = 1;
    }

#region
    public bool Loop;
    public string ID;
    public AnimSectionMaskEnum MaskEnum;
    public float StartTime;
#endregion
}