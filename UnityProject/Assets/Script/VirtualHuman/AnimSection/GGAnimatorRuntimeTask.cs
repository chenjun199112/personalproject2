﻿
using System.Collections.Generic;
using UnityEngine;

public class GGAnimatorRuntimeTask : DisposableObject
{
    private readonly float[] _cached1Float4;
    private readonly float[] _cached2Float4;
    private readonly LinkedList<GGAnimatorRuntimeTaskData> _dataList = new LinkedList<GGAnimatorRuntimeTaskData>();
    private GGAnimatorRuntimeTaskData _backData;
    private GGAnimatorRuntimeTaskData _frontData;
    public BlendShapeSystem BlendShapePlayer;
    public BoneSystem BonePlayer;
    public AnimSectionMaskEnum MaskEnum;
    public string[] MaskList;

    public GGAnimatorRuntimeTask()
    {
        _cached1Float4 = FixedLengthArray<float>.Cache4.Alloc();
        _cached2Float4 = FixedLengthArray<float>.Cache4.Alloc();
    }

    public float CurrentTime { get; private set; }

    protected override void OnDisposeManagedData() { Clear(); }

    public override void Clear()
    {
        if (_dataList.Count > 0)
        {
            while (true)
            {
                DataFree(_dataList.Last.Value);
                _dataList.RemoveLast();
                if (_dataList.Count == 0)
                {
                    break;
                }
            }
        }
        _dataList.Clear();
        if (_frontData != null)
        {
            DataFree(_frontData);
            _frontData = null;
        }
        if (_backData != null)
        {
            DataFree(_backData);
            _backData = null;
        }
        MaskList = null;
        BonePlayer = null;
        BlendShapePlayer = null;
        FixedLengthArray<float>.Cache4.Free(_cached1Float4);
        FixedLengthArray<float>.Cache4.Free(_cached2Float4);
    }

    private void DataFree(GGAnimatorRuntimeTaskData task) { ClassInstance<GGAnimatorRuntimeTaskData>.Cache.Free(task); }

    private GGAnimatorRuntimeTaskData Popup(float currentTime)
    {
        if (_dataList.Count > 0)
        {
            GGAnimatorRuntimeTaskData firstValue = _dataList.First.Value;
            if (firstValue != null)
            {
                if (currentTime >= firstValue.StartTime)
                {
                    _dataList.RemoveFirst();
                    return firstValue;
                }
            }
        }
        return null;
    }

    public void Update(float currentTime)
    {
        CurrentTime = currentTime;
        GGAnimatorRuntimeTaskData newData = Popup(CurrentTime);
        if (newData == null)
        {
            UpdateSwapBuff(CurrentTime);
            return;
        }
        SetNewData(newData);
        UpdateSwapBuff(CurrentTime);
    }

    private void UpdateSwapBuff(float currentTime)
    {
        BEGIN_UPDATE:
        if (_frontData == null && _backData == null)
        {
            return;
        }
        if (_frontData != null && _backData == null)
        {
            if (_frontData.IsEnd)
            {
                DataFree(_frontData);
                _frontData = null;
                goto BEGIN_UPDATE;
            }
            _frontData.Update(currentTime);
            UpdateFrontBackShow(_frontData);
        }
        else if (_frontData == null && _backData != null)
        {
            if (_backData.IsEnd)
            {
                DataFree(_backData);
                _backData = null;
                goto BEGIN_UPDATE;
            }
            _backData.Update(currentTime);
            UpdateFrontBackShow(_backData);
        }
        else
        {
            if (_frontData.IsEnd)
            {
                DataFree(_frontData);
                _frontData = null;
                goto BEGIN_UPDATE;
            }
            if (_backData.IsEnd)
            {
                DataFree(_backData);
                _backData = null;
                goto BEGIN_UPDATE;
            }
            _frontData.Update(currentTime);
            _backData.Update(currentTime);
            UpdateFrontBackShow(_frontData,
                                _backData);
        }
    }

    private void UpdateFrontBackShow(GGAnimatorRuntimeTaskData taskData)
    {
        if (MaskEnum < AnimSectionMaskEnum.BoneEye)
        {
            for (int i = 0; i < MaskList.Length; i++)
            {
                float bsValue = taskData.RetriveBlendShapeCurveValue(MaskList[i]);
                BlendShapePlayer.SetValue(MaskList[i],
                                          bsValue);
            }
        }
        else
        {
            for (int i = 0; i < MaskList.Length; i++)
            {
                taskData.RetriveRotBoneCurveValue(MaskList[i],
                                                  _cached1Float4);
                BonePlayer.SetValue(MaskList[i],
                                    _cached1Float4);
            }
        }
    }

    private void UpdateFrontBackShow(GGAnimatorRuntimeTaskData front,
                                     GGAnimatorRuntimeTaskData back)
    {
        if (MaskEnum < AnimSectionMaskEnum.BoneEye)
        {
            for (int i = 0; i < MaskList.Length; i++)
            {
                float bsValue1 = back.RetriveBlendShapeCurveValue(MaskList[i]);
                float bsValue2 = front.RetriveBlendShapeCurveValue(MaskList[i]);
                bsValue1 += bsValue2;
                BlendShapePlayer.SetValue(MaskList[i],
                                          bsValue1);
            }
        }
        else
        {
            for (int i = 0; i < MaskList.Length; i++)
            {
                back.RetriveRotBoneCurveValue(MaskList[i],
                                              _cached1Float4);
                front.RetriveRotBoneCurveValue(MaskList[i],
                                               _cached2Float4);
                _cached1Float4[0] += _cached2Float4[0];
                _cached1Float4[1] += _cached2Float4[1];
                _cached1Float4[2] += _cached2Float4[2];
                _cached1Float4[3] += _cached2Float4[3];
                BonePlayer.SetValue(MaskList[i],
                                    _cached1Float4);
            }
        }
    }

    private void SetNewData(GGAnimatorRuntimeTaskData newData)
    {
        BEGIN_SETNEWDATA:
        if (_frontData == null && _backData == null)
        {
            if (newData.IsValid())
            {
                _frontData = newData;
                _frontData.StartNormal();
            }
            else
            {
                DataFree(newData);
            }
        }
        else if (_frontData != null && _backData == null)
        {
            if (_frontData.IsEnd)
            {
                DataFree(_frontData);
                _frontData = null;
                goto BEGIN_SETNEWDATA;
            }
            _frontData.StartFadeOut(0.2f,
                                    CurrentTime);
            if (newData.IsValid())
            {
                _backData = newData;
                _backData.StartFadeIn(0.2f);
            }
            else
            {
                DataFree(newData);
            }
        }
        else if (_frontData == null && _backData != null)
        {
            if (_backData.IsEnd)
            {
                DataFree(_backData);
                _backData = null;
                goto BEGIN_SETNEWDATA;
            }
            _frontData = _backData;
            _frontData.StartFadeOut(0.2f,
                                    CurrentTime);
            if (newData.IsValid())
            {
                _backData = newData;
                _backData.StartFadeIn(0.2f);
            }
            else
            {
                _backData = null;
                DataFree(newData);
            }
        }
        else
        {
            if (_frontData.IsEnd)
            {
                DataFree(_frontData);
                _frontData = null;
                goto BEGIN_SETNEWDATA;
            }
            if (_backData.IsEnd)
            {
                DataFree(_backData);
                _backData = null;
                goto BEGIN_SETNEWDATA;
            }
            _frontData = _backData;
            _frontData.StartFadeOut(0.2f,
                                    CurrentTime);
            if (newData.IsValid())
            {
                _backData = newData;
                _backData.StartFadeIn(0.2f);
            }
            else
            {
                _backData = null;
                DataFree(newData);
            }
        }
    }

    public void PushTask(float currentTime,
                         float deltaTime,
                         string sectionName,
                         string id,
                         bool loop)
    {
        if (deltaTime < 0)
        {
            return;
        }
        while (true)
        {
            if (_dataList.Count > 0)
            {
                GGAnimatorRuntimeTaskData pop = _dataList.Last.Value;
                if (pop.StartTime >= currentTime + deltaTime)
                {
                    DataFree(pop);
                    _dataList.RemoveLast();
                    continue;
                }
            }
            GGAnimatorRuntimeTaskData taskData = ClassInstance<GGAnimatorRuntimeTaskData>.Cache.Alloc();
            taskData.StartTime = currentTime + deltaTime;
            taskData.SectionName = sectionName;
            taskData.MaskEnum = MaskEnum;
            taskData.ID = id;
            taskData.UseTaskCurve = false;
            taskData.Loop = loop;
            _dataList.AddLast(taskData);
            break;
        }
    }

    public void PushTask(float currentTime,
                         Dictionary<string, AnimationCurve> muscleCurves,
                         float length,
                         string id,
                         bool loop)
    {
        while (true)
        {
            if (_dataList.Count > 0)
            {
                GGAnimatorRuntimeTaskData pop = _dataList.Last.Value;
                if (pop.StartTime >= currentTime)
                {
                    DataFree(pop);
                    _dataList.RemoveLast();
                    continue;
                }
            }
            GGAnimatorRuntimeTaskData taskData = ClassInstance<GGAnimatorRuntimeTaskData>.Cache.Alloc();
            taskData.StartTime = currentTime;
            taskData.TaskCurves = muscleCurves;
            taskData.TaskCurveLength = length;
            taskData.MaskEnum = MaskEnum;
            taskData.ID = id;
            taskData.UseTaskCurve = true;
            taskData.Loop = loop;
            _dataList.AddLast(taskData);
            break;
        }
    }
}