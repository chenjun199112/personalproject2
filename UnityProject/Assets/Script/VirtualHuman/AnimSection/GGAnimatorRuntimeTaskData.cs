﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public class GGAnimatorRuntimeTaskData : GGAnimatorRuntimeBaseTaskData
{
    public override void Clear()
    {
        if (UseTaskCurve)
        {
            if (TaskCurves != null)
            {
                TaskCurves = null;
            }
        }
        else
        {
            ReleaseSection();
        }
        base.Clear();
    }

    public bool IsValid()
    {
        if (UseTaskCurve)
        {
            if (TaskCurves == null)
            {
                return false;
            }
            return true;
        }
        if (string.IsNullOrEmpty(SectionName))
        {
            return false;
        }
        return true;
    }

    protected override float GeTaskLength()
    {
        if (UseTaskCurve)
        {
            return TaskCurveLength;
        }
        AnimSection section = GetSection();
        if (section == null)
        {
            Debug.LogError("[ERROR]LowTaskData miss section:" + SectionName);
            return 0;
        }
        return section.Length;
    }

    public override float RetriveBlendShapeCurveValue(string name)
    {
        if (UseTaskCurve)
        {
            if (TaskCurves.ContainsKey(name))
            {
                return TaskCurves[name].Evaluate(CurrentTime - StartTime) * Weight;
            }
            return 0;
        }
        AnimSection section = GetSection();
        if (section == null)
        {
            Debug.LogError("[ERROR]LowTaskData miss section:" + SectionName);
            return 0;
        }
        AnimSection.BlendShapeKeyFrames blendShapeCurve = section.GetBlendShapeCurve(name);
        if (blendShapeCurve == null)
        {
            return 0;
        }
        float delta = (CurrentTime - StartTime) % GeTaskLength();
        return blendShapeCurve.ValueAt(delta) * Weight;
    }

    public override void RetriveRotBoneCurveValue(string name,
                                                  float[] returnedValues)
    {
        if (UseTaskCurve)
        {
            throw new NotImplementedException("[EXCEPTION]FaceMuscle RuntimeTaskData failed retrived values");
        }
        AnimSection section = GetSection();
        if (section == null)
        {
            Debug.LogError("[ERROR]LowTaskData miss section:" + SectionName);
            returnedValues[0] = 0;
            returnedValues[1] = 0;
            returnedValues[2] = 0;
            returnedValues[3] = 1;
            return;
        }
        AnimSection.BoneRotKeyFrames boneCurve = section.GetBoneCurve(name);
        if (boneCurve == null)
        {
            returnedValues[0] = 0;
            returnedValues[1] = 0;
            returnedValues[2] = 0;
            returnedValues[3] = 1;
            return;
        }
        float delta = (CurrentTime - StartTime) % GeTaskLength();
        boneCurve.ValueAt(delta,
                          ref returnedValues);
        for (int i = 0; i < returnedValues.Length; i++)
        {
            returnedValues[i] *= Weight;
        }
    }

    private AnimSection GetSection()
    {
        if (_sectionData == null)
        {
            _sectionInstanceID = SerializationData<AnimSection>.Instance.Get(ID,
                                                                             SectionName,
                                                                             out _sectionData);
        }
        return _sectionData;
    }

    private void ReleaseSection()
    {
        if (_sectionData != null)
        {
            SerializationData<AnimSection>.Instance.Release(_sectionInstanceID);
            _sectionInstanceID = -1;
            _sectionData = null;
        }
    }

#region
#region
    private int _sectionInstanceID;
    private AnimSection _sectionData;
    public string SectionName;
#endregion
#region
    public bool UseTaskCurve;
    public float TaskCurveLength;
    public Dictionary<string, AnimationCurve> TaskCurves;
#endregion
#endregion
}