﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GGAnimator : DisposableScriptWithUpdate
{
    private readonly BlendShapeSystem _blendShapeSystem;
    private readonly BoneSystem _boneSystem;
    private readonly BetterList<GGAnimatorRuntimeTask> _maskedCachedTask = new BetterList<GGAnimatorRuntimeTask>();
    private readonly int MaxAnimSectionMaskEnumCount;
    private AnimSectionLayoutMask _layoutMask;

    public string ID { get; private set; }

    public float Speed { get; set; }
    public float PlayedTime { get; private set; }

    protected override void OnUpdateAction()
    {
        PlayedTime += Time.deltaTime;
        for (int i = 0; i < MaxAnimSectionMaskEnumCount; i++)
        {
            string[] maskNames = _layoutMask.GetLayoutByIndex(i);
            if (maskNames == null || maskNames.Length == 0)
            {
                continue;
            }
            _maskedCachedTask[i].Update(PlayedTime);
        }
    }

    protected override void OnUninitAction()
    {
        for (int i = 0; i < MaxAnimSectionMaskEnumCount; i++)
        {
            ClassInstance<GGAnimatorRuntimeTask>.Cache.Free(_maskedCachedTask[i]);
            _maskedCachedTask[i].Dispose();
        }
        _maskedCachedTask.Clear();
        _maskedCachedTask.Release();
        _layoutMask = null;
    }

    private void AddTask(float deltaTime,
                         string sectionName,
                         int sectionMask,
                         int replacedMask,
                         bool loop)
    {
        int combineMask = sectionMask & replacedMask;
        for (int i = 0; i < MaxAnimSectionMaskEnumCount; i++)
        {
            if ((combineMask & (1 << i)) > 0)
            {
                _maskedCachedTask[i].PushTask(PlayedTime,
                                              deltaTime,
                                              sectionName,
                                              ID,
                                              loop);
            }
            else if ((replacedMask & (1 << i)) > 0)
            {
                _maskedCachedTask[i].PushTask(PlayedTime,
                                              deltaTime,
                                              null,
                                              ID,
                                              loop);
            }
        }
    }

    private void AddMusleFaceTask(Dictionary<string, AnimationCurve> curves,
                                  float length,
                                  bool loop)
    {
        int combineMask = AnimSectionMaskEnumOperation.GetHeadFaceMask();
        for (int i = 0; i < MaxAnimSectionMaskEnumCount; i++)
        {
            if ((combineMask & (1 << i)) > 0)
            {
                _maskedCachedTask[i].PushTask(PlayedTime,
                                              curves,
                                              length,
                                              ID,
                                              loop);
            }
        }
    }

#region
    public GGAnimator(string id,
                      AnimSectionLayoutMask layout,
                      BoneSystem boneSystem,
                      BlendShapeSystem blendShapeSystem)
    {
        ID = id;
        Speed = 1;
        PlayedTime = 0;
        _layoutMask = layout;
        _boneSystem = boneSystem;
        _blendShapeSystem = blendShapeSystem;
        MaxAnimSectionMaskEnumCount = Enum.GetNames(typeof(AnimSectionMaskEnum)).Length;
        for (int i = 0; i < MaxAnimSectionMaskEnumCount; i++)
        {
            _maskedCachedTask.Add(ClassInstance<GGAnimatorRuntimeTask>.Cache.Alloc());
            _maskedCachedTask[i].MaskEnum = (AnimSectionMaskEnum) (1 << i);
            _maskedCachedTask[i].MaskList = _layoutMask.GetLayoutByIndex(i);
            _maskedCachedTask[i].BonePlayer = _boneSystem;
            _maskedCachedTask[i].BlendShapePlayer = _blendShapeSystem;
        }
    }

    public void PlaySection(string sectionName,
                            int sectionMask,
                            int replacedMask,
                            bool loop)
    {
        AddTask(0,
                sectionName,
                sectionMask,
                replacedMask,
                loop);
    }

    public void PlayMusleFaceSection(Dictionary<string, AnimationCurve> curves,
                                     float length,
                                     bool loop)
    {
        AddMusleFaceTask(curves,
                         length,
                         loop);
    }

    public void PlayTask(string taskName)
    {
        AnimSectionTask sectionTask;
        int id = SerializationData<AnimSectionTask>.Instance.Get(ID,
                                                                 taskName,
                                                                 out sectionTask);
        int sectionCount = sectionTask.Section.Count;
        for (int index = 0; index < sectionCount; index++)
        {
            AddTask(sectionTask.StartTimes[index],
                    sectionTask.Section[index],
                    sectionTask.SectionMask[index],
                    sectionTask.SectionReplaceMask[index],
                    sectionTask.SectionLoop[index]);
        }
        SerializationData<AnimSectionTask>.Instance.Release(id);
    }

    public void PlayTask(AnimSectionTask task)
    {
        int sectionCount = task.Section.Count;
        for (int index = 0; index < sectionCount; index++)
        {
            AddTask(task.StartTimes[index],
                    task.Section[index],
                    task.SectionMask[index],
                    task.SectionReplaceMask[index],
                    task.SectionLoop[index]);
        }
    }
#endregion
}