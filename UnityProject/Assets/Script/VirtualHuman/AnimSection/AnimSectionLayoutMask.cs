﻿
using UnityEngine;

public class AnimSectionLayoutMask : ScriptableObject
{
    public string[] GetLayoutByIndex(int index)
    {
        switch (index)
        {
            case 0:
                return Layout1MuscleFace;
            case 1:
                return Layout2MuscleEyebrow;
            case 2:
                return Layout3MuscleEye;
            case 3:
                return Layout4MuscleTongue;
            case 4:
                return Layout5MuscleLowerJaw;
            case 5:
                return Layout6BoneEye;
            case 6:
                return Layout7BoneHead;
            case 7:
                return Layout8BoneUpBody;
            case 8:
                return Layout9BoneLeftGesture;
            default:
                return Layout10BoneRightGesture;
        }
    }

#region
    public string[] Layout1MuscleFace;
    public string[] Layout2MuscleEyebrow;
    public string[] Layout3MuscleEye;
    public string[] Layout4MuscleTongue;
    public string[] Layout5MuscleLowerJaw;
    public string[] Layout6BoneEye;
    public string[] Layout7BoneHead;
    public string[] Layout8BoneUpBody;
    public string[] Layout9BoneLeftGesture;
    public string[] Layout10BoneRightGesture;
#endregion
}