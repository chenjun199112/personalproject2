﻿// ************************************************
//                     weiya_gao  
//                     2019-03.14
// ************************************************

#region
using System.Collections.Generic;
#endregion

public class AnimSectionTask : DisposableObject
{
    public override void Clear()
    {
        Length = 0;
        if (StartTimes != null)
        {
            StartTimes.Clear();
        }
        if (Section != null)
        {
            Section.Clear();
        }
        if (SectionMask != null)
        {
            SectionMask.Clear();
        }
        if (SectionReplaceMask != null)
        {
            SectionReplaceMask.Clear();
        }
        if (SectionLoop != null)
        {
            SectionLoop.Clear();
        }
    }

    protected override void OnDisposeManagedData() { Clear(); }

    public float AddSection(float startTime,
                            string sectionName,
                            int sectionMask,
                            int sectionReplaceMask,
                            bool loop,
                            string cID)
    {
        if (StartTimes == null)
        {
            StartTimes = new List<float>();
            Section = new List<string>();
            SectionMask = new List<int>();
            SectionReplaceMask = new List<int>();
            SectionLoop = new List<bool>();
        }
        StartTimes.Add(startTime);
        Section.Add(sectionName);
        SectionMask.Add(sectionMask);
        SectionReplaceMask.Add(sectionReplaceMask);
        SectionLoop.Add(loop);
        AnimSection section;
        int id = SerializationData<AnimSection>.Instance.Get(cID,
                                                             sectionName,
                                                             out section);
        if (startTime + section.Length > Length)
        {
            Length = startTime + section.Length;
        }
        SerializationData<AnimSection>.Instance.Release(id);
        return section.Length;
    }

#region
    public string Name;
    public float Length;
    public List<float> StartTimes;
    public List<string> Section;
    public List<int> SectionMask;
    public List<int> SectionReplaceMask;
    public List<bool> SectionLoop;
#endregion
}