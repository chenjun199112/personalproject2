// ************************************************           
//                     自己实现
// ************************************************

#region
using System;
using System.IO;
using System.Net;
using System.Text;
using LitJson;
using UnityEngine;
#endregion

namespace Xunfei
{
    public class XFAIUIWebClient : XFBaseWebClient
    {
        public enum AIUI_AUE_TYPE
        {
            raw,
            speex,
            speex_wb
        }

        public enum AIUI_DATA_TYPE
        {
            text,
            audio
        }

        public enum AIUI_RESULT_LEVEL
        {
            plain,
            complete
        }

        private const string AIUI_URL = "http://openapi.xfyun.cn/v2/aiui";
        private readonly string _aiuiAuthID;
        private string _audioSampleRate;

        public XFAIUIWebClient() : base("59d8235368cd47b08cb8523086da46b2")
        {
            _aiuiAuthID = EncryptWithMD5(SystemInfo.deviceUniqueIdentifier);
        }

        public XF_AIUI_DATA_RootObject ResultObject
        {
            get
            {
                if (Disposed)
                {
                    return null;
                }
                if (string.IsNullOrEmpty(ResultString))
                {
                    return null;
                }
                return JsonMapper.ToObject<XF_AIUI_DATA_RootObject>(ResultString);
            }
        }

        private void BuildRecognizeAudioHeader()
        {
            string param = "{\"aue\":\"" +
                           AIUI_AUE_TYPE.raw +
                           "\"," +
                           "\"result_level\":\"" +
                           AIUI_RESULT_LEVEL.complete +
                           "\"," +
                           "\"sample_rate\":\"" +
                           _audioSampleRate +
                           "\"," +
                           "\"auth_id\":\"" +
                           _aiuiAuthID +
                           "\"," +
                           "\"data_type\":\"" +
                           AIUI_DATA_TYPE.audio +
                           "\"," +
                           "\"scene\":\"main\"}";
            MakeHttpheader(param);
        }

        private void BuildRecognizeTextHeader()
        {
            string param = "{\"result_level\":\"" +
                           AIUI_RESULT_LEVEL.complete +
                           "\"," +
                           "\"auth_id\":\"" +
                           _aiuiAuthID +
                           "\"," +
                           "\"data_type\":\"" +
                           AIUI_DATA_TYPE.text +
                           "\"," +
                           "\"scene\":\"main\"}";
            MakeHttpheader(param);
        }

        protected bool PostReturn()
        {
            bool hasError;
            try
            {
                hasError = false;
                _webResponse = (HttpWebResponse) _webRequest.GetResponse();
                using (Stream responseStream = _webResponse.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream,
                                                                            Encoding.UTF8))
                        {
                            ResultString = streamReader.ReadToEnd();
                        }
                    }
                }
                //                Debug.Log("[AIUI POST END]:" + ResultJsonString);
                End();
            }
            catch (Exception ex)
            {
                Debug.LogError("[AIUI POST EXCEPTION]: " + ex);
                hasError = true;
            }
            return !hasError;
        }

        public bool PostData(byte[] data,
                             int offset,
                             int length,
                             int sampleRate)
        {
            if (Disposed)
            {
                return false;
            }
            _webRequest = null;
            _webResponse = null;
            IsTeminated = false;
            _audioSampleRate = sampleRate.ToString();
            BuildRecognizeAudioHeader();
            CreateHttpPost(AIUI_URL,
                           data,
                           offset,
                           length,
                           "text/plain");
            return PostReturn();
        }

        public bool PostData(string text)
        {
            if (Disposed)
            {
                return false;
            }
            _webRequest = null;
            _webResponse = null;
            IsTeminated = false;
            BuildRecognizeTextHeader();
            byte[] body = Encoding.UTF8.GetBytes(text);
            CreateHttpPost(AIUI_URL,
                           body,
                           0,
                           body.Length,
                           "text/plain");
            return PostReturn();
        }
    }
}