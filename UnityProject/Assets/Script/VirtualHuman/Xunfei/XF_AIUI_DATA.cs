using System.Collections.Generic;

public class XF_AIUI_DATA_Cw
{
    public int sc { get; set; }
    public string w { get; set; }
}

public class XF_AIUI_DATA_W
{
    public int bg { get; set; }
    public List<XF_AIUI_DATA_Cw> cw { get; set; }
}

public class XF_AIUI_DATA_Text
{
    public int bg { get; set; }
    public int ed { get; set; }
    public bool ls { get; set; }
    public string pgs { get; set; }
    public int sn { get; set; }
    public List<XF_AIUI_DATA_W> ws { get; set; }
}

public class XF_AIUI_DATA_Question
{
    public string question { get; set; }
    public string question_ws { get; set; }
}

public class XF_AIUI_DATA_Answer
{
    public string answerType { get; set; }
    public string emotion { get; set; }
    public XF_AIUI_DATA_Question question { get; set; }
    public string text { get; set; }
    public string topicID { get; set; }
    public string type { get; set; }
}

public class XF_AIUI_DATA_Intent
{
    public XF_AIUI_DATA_Answer answer { get; set; }
    public int no_nlu_result { get; set; }
    public string operation { get; set; }
    public int rc { get; set; }
    public string service { get; set; }
    public string sid { get; set; }
    public int status { get; set; }
    public string text { get; set; }
    public string uuid { get; set; }
}

public class XF_AIUI_DATA_Datum
{
    public string sub { get; set; }
    public string auth_id { get; set; }
    public XF_AIUI_DATA_Text text { get; set; }
    public int result_id { get; set; }
    public XF_AIUI_DATA_Intent intent { get; set; }
}

public class XF_AIUI_DATA_RootObject
{
    public string code { get; set; }
    public List<XF_AIUI_DATA_Datum> data { get; set; }
    public string desc { get; set; }
    public string sid { get; set; }
}