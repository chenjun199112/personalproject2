// ************************************************
//                     自己实现 
// ************************************************

using System.Text;

internal class XFChatSub : DisposableObject
{
    private StringBuilder _stringBuild;
    private int _stringID;

    public XFChatSub(float[] f32nData,
                     int validSize,
                     IntIntStringDelegate endDelegate)
    {
        Floats = f32nData;
        ValidSize = validSize;
        EndDelegate = endDelegate;
        Init();
    }

    public XFChatSub(byte[] s16Data,
                     int validSize,
                     IntIntStringDelegate endDelegate)
    {
        Bytes = s16Data;
        ValidSize = validSize;
        EndDelegate = endDelegate;
        Init();
    }

    public IntIntStringDelegate EndDelegate { get; }

    public int ValidSize { get; }

    public float[] Floats { get; private set; }
    public byte[] Bytes { get; }
    public int InstanceID { get; private set; }
    public bool IsEnd { get; private set; }
    public XFChat.State ChatState { get; set; }

    public int GetShortByteSize
    {
        get
        {
            if (Floats == null)
            {
                if (Bytes == null)
                {
                    return 0;
                }
                {
                    return ValidSize;
                }
            }
            return ValidSize * 2;
        }
    }

    private void Init()
    {
        IsEnd = false;
        _stringID = BasicClassInstance<StringBuilder>.Cache.Alloc(out _stringBuild);
        _stringBuild.Capacity = 500;
        InstanceID = GetHashCode();
    }

    public string GetResult()
    {
        if (ChatState == XFChat.State.XFTTSSuccess ||
            ChatState == XFChat.State.HardcodeSayHello ||
            ChatState == XFChat.State.XFAIUISuccess)
        {
            return _stringBuild.ToString();
        }
        return string.Empty;
    }

    protected override void OnDisposeManagedData()
    {
        _stringBuild.Length = 0;
        if (_stringBuild != null)
        {
            BasicClassInstance<StringBuilder>.Cache.Free(_stringID);
            _stringID = -1;
            _stringBuild = null;
        }
        Floats = null;
        InstanceID = 0;
    }

    public void Finish() { IsEnd = true; }

    public void SetResult(string result)
    {
        _stringBuild.Length = 0;
        if (ChatState == XFChat.State.XFAIUISuccess)
        {
            if (!string.IsNullOrEmpty(result) && (result.Contains("您好") || result.Contains("你好")))
            {
                ChatState = XFChat.State.HardcodeSayHello;
                _stringBuild.Append(result);
                return;
            }
        }
        if (result != null)
        {
            _stringBuild.Append(result);
        }
    }
}