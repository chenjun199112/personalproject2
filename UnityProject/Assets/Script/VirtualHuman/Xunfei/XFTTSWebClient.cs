// ************************************************
//                     自己实现 
// ************************************************

#region
using System;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
#endregion

namespace Xunfei
{
    public class XFTTSWebClient : XFBaseWebClient
    {
        public enum TTS_AUE_TYPE
        {
            raw,
            lame
        }

        public enum TTS_ENGINE_TYPE
        {
            aisound,
            intp65,
            intp65_en,
            mtts
        }

        public enum TTS_TEXT_TYPE
        {
            text
        }

        public enum TTS_VOICE_NAME
        {
            xiaoyan,
            xiaofeng
        }

        private const int MAX_BUFF_SIZE = 1024;

        private const string TTS_AUF_16000 = "audio/L16;rate=16000";
        private const string TTS_URL = "http://api.xfyun.cn/v1/service/v1/tts";

        private static int _tts_speed = 45;

        public static int TTS_SPEED
        {
            get { return _tts_speed;}
            set { _tts_speed = value;}
        }
        private const int TTS_VOLUME = 50;
        private const int TTS_PITCH = 50;
        private readonly byte[] _buffer;
        private int _buffID;

        public XFTTSWebClient() : base("294c1b4fc8196e94d93d7e45ec0e5c98")
        {
            _buffID = VariableLengthArray<byte>.Cache.AccurateAlloc(MAX_BUFF_SIZE,
                                                                out _buffer);
        }

        protected override void OnDisposeManagedData()
        {
            base.OnDisposeManagedData();
            VariableLengthArray<byte>.Cache.Free(_buffID);
            _buffID = -1;
        }

        private bool PostReturn(string binFile)
        {
            bool hasError;
            try
            {
                _webResponse = (HttpWebResponse) _webRequest.GetResponse();
                using (Stream responseStream = _webResponse.GetResponseStream())
                {
                    if (_webResponse.ContentType.Contains("text"))
                    {
                        hasError = true;
                        if (responseStream != null)
                        {
                            using (StreamReader streamReader = new StreamReader(responseStream,
                                                                                Encoding.UTF8))
                            {
                                ResultString = streamReader.ReadToEnd();
                            }
                        }
                        Debug.Log("[BIN POST ERROR]:" + ResultString);
                    }
                    else
                    {
                        hasError = false;
                        using (FileStream fileStream = new FileStream(binFile,
                                                                      FileMode.Create))
                        {
                            int bytesRead;
                            while (responseStream != null &&
                                   (bytesRead = responseStream.Read(_buffer,
                                                                    0,
                                                                    _buffer.Length)) >
                                   0)
                            {
                                fileStream.Write(_buffer,
                                                 0,
                                                 bytesRead);
                            }
                        }
                    }
                }
                End();
            }
            catch (Exception ex)
            {
                Debug.LogError("[BIN POST ERROR]: " + ex);
                hasError = true;
            }
            return !hasError;
        }

        private void BuildTTSHeader()
        {
            string param = "{\"auf\":\"" +
                           TTS_AUF_16000 +
                           "\"," +
                           "\"aue\":\"" +
                           TTS_AUE_TYPE.raw +
                           "\"," +
                           "\"voice_name\":\"" +
                           TTS_VOICE_NAME.xiaoyan +
                           "\"," +
                           "\"speed\":\"" +
                           TTS_SPEED +
                           "\"," +
                           "\"volume\":\"" +
                           TTS_VOLUME +
                           "\"," +
                           "\"pitch\":\"" +
                           TTS_PITCH +
                           "\"," +
                           "\"engine_type\":\"" +
                           TTS_ENGINE_TYPE.intp65 +
                           "\"," +
                           "\"text_type\":\"" +
                           TTS_TEXT_TYPE.text +
                           "\"}";
            MakeHttpheader(param);
        }

        public bool PostData(string text,
                             string tempSavedFilePath)
        {
            if (Disposed)
            {
                return false;
            }
            _webRequest = null;
            _webResponse = null;
            IsTeminated = false;
            BuildTTSHeader();
            text = "text=" + text.UriEncode();
            byte[] body = Encoding.UTF8.GetBytes(text);
            CreateHttpPost(TTS_URL,
                           body,
                           0,
                           body.Length,
                           "application/x-www-form-urlencoded; charset=utf-8");
            return PostReturn(tempSavedFilePath);
        }
    }
}