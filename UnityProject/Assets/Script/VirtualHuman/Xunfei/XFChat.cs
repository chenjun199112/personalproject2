﻿// ************************************************
//                     自己实现
// ************************************************

#region
using System;
using System.Text;
using System.Threading;
using UnityEngine;
using Xunfei;
#endregion

internal sealed class XFChat : DisposableScriptWithUpdate
{
    public enum RawType
    {
        PCM_S16,
        PCM_F32_NORMAL
    }

    public enum State
    {
        None,
        InvalidDataSize,
        XFAIUIException,
        XFAIUIFailed,
        XFAIUISuccess,
        XFTTSException,
        XFTTSSuccess,
        HardcodeSayHello,
        NetworkError
    }

    public const int WaitTimes = 5000;

    private static readonly string[,] replaceMap = {
                                                           {"[k3]", ""}, {"[k0]", ""}, {"[n2]", ""},
                                                           {"༺", ""}, {"✾", ""}, {"೨", ""},
                                                           {"༻", ""}, {"ㄜ", ""}, {"ゞ", ""},
                                                           {"べ", ""}, {"☆", ""}, {"[", ""}, {"]", ""},
                                                   };

    private static readonly char[] lastIndexDot = {
                                                          '\\', '/', ':', '：', '•', '，', '。', '、', ',', '!', '！', ';',
                                                          '？', '；', '?'
                                                  };

    private readonly int _buffID;

    private readonly LockedValue<int> _locked = new LockedValue<int>(0);
    private readonly BetterList<XFChatSub> _processList;

    private readonly ManualResetEvent _signalStart;

    private readonly int MaxRecordBuffSecondLength;

    private readonly XFAIUIWebClient xfaiuiWebClient;
    private readonly XFTTSWebClient xfttsWebClient;

    private byte[] _buffBytes;
    private int _signalStartTick;


    public XFChat(RawType rawType)
    {
        SampleRate = XFConst.MicroPhoneSampleRate;
        InputAudioType = rawType;
        xfaiuiWebClient = new XFAIUIWebClient();
        xfttsWebClient = new XFTTSWebClient();
        _processList = new BetterList<XFChatSub>();
        _buffID = VariableLengthArray<byte>.Cache.AccurateAlloc(XFConst.MicroPhoneSampleRate * XFConst.MaxRecordBuffSecondLength * 2,
                                                            out _buffBytes);
        _signalStart = new ManualResetEvent(false);
        Pump.Instance.PushToThreadProcess(OnThread,
                                          null,
                                          null);
        Pump.Instance.PushToThreadProcess(OnThreadIdle,
                                          null,
                                          null);
    }

    public int SampleRate { get; private set; }
    public RawType InputAudioType { get; private set; }

    public int AIChat(float[] f32nData,
                      int validDataSize,
                      IntIntStringDelegate endDelegate)
    {
        if (Disposed)
        {
            return 0;
        }
        using (_locked.Lock())
        {
            XFChatSub speechData = new XFChatSub(f32nData,
                                                 validDataSize,
                                                 endDelegate);
            _processList.Add(speechData);
            return speechData.InstanceID;
        }
    }

    public int AIChat(byte[] u16Data,
                      int validDataSize,
                      IntIntStringDelegate endDelegate)
    {
        if (Disposed)
        {
            return 0;
        }
        using (_locked.Lock())
        {
            XFChatSub speechData = new XFChatSub(u16Data,
                                                 validDataSize,
                                                 endDelegate);
            _processList.Add(speechData);
            return speechData.InstanceID;
        }
    }

    protected override void OnDisposeManagedData()
    {
        xfaiuiWebClient.Dispose();
        xfttsWebClient.Dispose();
        _processList.Clear();
        _processList.Release();
        VariableLengthArray<byte>.Cache.Free(_buffID);
    }

    protected override void OnUpdateAction()
    {
        using (_locked.Lock())
        {
            if (_processList.size <= 0)
            {
                return;
            }
            XFChatSub innerProcessData = _processList[0];
            if (innerProcessData.IsEnd && !innerProcessData.Disposed)
            {
                if (innerProcessData.EndDelegate != null)
                {
                    innerProcessData.EndDelegate((int) innerProcessData.ChatState,
                                                 innerProcessData.InstanceID,
                                                 innerProcessData.GetResult());
                }
                innerProcessData.Dispose();
                _processList.Remove(innerProcessData);
            }
        }
    }

    private void OnThread()
    {
        while (!Disposed)
        {
            try
            {
                XFChatSub chatSub = null;
                using (_locked.Lock())
                {
                    if (_processList.size > 0)
                    {
                        if (!_processList[0].IsEnd)
                        {
                            chatSub = _processList[0];
                        }
                    }
                }
                if (chatSub == null)
                {
                    Thread.Sleep(1000);
                    continue;
                }
                _signalStartTick = Environment.TickCount;
                _signalStart.Set();
                OnProcess(chatSub);
                _signalStart.Reset();
                Debug.Log("XFChat ms:" + (Environment.TickCount - _signalStartTick));
            }
            catch (Exception e)
            {
                Debug.LogError("XFChat Exception:\n" + e);
            }
        }
    }

    private void OnThreadIdle()
    {
        while (!Disposed)
        {
            if (_signalStart.WaitOne(1))
            {
                if (Environment.TickCount - _signalStartTick < WaitTimes)
                {
                    continue;
                }
                _signalStart.Reset();
                xfaiuiWebClient.Teminate();
                xfttsWebClient.Teminate();
            }
            Thread.Sleep(30);
        }
    }

    private void OnProcess(XFChatSub xfChatSub)
    {
        if (xfChatSub.GetShortByteSize <= 0)
        {
            xfChatSub.ChatState = State.InvalidDataSize;
        }
        else
        {
            if (InputAudioType != RawType.PCM_S16)
            {
                xfChatSub.Floats.ToShortByteAudio(xfChatSub.ValidSize,
                                                  _buffBytes,
                                                  0);
            }
            else
            {
                _buffBytes = xfChatSub.Bytes;
            }
            bool recogState = xfaiuiWebClient.PostData(_buffBytes,
                                                       0,
                                                       xfChatSub.GetShortByteSize,
                                                       SampleRate);
            if (!recogState)
            {
                xfChatSub.ChatState = State.XFAIUIException;
            }
            else
            {
                xfChatSub.ChatState = State.XFAIUIFailed;
                XF_AIUI_DATA_RootObject jsonData = xfaiuiWebClient.ResultObject;
                if (jsonData.code == "0")
                {
                    foreach (XF_AIUI_DATA_Datum data in jsonData.data)
                    {
                        if (data.sub == "nlp")
                        {
                            if (data.intent != null && data.intent.rc == 0 && data.intent.answer != null)
                            {
                                xfChatSub.ChatState = State.XFAIUISuccess;
                                xfChatSub.SetResult(data.intent.answer.text);
                                break;
                            }
                        }
                    }
                }
            }
        }

        /////////////////////////
        if (xfChatSub.ChatState == State.XFAIUISuccess || xfChatSub.ChatState == State.HardcodeSayHello)
        {
            string wordList = xfChatSub.GetResult();
            ProcessWordListBeforeTTS(ref wordList);
            bool ttsState = xfttsWebClient.PostData(wordList,
                                                    Application.persistentDataPath + "/tts.wav");
            xfChatSub.ChatState = ttsState ? State.XFTTSSuccess : State.XFTTSException;
            xfChatSub.SetResult(wordList);
        }
        if (xfaiuiWebClient.IsTeminated || xfttsWebClient.IsTeminated)
        {
            xfChatSub.ChatState = State.NetworkError;
        }
        xfChatSub.Finish();
    }

    //NLP->TTS间的文本预处理：过滤/字符限制
    private void ProcessWordListBeforeTTS(ref string wordList)
    {
        //定义
        int wordMaxLength = 150; //字数上限
        StringBuilder sb = new StringBuilder(wordList);

        //过滤标记符
        for (int i = 0; i < replaceMap.Length / 2; i++)
        {
            sb.Replace(replaceMap[i,
                                  0],
                       replaceMap[i,
                                  1]);
        }
        if (Array.IndexOf(lastIndexDot,
                          sb[sb.Length - 1]) ==
            -1)
        {
            sb.Append("。");
        }

        //强制字数限制
        if (sb.Length > wordMaxLength)
        {
            sb.Length = wordMaxLength;
        }
        wordList = sb.ToString();
    }
}