﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XFConst
{
    public const int MiniNoiseCalcTimes = 10;
    public const int MicroPhoneSampleRate = 16000;
    public const int MaxRecordBuffSecondLength = 15;
}
