// ************************************************
//                   自己实现
// ************************************************

#region
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
#endregion

namespace Xunfei
{
    public abstract class XFBaseWebClient : DisposableObject
    {
        protected const string Post = "POST";
        protected const string XParam = "X-Param";
        protected const string XCurtime = "X-CurTime";
        protected const string XChecksum = "X-CheckSum";
        protected const string XAppid = "X-Appid";
        protected string _apiKey;
        protected string _appid;
        protected HttpWebRequest _webRequest;
        protected HttpWebResponse _webResponse;
        protected Dictionary<string, string> HttpHeader = new Dictionary<string, string>();

        protected XFBaseWebClient(string apiKey)
        {
            _appid = "5b68f403";
            _apiKey = apiKey;
        }

        public string ResultString { get; protected set; }

        public bool IsTeminated { get; protected set; }

        protected void MakeHttpheader(string param)
        {
            string curTime = Convert.ToInt32((DateTime.Now.ToUniversalTime() -
                                              new DateTime(1970,
                                                           1,
                                                           1,
                                                           0,
                                                           0,
                                                           0)).TotalSeconds).ToString();
            string paramBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(param));
            string checkSum = EncryptWithMD5(_apiKey + curTime + paramBase64);
            HttpHeader[XParam] = paramBase64;
            //Debug.LogError(paramBase64);
            HttpHeader[XCurtime] = curTime;
            //Debug.LogError(curTime);
            HttpHeader[XChecksum] = checkSum;
            //Debug.LogError(checkSum);
            HttpHeader[XAppid] = _appid;
        }

        protected string EncryptWithMD5(string source)
        {
            byte[] sor = Encoding.UTF8.GetBytes(source);
            MD5 md5 = MD5.Create();
            byte[] result = md5.ComputeHash(sor);
            StringBuilder strbul = new StringBuilder(40);
            for (int i = 0; i < result.Length; i++)
            {
                strbul.Append(result[i].ToString("x2"));
            }
            return strbul.ToString();
        }

        protected void CreateHttpPost(string url,
                                      byte[] body,
                                      int offset,
                                      int length,
                                      string contentType)
        {
            ResultString = null;
            _webRequest = (HttpWebRequest) WebRequest.Create(url);
            _webRequest.Method = Post;
            _webRequest.ContentType = contentType;
            _webRequest.Headers.Add(XParam,
                                    HttpHeader[XParam]);
            _webRequest.Headers.Add(XCurtime,
                                    HttpHeader[XCurtime]);
            _webRequest.Headers.Add(XChecksum,
                                    HttpHeader[XChecksum]);
            _webRequest.Headers.Add(XAppid,
                                    HttpHeader[XAppid]);
            using (Stream stream = _webRequest.GetRequestStream())
            {
                stream.Write(body,
                             offset,
                             length);
            }
        }

        protected override void OnDisposeManagedData() { Teminate(); }

        public void Teminate()
        {
            if (IsTeminated)
            {
                return;
            }
            End();
            IsTeminated = true;
        }

        protected void End()
        {
            if (_webRequest != null)
            {
                _webRequest.Abort();
                _webRequest = null;
            }
            if (_webResponse != null)
            {
                _webResponse.Close();
                _webResponse = null;
            }
            HttpHeader.Clear();
        }
    }
}