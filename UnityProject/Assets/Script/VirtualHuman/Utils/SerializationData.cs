﻿// ************************************************
//                     weiya_gao  
//                     2019-03.14
// ************************************************

#region
using System;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;
using UnityEngine;
#endregion

public class SerializationData<T> : SingletonWithUpdate<SerializationData<T>>
        where T : IClear
{
    private readonly Dictionary<int, T> _cachedInstance = new Dictionary<int, T>();
    private readonly Dictionary<int, int> _cachedInstanceCounter = new Dictionary<int, int>();
    private readonly BetterList<string> _cachedInstanceName1 = new BetterList<string>();
    private readonly BetterList<string> _cachedInstanceName2 = new BetterList<string>();
    private readonly BetterList<int> _zeroRefKeyList = new BetterList<int>();
    private string _dataPath;

    protected override void OnUninitAction()
    {
        foreach (KeyValuePair<int, T> keyValuePair in _cachedInstance)
        {
            keyValuePair.Value.Clear();
        }
        _cachedInstance.Clear();
        _cachedInstanceCounter.Clear();
        _cachedInstanceName1.Clear();
        _cachedInstanceName2.Clear();
        _zeroRefKeyList.Release();
    }

    protected override void OnInitAction()
    {
        if (!Application.isEditor)
        {
            _dataPath = Application.streamingAssetsPath;
        }
        else
        {
            _dataPath = Path.GetDirectoryName(Application.dataPath) + "/conf";
        }
    }

#if UNITY_EDITOR
    public bool Save(T data,
                     string parentDirName,
                     string fileName)
    {
        string filePath = _dataPath + "/" + typeof(T) + "/" + parentDirName + "/";
        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }
        filePath += fileName + ".bytes";
        using (FileStream fileStream = File.Create(filePath))
        {
            try
            {
                Serializer.Serialize(fileStream,
                                     data);
            }
            catch (Exception e)
            {
                Debug.LogError("[ERROR] Serialization:Save\n" + e);
                return false;
            }
            Debug.Log("Save to:" + filePath);
        }
        return true;
    }
#endif

    public int Get(string parentDir,
                   string fileName,
                   out T instance)
    {
        int index1 = _cachedInstanceName1.IndexOf(parentDir);
        int index2 = _cachedInstanceName2.IndexOf(fileName);
        int keyValue;
        if (index1 >= 0 && index2 >= 0)
        {
            keyValue = index1 * 10000 + index2;
            if (_cachedInstanceCounter.ContainsKey(keyValue))
            {
                _cachedInstanceCounter[keyValue]++;
                instance = _cachedInstance[keyValue];
                return keyValue;
            }
        }
        string filePath = _dataPath + "/" + typeof(T) + "/" + parentDir + "/" + fileName + ".bytes";
        if (File.Exists(filePath))
        {
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                try
                {
                    instance = Serializer.Deserialize<T>(fileStream);
                }
                catch (Exception e)
                {
                    Debug.LogError("[ERROR] Serialization:Get1\n" + e);
                    instance = default(T);
                    return -1;
                }
            }
        }
        else
        {
            Debug.LogError("[ERROR] Serialization:Get2\n" + filePath);
            instance = default(T);
            return -1;
        }
        if (index1 < 0)
        {
            _cachedInstanceName1.Add(parentDir);
            index1 = _cachedInstanceName1.size - 1;
        }
        if (index2 < 0)
        {
            _cachedInstanceName2.Add(fileName);
            index2 = _cachedInstanceName2.size - 1;
        }
        keyValue = index1 * 10000 + index2;
        _cachedInstance[keyValue] = instance;
        _cachedInstanceCounter[keyValue] = 1;
        return keyValue;
    }

    public void Release(int keyID)
    {
        if (_cachedInstanceCounter.ContainsKey(keyID))
        {
            _cachedInstanceCounter[keyID]--;
            if (_cachedInstanceCounter[keyID] == 0)
            {
                _zeroRefKeyList.Add(keyID);
            }
        }
    }

    //protected override void OnLateUpdateAction()
    //{
    //    if ((int) Time.realtimeSinceStartup % 10 != 0)
    //    {
    //        return;
    //    }
    //    if (_cachedInstance.Count > 20)
    //    {
    //        for (int i = 0; i < _zeroRefKeyList.size; i++)
    //        {
    //            if (_cachedInstance.ContainsKey(_zeroRefKeyList[i]))
    //            {
    //                _cachedInstance[_zeroRefKeyList[i]].Clear();
    //                _cachedInstance.Remove(_zeroRefKeyList[i]);
    //            }
    //            _cachedInstanceCounter.Remove(_zeroRefKeyList[i]);
    //        }
    //        _zeroRefKeyList.Clear();
    //    }
    //}
}