﻿
internal class BuffCacheForClass<T> : DisposableObject
        where T : DisposableObject, new()
{
    private readonly BetterList<T> _buffers;
    private readonly BetterList<bool> _takenFlag;

    public BuffCacheForClass(int initialMax)
    {
        _buffers = new BetterList<T>();
        _takenFlag = new BetterList<bool>();
        for (int i = 0; i < initialMax; i++)
        {
            _buffers.Add(new T());
            _takenFlag.Add(false);
        }
        Count = initialMax;
    }

    public int Count { get; private set; }

    private void Expand()
    {
        //Debug.Log("[INFO] BUFF Expand:" + Count);
        _buffers.Add(new T());
        _takenFlag.Add(false);
        Count++;
    }

    public int Alloc(out T outArray)
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            outArray = _buffers[i];
            if (!_takenFlag[i])
            {
                _takenFlag[i] = true;
                return i;
            }
        }
        Expand();
        _takenFlag[Count - 1] = true;
        outArray = _buffers[Count - 1];
        return Count - 1;
    }

    public T Alloc()
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            if (!_takenFlag[i])
            {
                _takenFlag[i] = true;
                return _buffers[i];
            }
        }
        Expand();
        _takenFlag[Count - 1] = true;
        return _buffers[Count - 1];
    }

    public void Free(int index)
    {
        if (index > Count && index < 0)
        {
            //Debug.LogError("[ERROR BUFF] Free:P" + Count + "@" + index);
            return;
        }
        //Debug.Log("[<<<FREE] BUFF:"+ _buffers[index].Length);
        _takenFlag[index] = false;
        _buffers[index].Clear();
    }

    public void Free(T instance)
    {
        for (int i = 0; i < Count; i++)
        {
            if (_buffers[i] == instance)
            {
                _takenFlag[i] = false;
                _buffers[i].Clear();
                return;
            }
        }
    }

    protected override void OnDisposeManagedData()
    {
        _buffers.Release();
        _buffers.Clear();
        _takenFlag.Release();
        _takenFlag.Clear();
        Count = 0;
    }
}

internal class BuffCacheForBasicClass<T> : DisposableObject
        where T : new()
{
    private readonly BetterList<T> _buffers;
    private readonly BetterList<bool> _takenFlag;

    public BuffCacheForBasicClass(int initialMax)
    {
        _buffers = new BetterList<T>();
        _takenFlag = new BetterList<bool>();
        for (int i = 0; i < initialMax; i++)
        {
            _buffers.Add(new T());
            _takenFlag.Add(false);
        }
        Count = initialMax;
    }

    public int Count { get; private set; }

    private void Expand()
    {
        //Debug.Log("[INFO] BUFF Expand:" + Count);
        _buffers.Add(new T());
        _takenFlag.Add(false);
        Count++;
    }

    public int Alloc(out T outArray)
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            outArray = _buffers[i];
            if (!_takenFlag[i])
            {
                _takenFlag[i] = true;
                return i;
            }
        }
        Expand();
        _takenFlag[Count - 1] = true;
        outArray = _buffers[Count - 1];
        return Count - 1;
    }

    public T Alloc()
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            if (!_takenFlag[i])
            {
                _takenFlag[i] = true;
                return _buffers[i];
            }
        }
        Expand();
        _takenFlag[Count - 1] = true;
        return _buffers[Count - 1];
    }

    public void Free(int index)
    {
        if (index > Count && index < 0)
        {
            //Debug.LogError("[ERROR BUFF] Free:P" + Count + "@" + index);
            return;
        }
        //Debug.Log("[<<<FREE] BUFF:"+ _buffers[index].Length);
        _takenFlag[index] = false;
    }

    public void Free(T instance)
    {
        if (instance == null)
        {
            return;
        }
        for (int i = 0; i < Count; i++)
        {
            if (_buffers[i].Equals(instance))
            {
                _takenFlag[i] = false;
                return;
            }
        }
    }

    protected override void OnDisposeManagedData()
    {
        _buffers.Release();
        _buffers.Clear();
        _takenFlag.Release();
        _takenFlag.Clear();
        Count = 0;
    }
}

internal static class ClassInstance<T>
        where T : DisposableObject, new()
{
    public static BuffCacheForClass<T> Cache { get; private set; }
    public static void Init(int size) { Cache = new BuffCacheForClass<T>(size); }
    public static void Uninit() { Cache.Dispose(); }
}

internal static class BasicClassInstance<T>
        where T : class, new()
{
    public static BuffCacheForBasicClass<T> Cache { get; private set; }
    public static void Init(int size) { Cache = new BuffCacheForBasicClass<T>(size); }
    public static void Uninit() { Cache.Dispose(); }
}