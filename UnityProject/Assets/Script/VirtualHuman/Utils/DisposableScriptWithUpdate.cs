﻿
public abstract class DisposableScriptWithUpdate : DisposableObject
{
    //继承类 一般不需要实现 构造和dispose，只需实现 OnInitAction 和 OnUninitAction
    //用 构造直接和Dispose组成初始化对，这样就不要init,uninit了
    protected DisposableScriptWithUpdate()
    {
        OnInitAction();
        Pump.Instance.SubscribeUpdate(OnGGUpdate);
        Pump.Instance.SubscribeFixedUpdateUpdate(OnGGFixedUpdate);
        Pump.Instance.SubscribeLateUpdate(OnGGLateUpdate);
        Pump.Instance.SubscribeAppPause(OnLowPause);
        //为了方便unity自动调用，editor模式的问题
        Pump.Instance.SubscribeUninit(Dispose);
    }

    public bool PauseHosting { get; set; }

    public bool PauseState
    {
        get { return PauseValue != 0; }
    }
    protected int PauseValue { get; set; }

    protected override void OnDisposeManagedData()
    {
        PauseValue = 0;
        OnUninitAction();
        Pump.Instance.UnsubscribeUpdate(OnGGUpdate);
        Pump.Instance.UnsubscribeFixedUpdate(OnGGFixedUpdate);
        Pump.Instance.UnsubscribeLateUpdate(OnGGLateUpdate);
        Pump.Instance.UnsubscribeAppPause(OnLowPause);
        Pump.Instance.UnsubscribeUninit(Dispose);
    }

    private void OnGGUpdate()
    {
        if (Disposed)
        {
            return;
        }
        if (PauseState)
        {
            return;
        }
        OnUpdateAction();
    }

    protected virtual void OnUpdateAction() { }

    private void OnGGLateUpdate()
    {
        if (Disposed)
        {
            return;
        }
        if (PauseState)
        {
            return;
        }
        OnLateUpdateAction();
    }

    protected virtual void OnLateUpdateAction() { }

    private void OnGGFixedUpdate()
    {
        if (Disposed)
        {
            return;
        }
        if (PauseState)
        {
            return;
        }
        OnFixedUpdateAction();
    }

    protected virtual void OnFixedUpdateAction() { }

    private void OnLowPause(bool state, bool noSys)
    {
        if (PauseHosting)
        {
            return;
        }
        Pause(state);
    }

    //可以手动，也会自动调用
    public void Pause(bool state)
    {
        if (Disposed)
        {
            return;
        }
        var preState = PauseState;
        if (state)
        {
            PauseValue++;
        }
        else
        {
            PauseValue--;
            if (PauseValue < 0)
            {
                PauseValue = 0;
            }
        }
        if (PauseState != preState)
        {
            OnPauseAction(state);
        }
    }

    protected virtual void OnPauseAction(bool state) { }

    protected virtual void OnUninitAction() { }

    protected virtual void OnInitAction() { }
}