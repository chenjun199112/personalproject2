﻿using System;
using System.Threading;

public class TransferBuffer<T> : DisposableObject
{
    private readonly T[] _buffer;
    private int _bufferId;
    private int _endPosition;

    private int _readHead;
    private int _unread;
    private int _writeHead;

    public TransferBuffer(int capacity = 4096)
    {
        _readHead = 0;
        _unread = 0;
        _writeHead = 0;
        _endPosition = -1;
        _bufferId = VariableLengthArray<T>.Cache.AccurateAlloc(capacity, out _buffer);
    }

    public int UnreadCount
    {
        get { return _unread; }
    }

    public int Capacity
    {
        get { return _buffer.Length; }
    }

    public bool IsWriteEnd
    {
        get { return _endPosition == _writeHead; }
    }

    public bool IsReadEnd
    {
        get { return _readHead == _writeHead && _endPosition == _readHead; }
    }

    public void SetEndPosition() { _endPosition = _writeHead; }

    public bool Write(T[] data,
                      int offset,
                      int dataSize)
    {
        if (_unread + dataSize > _buffer.Length)
            return false;
        if (_writeHead + dataSize > _buffer.Length)
        {
            var remainingSpace = _buffer.Length - _writeHead;
            Array.Copy(data, offset, _buffer, _writeHead, remainingSpace);
            Array.Copy(data, offset + remainingSpace, _buffer, 0, data.Length - remainingSpace);
            _writeHead = (_writeHead + dataSize) % _buffer.Length;
        }
        else
        {
            Array.Copy(data, offset, _buffer, _writeHead, dataSize);
            _writeHead += dataSize;
        }
        Interlocked.Add(ref _unread, dataSize);
        return true;
    }

    public int Read(T[] data,
                     int offset,
                     int size)
    {
        if (_unread < size)
        {
            if (_unread <= 0)
                return 0;
            size = _unread;
        }
        if (_readHead + size > _buffer.Length)
        {
            var remainingSpace = _buffer.Length - _readHead;
            Array.Copy(_buffer, _readHead, data, offset, remainingSpace);
            Array.Copy(_buffer, 0, data, offset + remainingSpace, size - remainingSpace);
            _readHead = (_readHead + size) % _buffer.Length;
        }
        else
        {
            Array.Copy(_buffer, _readHead, data, offset, size);
            _readHead += size;
        }
        Interlocked.Add(ref _unread, -size);
        return size;
    }

    protected override void OnDisposeManagedData()
    {
        VariableLengthArray<T>.Cache.Free(_bufferId);
        _readHead = 0;
        _writeHead = 0;
        _unread = 0;
        _bufferId = 0;
    }
}