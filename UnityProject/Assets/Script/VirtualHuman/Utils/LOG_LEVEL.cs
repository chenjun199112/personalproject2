﻿
public enum LOG_LEVEL
{
    INFO = 0,
    WARN = 1,
    DEBUG = 2,
    VERBOSE = 3
}