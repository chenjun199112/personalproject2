﻿
internal class BufferCacheForVariableLengthArray<T> : DisposableObject
{
    private readonly BetterList<T[]> _buffers;
    private readonly BetterList<bool> _takenFlag;

    public BufferCacheForVariableLengthArray(int initialMaxPow)
    {
        _buffers = new BetterList<T[]>();
        _takenFlag = new BetterList<bool>();
        for (int i = 0; i < initialMaxPow; i++)
        {
            _buffers.Add(new T[1 << i]);
            _takenFlag.Add(false);
        }
        Count = initialMaxPow - 1;
    }

    public int Count { get; private set; }

    private int Expand()
    {
        //Debug.Log("[INFO] BUFF Expand:" + Count);
        Count++;
        _buffers.Add(new T[1 << Count]);
        _takenFlag.Add(false);
        return 1 << Count;
    }

    private int AccurateExpand(int buffSize)
    {
        //Debug.Log("[INFO] BUFF AccurateExpand:" + buffSize);
        Count++;
        _buffers.Add(new T[buffSize]);
        _takenFlag.Add(false);
        return buffSize;
    }

    public int AccurateAlloc(int buffSize,
                             out T[] buf)
    {
        //Debug.Log("[>>>A ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            buf = _buffers[i];
            if (buf.Length == buffSize && !_takenFlag[i])
            {
                _takenFlag[i] = true;
                return i;
            }
        }
        AccurateExpand(buffSize);
        _takenFlag[Count] = true;
        buf = _buffers[Count];
        return Count;
    }

    public int Alloc(int buffSize,
                     out T[] buf)
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            buf = _buffers[i];
            if (buf.Length >= buffSize && !_takenFlag[i])
            {
                _takenFlag[i] = true;
                return i;
            }
        }
        while (true)
        {
            if (Expand() >= buffSize)
            {
                buf = _buffers[Count];
                _takenFlag[Count] = true;
                return Count;
            }
        }
    }

    public void Free(int index)
    {
        if (index > Count && index < 0)
        {
            //Debug.LogError("[ERROR BUFF] Free:P" + Count + "@" + index);
            return;
        }
        //Debug.Log("[<<<FREE] BUFF:"+ _buffers[index].Length);
        _takenFlag[index] = false;
    }

    protected override void OnDisposeManagedData()
    {
        _buffers.Clear();
        _takenFlag.Clear();
        Count = 0;
    }
}

internal static class VariableLengthArray<T>
{
    public static BufferCacheForVariableLengthArray<T> Cache { get; private set; }
    public static void Init(int size) { Cache = new BufferCacheForVariableLengthArray<T>(size); }

    public static void Uninit() { Cache.Dispose(); }
}