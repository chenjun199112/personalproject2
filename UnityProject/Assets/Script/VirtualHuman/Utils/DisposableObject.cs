﻿using System;

public abstract class DisposableObject : IDisposable, IClear
{
    public virtual bool Disposed { get; private set; }

    public virtual void Clear() { }

    public void Dispose()
    {
        if (Disposed)
        {
            return;
        }
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool state)
    {
        if (state)
        {
            OnDisposeManagedData();
            Disposed = true;
        }
        OnDisposeUnManagedData();
    }

    protected abstract void OnDisposeManagedData();
    protected virtual void OnDisposeUnManagedData() { }
}