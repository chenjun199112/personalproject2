﻿using System;
using System.Collections;
using System.Threading;
using UnityEngine;

internal sealed class Pump : MonoBehaviour
{
#region CreatePump
    private static Pump CurrentInstance;

    public static Pump Instance
    {
        get
        {
            if (CurrentInstance == null)
            {
                CurrentInstance = Create("Pump");
                return CurrentInstance;
            }
            return CurrentInstance;
        }
    }

    private static Pump Create(string instanceName)
    {
        GameObject o = new GameObject(instanceName);
        DontDestroyOnLoad(o);
        Pump comp = o.AddComponent<Pump>();
        return comp;
    }
#endregion

#region PushAction
    private readonly object _pushLocker = new object();

    private class PumpProcessData : IDisposable
    {
        private readonly BoolDelegate _endAction;
        private readonly VoidDelegate _progressAction;
        private readonly VoidDelegate _runAction;
        private bool _disposed;

        public PumpProcessData(bool threadProcess, VoidDelegate action, VoidDelegate progress, BoolDelegate endDelegate)
        {
            _runAction = action;
            _progressAction = progress;
            _endAction = endDelegate;
            Running = false;
            IsEnd = false;
            RunEnumerator = !threadProcess ? ToMain() : ToSub();
        }

        public IEnumerator RunEnumerator { get; private set; }

        public bool Running { get; set; }
        public bool IsEnd { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void RunAction()
        {
            if (_runAction != null)
            {
                _runAction();
            }
        }

        private void RunProgressAction()
        {
            if (_progressAction != null)
            {
                _progressAction();
            }
        }

        private void RunEndAction(bool state)
        {
            if (_endAction != null)
            {
                _endAction(state);
            }
            IsEnd = true;
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                _disposed = true;
            }
        }

        private IEnumerator ToMain()
        {
            bool hasException = false;
            try
            {
                RunAction();
            }
            catch (Exception ex)
            {
                Debug.LogError("[SubscribeCoroutine] Main Exception:" + ex);
                hasException = true;
            }
            yield return Yielders.EndOfFrame;
            RunEndAction(!hasException);
        }

        private IEnumerator ToSub()
        {
            bool returnValue = false;
            bool hasException = false;
            ThreadPool.QueueUserWorkItem(objects =>
                                         {
                                             try
                                             {
                                                 RunAction();
                                             }
                                             catch (Exception ex)
                                             {
                                                 Debug.LogError("SubscribeCoroutine Thread Exception:" + ex);
                                                 hasException = true;
                                             }
                                             finally
                                             {
                                                 returnValue = true;
                                             }
                                         });
            while (!returnValue)
            {
                RunProgressAction();
                yield return Yielders.EndOfFrame;
            }
            RunEndAction(!hasException);
        }

        ~PumpProcessData() { Dispose(false); }
    }

    private readonly BetterList<PumpProcessData> _processList = new BetterList<PumpProcessData>();
    private readonly BetterList<PumpProcessData> _processDeleteList = new BetterList<PumpProcessData>();

    public void PushToMainProcess(VoidDelegate action, BoolDelegate endDelegate)
    {
        lock (_pushLocker)
        {
            _processList.Add(new PumpProcessData(false,
                                                 action,
                                                 null,
                                                 endDelegate));
        }
    }

    public void PushToThreadProcess(VoidDelegate action, VoidDelegate progress, BoolDelegate endDelegate)
    {
        lock (_pushLocker)
        {
            _processList.Add(new PumpProcessData(true,
                                                 action,
                                                 progress,
                                                 endDelegate));
        }
    }

    private void OnUpdatePushedAction()
    {
        lock (_pushLocker)
        {
            for (int i = 0; i < _processList.size; i++)
            {
                PumpProcessData current = _processList[i];
                if (!current.Running)
                {
                    current.Running = true;
                    SubscribeCoroutine(current.RunEnumerator);
                }
                else if (current.IsEnd)
                {
                    _processDeleteList.Add(current);
                }
            }
            for (int i = 0; i < _processDeleteList.size; i++)
            {
                _processList.Remove(_processDeleteList[i]);
                _processDeleteList[i].Dispose();
            }
            _processDeleteList.Clear();
        }
    }
#endregion

#region SubscribeCoroutine
    private readonly BetterList<CoroutineData> _coroutineDataList = new BetterList<CoroutineData>();

    private sealed class CoroutineData : IDisposable
    {
        private readonly VoidDelegate _endAction;
        private bool _disposed;

        public CoroutineData(MonoBehaviour monoRun, IEnumerator enumTarget, VoidDelegate end)
        {
            MonoRun = monoRun;
            ID = MonoRun.GetInstanceID();
            EnumTarget = enumTarget;
            _endAction = end;
            IsRunning = false;
        }

        public IEnumerator EnumTarget { get; private set; }
        public MonoBehaviour MonoRun { get; private set; }
        public int ID { get; private set; }

        public bool IsRunning { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void RunEndAction()
        {
            if (_endAction != null)
            {
                _endAction();
            }
        }

        public bool Run(IEnumerator newEnumTarget)
        {
            if (!IsRunning)
            {
                IsRunning = true;
                MonoRun.StartCoroutine(newEnumTarget);
                return true;
            }
            return false;
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    MonoRun.StopAllCoroutines();
                    Destroy(MonoRun);
                }
                _disposed = true;
            }
        }

        ~CoroutineData() { Dispose(false); }
    }

    public int SubscribeCoroutine(IEnumerator runner, VoidDelegate endDelegate = null)
    {
        CoroutineData run = new CoroutineData(gameObject.AddComponent<BaseMono>(),
                                              runner,
                                              endDelegate);
        _coroutineDataList.Add(run);
        return run.ID;
    }

    public void UnsubscribeCoroutine(int id)
    {
        for (int i = 0; i < _coroutineDataList.size; i++)
        {
            if (_coroutineDataList[i].ID == id)
            {
                _coroutineDataList[i].Dispose();
                _coroutineDataList.RemoveAt(i);
                return;
            }
        }
    }

    private IEnumerator InnerCoroutine(CoroutineData co)
    {
        yield return co.MonoRun.StartCoroutine(co.EnumTarget);
        _coroutineDataList.Remove(co);
        co.RunEndAction();
        co.Dispose();
    }

    private void OnUpdateCoroutine()
    {
        for (int i = 0; i < _coroutineDataList.size; i++)
        {
            if (!_coroutineDataList[i].IsRunning)
            {
                _coroutineDataList[i].Run(InnerCoroutine(_coroutineDataList[i]));
            }
        }
    }
#endregion

#region Update
    private event VoidDelegate UpdateEvent;

    public void SubscribeUpdate(VoidDelegate sDelegate) { UpdateEvent += sDelegate; }

    public void UnsubscribeUpdate(VoidDelegate sDelegate) { UpdateEvent -= sDelegate; }

    private void Update()
    {
        OnUpdatePushedAction();
        OnUpdateCoroutine();
        OnUpdateEvent();
    }

    private void OnUpdateEvent()
    {
        if (_appPauseValue)
        {
            return;
        }
        if (UpdateEvent != null)
        {
            UpdateEvent();
        }
    }
#endregion

#region LateUpdate
    private event VoidDelegate LateUpdateEvent;

    public void SubscribeLateUpdate(VoidDelegate sDelegate) { LateUpdateEvent += sDelegate; }

    public void UnsubscribeLateUpdate(VoidDelegate sDelegate) { LateUpdateEvent -= sDelegate; }

    private void LateUpdate() { OnLateUpdateEvent(); }

    private void OnLateUpdateEvent()
    {
        if (_appPauseValue)
        {
            return;
        }
        if (LateUpdateEvent != null)
        {
            LateUpdateEvent();
        }
    }
#endregion

#region FixedUpdate
    private event VoidDelegate FixedUpdateEvent;

    public void SubscribeFixedUpdateUpdate(VoidDelegate sDelegate) { FixedUpdateEvent += sDelegate; }

    public void UnsubscribeFixedUpdate(VoidDelegate sDelegate) { FixedUpdateEvent -= sDelegate; }

    private void FixedUpdate() { OnFixedUpdateEvent(); }

    private void OnFixedUpdateEvent()
    {
        if (_appPauseValue)
        {
            return;
        }
        if (FixedUpdateEvent != null)
        {
            FixedUpdateEvent();
        }
    }
#endregion

#region Pause
    private event BoolBoolDelegate PauseEvent;
    private bool _appPauseValue;

    public void SubscribeAppPause(BoolBoolDelegate pauseDelegate)
    {
        if (pauseDelegate == OnPauseEvent)
        {
            return;
        }
        PauseEvent += pauseDelegate;
    }

    public void UnsubscribeAppPause(BoolBoolDelegate sDelegate)
    {
        if (sDelegate == OnPauseEvent)
        {
            return;
        }
        PauseEvent -= sDelegate;
    }

    private void OnApplicationPause(bool pause)
    {
        OnPauseEvent(pause,
                     false);
    }

    private void OnPauseEvent(bool pause, bool noSystem)
    {
        _appPauseValue = pause;
        if (PauseEvent != null)
        {
            PauseEvent(pause,
                       noSystem);
        }
    }
#endregion

#region Uninit
    private event VoidDelegate UninitEvent;

    public void SubscribeUninit(VoidDelegate sDelegate)
    {
        if (sDelegate == UninitEvent)
        {
            return;
        }
        UninitEvent += sDelegate;
    }

    public void UnsubscribeUninit(VoidDelegate sDelegate)
    {
        if (sDelegate == UninitEvent)
        {
            return;
        }
        UninitEvent -= sDelegate;
    }

    private void OnDestroy() { OnUninitEvent(); }

    private void OnUninitEvent()
    {
        _appPauseValue = false;
        if (UninitEvent != null)
        {
            UninitEvent();
        }
    }
#endregion
}