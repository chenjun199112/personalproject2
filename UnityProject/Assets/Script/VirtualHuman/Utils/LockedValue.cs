﻿using System;
using System.Threading;


internal class LockedValue<T>
{
    private readonly object _lockObject;
    private readonly Unlocker _unlocker;
    private T _value;

    public LockedValue(T value)
    {
        _value = value;
        _lockObject = new object();
        _unlocker = new Unlocker(this);
    }

    public Unlocker Lock()
    {
        Monitor.Enter(_lockObject);
        return _unlocker;
    }

    private void Unlock() { Monitor.Exit(_lockObject); }

    public class Unlocker : IDisposable
    {
        private readonly LockedValue<T> _parent;

        public Unlocker(LockedValue<T> parent) { _parent = parent; }

        public T Value
        {
            get { return _parent._value; }
            set { _parent._value = value; }
        }

        public void Dispose() { _parent.Unlock(); }
    }
}

internal class ReadonlyLockedValue<T>
{
    private readonly object _lockObject;
    private readonly Unlocker _unlocker;
    private readonly T _value;

    public ReadonlyLockedValue(T value)
    {
        _value = value;
        _lockObject = new object();
        _unlocker = new Unlocker(this);
    }

    public Unlocker Lock()
    {
        Monitor.Enter(_lockObject);
        return _unlocker;
    }

    private void Unlock() { Monitor.Exit(_lockObject); }

    public class Unlocker : IDisposable
    {
        private readonly ReadonlyLockedValue<T> _parent;

        public Unlocker(ReadonlyLockedValue<T> parent) { _parent = parent; }

        public T Value
        {
            get { return _parent._value; }
        }

        public void Dispose() { _parent.Unlock(); }
    }
}