using UnityEngine;
using UnityEngine.EventSystems;


public delegate void VoidDelegate();

public delegate void BoolDelegate(bool state);

public delegate void BoolBoolDelegate(bool state1,
                                      bool state2);

public delegate void IntDelegate(int state);

public delegate void BoolGameObjectDelegate(bool state,
                                            GameObject instance);

public delegate void FloatGameObjectDelegate(float valueState,
                                             GameObject instance);

public delegate void Vector2GameObjectDelegate(Vector2 valueState,
                                               GameObject instance);

public delegate void Vector2DirectionDelegate(Vector2 valueState,
                                              MoveDirection direction);

public delegate void GameObjectDelegate(GameObject instance);

public delegate void GameObjectVector2Delegate(GameObject instance,
                                               Vector2 position);

public delegate void StringDelegate(string val1);

public delegate void IntStringDelegate(int intVal,
                                       string val1);

public delegate void BoolIntStringDelegate(bool state,
                                           int intVal1,
                                           string val1);

public delegate void IntIntStringDelegate(int state,
                                          int intVal1,
                                          string val1);

public delegate void StringStringDelegate(string val1,
                                          string val2);

public delegate void IntStringStringDelegate(int intVal,
                                             string val1,
                                             string val2);

public delegate void KeyInputStateDelegate(GameObject instance,
                                           KeyCode key,
                                           bool state);

public delegate void FloatBuffDelegate(float[] buff,
                                       int index,
                                       int size);

public delegate void BoolFloatBuffDelegate(bool state,
                                           float[] buff,
                                           int size);