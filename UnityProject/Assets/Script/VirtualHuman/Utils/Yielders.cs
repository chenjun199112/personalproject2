﻿using System.Collections.Generic;
using UnityEngine;

public static class Yielders
{
    private static readonly Dictionary<float, WaitForSeconds> TimeInterval = new Dictionary<float, WaitForSeconds>(100);

    private static readonly WaitForEndOfFrame _endOfFrame = new WaitForEndOfFrame();
    private static readonly WaitForFixedUpdate _fixedUpdate = new WaitForFixedUpdate();

    public static WaitForEndOfFrame EndOfFrame
    {
        get { return _endOfFrame; }
    }

    public static WaitForFixedUpdate FixedUpdate
    {
        get { return _fixedUpdate; }
    }

    public static WaitForSeconds Get(float seconds)
    {
        if (!TimeInterval.ContainsKey(seconds))
            TimeInterval.Add(seconds, new WaitForSeconds(seconds));
        return TimeInterval[seconds];
    }
}