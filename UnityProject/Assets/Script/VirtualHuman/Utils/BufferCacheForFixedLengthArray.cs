﻿// ************************************************
//                     weiya_gao  
//                     2019-02.22
// ************************************************

#region
#endregion

internal class BufferCacheForFixedLengthArray<T> : DisposableObject
{
    private readonly BetterList<T[]> _buffers;
    private readonly BetterList<bool> _takenFlag;

    public BufferCacheForFixedLengthArray(int initialMaxPow,
                                          int fixedBuffSize)
    {
        _buffers = new BetterList<T[]>();
        _takenFlag = new BetterList<bool>();
        FixedBuffSize = fixedBuffSize;
        for (int i = 0; i < initialMaxPow; i++)
        {
            _buffers.Add(new T[fixedBuffSize]);
            _takenFlag.Add(false);
        }
        Count = initialMaxPow;
    }

    public int Count { get; private set; }
    public int FixedBuffSize { get; }

    private void Expand()
    {
        //Debug.Log("[INFO] BUFF Expand:" + Count);
        _buffers.Add(new T[FixedBuffSize]);
        _takenFlag.Add(false);
        Count++;
    }

    public int Alloc(out T[] outArray)
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            outArray = _buffers[i];
            if (!_takenFlag[i])
            {
                _takenFlag[i] = true;
                return i;
            }
        }
        Expand();
        _takenFlag[Count - 1] = true;
        outArray = _buffers[Count - 1];
        return Count - 1;
    }

    public T[] Alloc()
    {
        //Debug.Log("[>>>ALLOC] BUFF:" + buffSize);
        for (int i = 0; i < _buffers.size; i++)
        {
            if (!_takenFlag[i])
            {
                _takenFlag[i] = true;
                return _buffers[i];
            }
        }
        Expand();
        _takenFlag[Count - 1] = true;
        return _buffers[Count - 1];
    }

    public void Free(int index)
    {
        if (index > Count && index < 0)
        {
            //Debug.LogError("[ERROR BUFF] Free:P" + Count + "@" + index);
            return;
        }
        //Debug.Log("[<<<FREE] BUFF:"+ _buffers[index].Length);
        _takenFlag[index] = false;
    }

    public void Free(T[] instance)
    {
        if (instance == null)
        {
            return;
        }
        for (int i = 0; i < Count; i++)
        {
            if (_buffers[i] == instance)
            {
                _takenFlag[i] = false;
                return;
            }
        }
    }

    protected override void OnDisposeManagedData()
    {
        _buffers.Release();
        _buffers.Clear();
        _takenFlag.Release();
        _takenFlag.Clear();
        Count = 0;
    }
}

internal static class FixedLengthArray<T>
{
    public static BufferCacheForFixedLengthArray<T> Cache2 { get; private set; }
    public static BufferCacheForFixedLengthArray<T> Cache3 { get; private set; }
    public static BufferCacheForFixedLengthArray<T> Cache4 { get; private set; }

    public static void Init(int size2,
                            int size3,
                            int size4)
    {
        Cache2 = new BufferCacheForFixedLengthArray<T>(size2,
                                                       2);
        Cache3 = new BufferCacheForFixedLengthArray<T>(size3,
                                                       3);
        Cache4 = new BufferCacheForFixedLengthArray<T>(size4,
                                                       4);
    }

    public static void Uninit()
    {
        Cache2.Dispose();
        Cache3.Dispose();
        Cache4.Dispose();
    }
}