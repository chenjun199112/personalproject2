﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

internal static class ExtendCS
{
    public static string SimpleSerializeXml(this object data)
    {
        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        ns.Add("",
               "");
        XmlWriterSettings settings = new XmlWriterSettings {
                                                                   OmitXmlDeclaration = true,
                                                                   Encoding = Encoding.UTF8
                                                           };
        using (MemoryStream memoryStream = new MemoryStream())
        {
            using (XmlWriter writer = XmlWriter.Create(memoryStream,
                                                       settings))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(data.GetType());
                xmlSerializer.Serialize(writer,
                                        data,
                                        ns);
                string serializeXml = Encoding.UTF8.GetString(memoryStream.ToArray());
                return serializeXml;
            }
        }
    }

    public static T SimpleDeserializeXML<T>(this string xmlString)
    {
        T ins;
        using (MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
        {
            XmlSerializer xz = new XmlSerializer(typeof(T));
            ins = (T) xz.Deserialize(xmlStream);
        }
        return ins;
    }

    public static void SaveAudioData(AudioClip clip, string path)
    {
        float[] samples = new float[clip.samples * clip.channels];
        clip.GetData(samples,
                     0);
        int sampleSize = samples.Length;
        using (FileStream fileStream = new FileStream(path,
                                                      FileMode.Create))
        {
            unsafe
            {
                fixed (float* ptr = samples)
                {
                    for (int i = 0; i < sampleSize; i++)
                    {
                        byte* ptrByte = (byte*) (ptr + i);
                        fileStream.WriteByte(*ptrByte);
                        fileStream.WriteByte(*(ptrByte + 1));
                        fileStream.WriteByte(*(ptrByte + 2));
                        fileStream.WriteByte(*(ptrByte + 3));
                    }
                }
            }
        }
        samples = null;
    }

    public static Color ConvertColor(int r, int g, int b, int a)
    {
        return new Color(r / (float) byte.MaxValue,
                         g / (float) byte.MaxValue,
                         b / (float) byte.MaxValue,
                         a / (float) byte.MaxValue);
    }

    public static void ResampleRawAudio(float[] src, float[] dst)
    {
        float rec = 1.0f / dst.Length;
        for (int i = 0; i < dst.Length; ++i)
        {
            float interp = rec * i * src.Length;
            dst[i] = src[(int) interp] * 4;
        }
    }

    public static void ToShortAudio(this float[] input, short[] output)
    {
        if (output.Length < input.Length)
        {
            throw new ArgumentException("in: " + input.Length + ", out: " + output.Length);
        }
        for (int i = 0; i < input.Length; ++i)
        {
            output[i] = (short) Mathf.Clamp((int) (input[i] * short.MaxValue * 1.0f),
                                            short.MinValue,
                                            short.MaxValue);
        }
    }

    public static void ToFloatNAudio(this byte[] input, int sampleSize, float[] output, int offset)
    {
        unsafe
        {
            fixed (byte* a = input)
            {
                fixed (float* b = output)
                {
                    for (int i = 0; i < sampleSize; i++)
                    {
                        *(b + offset + 1) = *((short*) a + 1) * 1.0f / short.MaxValue;
                    }
                }
            }
        }
    }

    public static string UriEncode(this string input, bool encodeSlash = false)
    {
        StringBuilder stringBuilder = new StringBuilder();
        foreach (byte num in Encoding.UTF8.GetBytes(input))
        {
            if (num >= 97 && num <= 122 || num >= 65 && num <= 90 || num >= 48 && num <= 57 || num == 95 || num == 45 ||
                num == 126 || num == 46)
            {
                stringBuilder.Append((char) num);
            }
            else if (num == 47)
            {
                if (encodeSlash)
                {
                    stringBuilder.Append("%2F");
                }
                else
                {
                    stringBuilder.Append((char) num);
                }
            }
            else
            {
                stringBuilder.Append('%').Append(num.ToString("X2"));
            }
        }
        return stringBuilder.ToString();
    }

#region
    private static readonly byte[] ToByteAudioArray = new byte[2];

    public static void ToShortByteAudio(this float[] input, int length, byte[] output, int offset)
    {
        if (output.Length < input.Length)
        {
            throw new ArgumentException("in: " + input.Length + ", out: " + output.Length);
        }
        for (int i = 0; i < length; ++i)
        {
            short clamp = (short) Mathf.Clamp((int) (input[i] * short.MaxValue * 1.5f),
                                              short.MinValue,
                                              short.MaxValue);
            unsafe
            {
                fixed (byte* numPtr = ToByteAudioArray)
                {
                    *(short*) numPtr = clamp;
                }
            }
            output[offset + i * 2] = ToByteAudioArray[0];
            output[offset + i * 2 + 1] = ToByteAudioArray[1];
        }
    }

    public static void ToShortByteAudio(this float[] input, int length, TransferBuffer<byte> outBuffer)
    {
        for (int i = 0; i < length; ++i)
        {
            short clamp = (short) Mathf.Clamp((int) (input[i] * short.MaxValue * 1.5f),
                                              short.MinValue,
                                              short.MaxValue);
            unsafe
            {
                fixed (byte* numPtr = ToByteAudioArray)
                {
                    *(short*) numPtr = clamp;
                }
            }
            outBuffer.Write(ToByteAudioArray,
                            0,
                            2);
        }
    }
#endregion

#region
    /// <summary>
    ///     Transforms an euler rotation in world space to one relative to a Transform.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="eulerAngle"></param>
    /// <returns></returns>
    public static Vector3 InverseTransformEulerAngle(this Transform transform, Vector3 eulerAngle)
    {
        return (eulerAngle - transform.eulerAngles).ToPositiveEuler();
    }

    /// <summary>
    ///     Transforms an euler rotation relative to a Transform to one in world space.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="eulerAngle"></param>
    /// <returns></returns>
    public static Vector3 TransformEulerAngle(this Transform transform, Vector3 eulerAngle)
    {
        return ClampRange(eulerAngle + transform.eulerAngles);
    }

    /// <summary>
    ///     Converts an euler rotation in the -180 - 180 range to one in the 0 to 360 range.
    /// </summary>
    /// <param name="eulerAngle"></param>
    /// <returns></returns>
    public static Vector3 ToPositiveEuler(this Vector3 eulerAngle)
    {
        float x = eulerAngle.x;
        float y = eulerAngle.y;
        float z = eulerAngle.z;
        if (x < 0)
        {
            x = 360 + x;
        }
        if (y < 0)
        {
            y = 360 + y;
        }
        if (z < 0)
        {
            z = 360 + z;
        }
        eulerAngle.x = x;
        eulerAngle.y = y;
        eulerAngle.z = z;
        return eulerAngle;
    }

    /// <summary>
    ///     Converts an euler rotation in the 0 - 360 range to one in the -180 to 180 range.
    /// </summary>
    /// <param name="eulerAngle"></param>
    /// <returns></returns>
    public static Vector3 ToNegativeEuler(this Vector3 eulerAngle)
    {
        float x = eulerAngle.x;
        float y = eulerAngle.y;
        float z = eulerAngle.z;
        if (x > 180)
        {
            x -= 360;
        }
        if (y > 180)
        {
            y -= 360;
        }
        if (z > 180)
        {
            z -= 360;
        }
        eulerAngle.x = x;
        eulerAngle.y = y;
        eulerAngle.z = z;
        return eulerAngle;
    }

    private static Vector3 ClampRange(Vector3 eulerAngle)
    {
        float x = eulerAngle.x;
        float y = eulerAngle.y;
        float z = eulerAngle.z;
        if (x > 360)
        {
            x -= 360;
        }
        if (y > 360)
        {
            y -= 360;
        }
        if (z > 360)
        {
            z -= 360;
        }
        eulerAngle.x = x;
        eulerAngle.y = y;
        eulerAngle.z = z;
        return eulerAngle.ToPositiveEuler();
    }
#endregion
}