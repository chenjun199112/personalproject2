﻿using UnityEngine;

public abstract class SingletonWithUpdate<T> : Singleton<T>
        where T : class, new()
{
    public bool PauseState
    {
        get { return PauseValue != 0; }
    }
    private int PauseValue { get; set; }
    public bool PauseHosting { get; set; }

    //只能手动
    public override bool Init()
    {
        if (InitOk)
        {
            return true;
        }
        PauseValue = 0;
        OnInitAction();
        if (Application.isPlaying)
        {
            Pump.Instance.SubscribeUpdate(OnGGUpdate);
            Pump.Instance.SubscribeFixedUpdateUpdate(OnGGFixedUpdate);
            Pump.Instance.SubscribeLateUpdate(OnGGLateUpdate);
            Pump.Instance.SubscribeAppPause(OnLowPause);
            Pump.Instance.SubscribeUninit(Uninit);
        }
        InitOk = true;
        return InitOk;
    }

    private void OnLowPause(bool state,
                            bool noSys)
    {
        if (PauseHosting)
        {
            return;
        }
        Pause(state);
    }

    //可以手动，也会自动调用
    public override void Uninit()
    {
        if (!InitOk)
        {
            return;
        }
        InitOk = false;
        OnUninitAction();
        if (Application.isPlaying)
        {
            Pump.Instance.UnsubscribeUpdate(OnGGUpdate);
            Pump.Instance.UnsubscribeFixedUpdate(OnGGFixedUpdate);
            Pump.Instance.UnsubscribeLateUpdate(OnGGLateUpdate);
            Pump.Instance.UnsubscribeAppPause(OnLowPause);
            Pump.Instance.UnsubscribeUninit(Uninit);
        }
    }

    private void OnGGUpdate()
    {
        if (!InitOk)
        {
            return;
        }
        if (PauseState)
        {
            return;
        }
        OnUpdateAction();
    }

    protected virtual void OnUpdateAction() { }

    private void OnGGLateUpdate()
    {
        if (!InitOk)
        {
            return;
        }
        if (PauseState)
        {
            return;
        }
        OnLateUpdateAction();
    }

    protected virtual void OnLateUpdateAction() { }

    private void OnGGFixedUpdate()
    {
        if (!InitOk)
        {
            return;
        }
        if (PauseState)
        {
            return;
        }
        OnFixedUpdateAction();
    }

    protected virtual void OnFixedUpdateAction() { }

    //可以手动，也会自动调用
    public void Pause(bool state)
    {
        if (!InitOk)
        {
            return;
        }
        bool preState = PauseState;
        if (state)
        {
            PauseValue++;
        }
        else
        {
            PauseValue--;
            if (PauseValue < 0)
            {
                PauseValue = 0;
            }
        }
        if (PauseState != preState)
        {
            OnPauseAction(state);
        }
    }

    protected virtual void OnPauseAction(bool state) { }
}