﻿
public abstract class Singleton<T> where T : class, new()
{
    public bool InitOk { get; protected set; }

    public static T Instance
    {
        get { return Sub.Instance; }
    }

    public virtual bool Init()
    {
        if (InitOk)
        {
            return true;
        }
        OnInitAction();
        InitOk = true;
        return true;
    }

    public virtual void Uninit()
    {
        if (!InitOk)
        {
            return;
        }
        OnUninitAction();
        InitOk = false;
    }

    protected abstract void OnUninitAction();
    protected abstract void OnInitAction();

    private class Sub
    {
        internal static readonly T Instance = new T();
        static Sub() { }
    }
}