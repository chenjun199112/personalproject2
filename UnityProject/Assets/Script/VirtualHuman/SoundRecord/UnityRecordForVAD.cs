using UnityEngine;


internal sealed class UnityRecordForVAD : SingletonWithUpdate<UnityRecordForVAD>
{
    private UnityRecordForVADSub _innerRecord;

    protected override void OnPauseAction(bool state) { _innerRecord.Pause(state); }

    protected override void OnInitAction()
    {
        string[] devices = Microphone.devices;
        if (devices.Length == 0)
        {
            return;
        }
        _innerRecord = new UnityRecordForVADSub {PauseHosting = true};
    }

    protected override void OnUninitAction()
    {
        if (_innerRecord != null)
        {
            _innerRecord.Dispose();
            _innerRecord = null;
        }
    }

    public void StartRecord(BoolFloatBuffDelegate recordEndDelegate)
    {
        if (!InitOk)
        {
            return;
        }
        _innerRecord.StartRecordWithVAD(recordEndDelegate);
    }

    public void StopRecord()
    {
        if (!InitOk)
        {
            return;
        }
        _innerRecord.StopRecord();
    }
}