using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class UnityRecordForVADSub : DisposableScriptWithUpdate
{
    private int _currentRMS = -1;
    private int _currentRMSMax = -1;
    private int _currentRMSMin = -1;
    private int _lastFalseVadTimeStamp;
    private float[] _maxBuff;
    private int _maxBuffID;
    private AudioClip _recordClip;

    private BoolFloatBuffDelegate _recordEndDelegate;
    private int _savedSamplePosition;
    private float[] _tempFrameBuff;
    private float[] _tempAudioBuff=new float[3000];
    private int _tempFrameBuffID;
    private int _validFinalBuffSize;
    private int _tempAudioBuffSize=0;
    private uint _sampleRate=16000;
    private bool hasFoundHead = false;

    public const int MicroPhoneSampleRate = 16000;
    public const int MaxRecordBuffSecondLength = 15;

    private int SaveDataFromDevice(int startSamplePosition,
                                   int sampleSize,
                                   bool loopEnd)
    {
        
        if (sampleSize < _tempFrameBuff.Length)
        {
            return 0;
        }
  
        int validSavedTotalSize = 0;
        int loopFactor = sampleSize / _tempFrameBuff.Length;
      
        if (loopEnd)
        {
            if (sampleSize % _tempFrameBuff.Length > 0)
            {
                loopFactor++;
            }
        }
        for (int i = 0; i < loopFactor; i++)
        {
            if (!InRecord)
            {
                break;
            }
            bool state = _recordClip.GetData(_tempFrameBuff,
                                             startSamplePosition);
            if (state)
            {
                if (validSavedTotalSize + _tempFrameBuff.Length > sampleSize)
                {
                    SaveTempData(_tempFrameBuff,
                                 sampleSize - validSavedTotalSize);
                    validSavedTotalSize = sampleSize;
                }
                else
                {
                    SaveTempData(_tempFrameBuff,
                                 _tempFrameBuff.Length);
                    validSavedTotalSize += _tempFrameBuff.Length;
                }
                //Debug.LogError("validSavedTotalSize "+ validSavedTotalSize);
            }
            
            else
            {
                break;
            }
        }
        return validSavedTotalSize;
    }
    float count = 0;
    float count_1=0;
    int validCount=0;
    int frameCount = 0;
    int countFront = 0;
    int audiocount = 0;
    float environment_rms = 0;
    float sum_rms = 0;
    private void SaveTempData(float[] tempBuff,
                              int validSize)
    {
        frameCount += 1;
        float sum = 0;
        //Debug.LogError("validSize:----->" + validSize);
        short[] tempBuff_int16 = new short[8000];

        for (int j = 0; j < validSize; j++)
        {
            tempBuff_int16[j] = (short)(tempBuff[j] * Int16.MaxValue);
        }
       
        //降噪
        WordAlignBridge.c_sharp_noise_suppression(tempBuff_int16, _sampleRate, validSize, 3);
        for (int o = 0; o < validSize; o++)
        {
            tempBuff[o] = (float)(tempBuff_int16[o] * 1.0f / Int16.MaxValue);
        }

        //计算rms的值
        for (int i = 0; i < validSize; ++i)
        {
            sum += tempBuff[i] * tempBuff[i];
        }
        int rms = (int) (Mathf.Sqrt(sum / validSize)*1000 );
        if (_currentRMS == -1)
        {
            _currentRMS = _currentRMSMin = _currentRMSMax = rms;
//            Debug.Log("--vad1->" + _currentRMS + "  " + _currentRMSMin + "  " + _currentRMSMax);
        }
        else
        {
            _currentRMS = rms;
//            Debug.Log("--vad2->" + _currentRMS);
        }

        if (frameCount <= 4)
        {
            sum_rms = sum_rms + rms;
            if (frameCount == 4)
            {
                environment_rms = sum_rms / 4;
                sum_rms = 0;
            }
        }




        //Debug.LogError("rms---->" + rms);
        //静音超过5s 就停止。
        if (rms<environment_rms+8)
        {
            count=count+1;
            //Debug.LogError("count"+count);
            if(count>100)
            {
                count=0;
                frameCount = 0;
                WriteFinalBuff(tempBuff,
                    validSize,
                    true);
                return;
            }
       
        }
        //中间还有语音活动继续录音，将coun_1清零。
        else
        {
            count = 0;
            count_1=0;
        }

        if (rms > _currentRMSMax)
        {
            _currentRMSMax = rms;
        }
        if (rms < _currentRMSMin)
        {
            _currentRMSMin = rms;
        }
        int maxDelta = _currentRMSMax ;
        int minDelta = _currentRMS ;
        // 查找结尾，持续无效片段0.5s才会才终止
        if (maxDelta != 0)
        {
//            Debug.Log("--vad3->" + _currentRMS + "->" + maxDelta + " ->" + minDelta);
            float cp = minDelta * 1.0f / maxDelta;
            //Debug.LogError("cp--->"+cp);
            if (cp < 0.5f && maxDelta > environment_rms+15 && validCount>2)
            {
                count_1+=1;
                if (count_1>17)
                {
                    count=0;
                    count_1=0;
                    hasFoundHead = false;
                    validCount = 0;
                    frameCount = 0;
                    WriteFinalBuff(tempBuff,
                                validSize,
                                true);
                    return;
                }
            }
        }

        //查找开头
        if (rms > environment_rms && hasFoundHead == false)
        {
            validCount = validCount + 1;
            Array.Copy(tempBuff,
                0,
                _tempAudioBuff,
                _tempAudioBuffSize,
                validSize);
            _tempAudioBuffSize += validSize;
            if (validCount > 2)
            {
               
                hasFoundHead = true;
                WriteFinalBuff(_tempAudioBuff,
                        _tempAudioBuffSize,
                        false);
                _tempAudioBuffSize = 0;
                Array.Clear(_tempAudioBuff, 0, _tempAudioBuff.Length);
            }

        }
   
        //Debug.LogError(hasFoundHead);
        //找到开头，储存有效的语音片段
        if (rms > 0 &&  hasFoundHead == true)
        {
        // Debug.LogError(" tempBuff"+ tempBuff.Length);
            WriteFinalBuff(tempBuff,
                        validSize,
                        false);
        }
    }

    private void WriteFinalBuff(float[] tempBuff,
                                int validSize,
                                bool forceEnd)
    {
        if (_validFinalBuffSize + validSize <= _maxBuff.Length)
        {
            Array.Copy(tempBuff,
                       0,
                       _maxBuff,
                       _validFinalBuffSize,
                       validSize);
            _validFinalBuffSize += validSize;
        }
        else
        {
            int size = _maxBuff.Length - _validFinalBuffSize;
            Array.Copy(tempBuff,
                       0,
                       _maxBuff,
                       _validFinalBuffSize,
                       size);
            _validFinalBuffSize = _maxBuff.Length;
        }
        if (forceEnd || _validFinalBuffSize == _maxBuff.Length)
        {
            if (_recordEndDelegate != null)
            {
                _recordEndDelegate(true,
                                   _maxBuff,
                                   _validFinalBuffSize);
            }
            StopRecord();
        }
    }

#region
    public bool InRecord { get; private set; }

    // private Queue q = new Queue(); 

    protected override void OnLateUpdateAction()
    {
        if (!InRecord)
        {
            return;
        }
        int currentSamplePosition = Microphone.GetPosition(null);
        if (currentSamplePosition <= 0)
        {
            return;
        }
        // if (xxx)
        // {
        //     q.Enqueue();
        // }
        // if (10)
        // {
        //     for (int i = 0; i 《10； i++))
        //     q.Dequeue(0);

        // }
        if (_savedSamplePosition < currentSamplePosition)
        {
            int validSize = SaveDataFromDevice(_savedSamplePosition,
                                               currentSamplePosition - _savedSamplePosition,
                                               false);
            _savedSamplePosition += validSize;
            return;
        }
        if (_savedSamplePosition > currentSamplePosition)
        {
            int validSize = SaveDataFromDevice(_savedSamplePosition,
                                               MicroPhoneSampleRate - _savedSamplePosition,
                                               true);
            _savedSamplePosition += validSize;
            if (_savedSamplePosition != MicroPhoneSampleRate)
            {
                Debug.LogError("[ERROR] record audio failed");
                return;
            }
            validSize = SaveDataFromDevice(0,
                                           currentSamplePosition,
                                           false);
            _savedSamplePosition = validSize;
        }
    }

    public bool StartRecordWithVAD(BoolFloatBuffDelegate recordEndDelegate)
    {
        if (Disposed)
        {
            return false;
        }
        if (InRecord)
        {
            return true;
        }
        InRecord = true;
        _currentRMS = -1;
        _currentRMSMax = -1;
        _currentRMSMin = -1;
        _recordEndDelegate = recordEndDelegate;
        _recordClip = Microphone.Start(null,
                                       true,
                                       1,
                                       MicroPhoneSampleRate);//录音开始，第三个参数是录音的时长每次。
        _validFinalBuffSize = 0;
        _savedSamplePosition = 0;
        return true;
    }

    public void StopRecord()
    {
        if (Disposed)
        {
            return;
        }
        if (!InRecord)
        {
            return;
        }
        InRecord = false;
        Microphone.End(null);
        Object.Destroy(_recordClip);
        _recordClip = null;
        count=0;
    }

    protected override void OnInitAction()
    {
        _maxBuffID =
                VariableLengthArray<float>.Cache.AccurateAlloc(MicroPhoneSampleRate *
                                                           MaxRecordBuffSecondLength,
                                                           out _maxBuff);
        _tempFrameBuffID = VariableLengthArray<float>.Cache.AccurateAlloc(MicroPhoneSampleRate / 25,
                                                                      out _tempFrameBuff);
    }

    protected override void OnUninitAction()
    {
        StopRecord();
        VariableLengthArray<float>.Cache.Free(_tempFrameBuffID);
        VariableLengthArray<float>.Cache.Free(_maxBuffID);
    }

    protected override void OnPauseAction(bool state)
    {
        if (state)
        {
            StopRecord();
            if (_recordEndDelegate != null)
            {
                _recordEndDelegate(false,
                                   null,
                                   0);
            }
        }
    }
#endregion
}