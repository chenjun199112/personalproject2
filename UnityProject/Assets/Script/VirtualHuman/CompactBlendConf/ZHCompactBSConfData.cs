﻿
using System.Collections.Generic;
using UnityEngine;

public class ZHCompactBSConfData : ScriptableObject
{
    public List<CompactBlendData> Data;

    public CompactBlendData Get(DefinedZHCompactBlendEnum id)
    {
        for (int i = 0; i < Data.Count; i++)
        {
            if (Data[i].ID == (int) id)
            {
                return Data[i];
            }
        }
        return null;
    }
}