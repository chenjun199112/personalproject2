﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZHEmotionCompactBSConfDataDic : ScriptableObject
{
    public Dictionary<DefinedEmotionEnum, ZHCompactBSConfData> Dic = new Dictionary<DefinedEmotionEnum, ZHCompactBSConfData>();

    public EmotionCompactBlendDataDic Get(DefinedZHCompactBlendEnum id)
    {
        EmotionCompactBlendDataDic result = new EmotionCompactBlendDataDic();

        List<DefinedEmotionEnum> Keys = new List<DefinedEmotionEnum>(Dic.Keys);

        for (int i = 0; i < Dic.Count; i++)
        {
            result.Dic[Keys[i]] = Dic[Keys[i]].Get(id);
        }
        return result;
    }
}
