﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionCompactBlendDataDic : ScriptableObject
{
    public Dictionary<DefinedEmotionEnum, CompactBlendData> Dic = new Dictionary<DefinedEmotionEnum, CompactBlendData>();
}
