﻿using System;
using System.Collections.Generic;

[Serializable]
public class CompactBlendData
{
    public List<BlendConfData> ConfList;
    public int ID;

    public int IndexOfName(string blendName)
    {
        for (int i = 0; i < ConfList.Count; i++)
        {
            if (ConfList[i].Name == blendName)
            {
                return i;
            }
        }
        return -1;
    }

    public BlendConfData ConfDataOfName(string blendName)
    {
        for (int i = 0; i < ConfList.Count; i++)
        {
            if (ConfList[i].Name == blendName)
            {
                return ConfList[i];
            }
        }
        return null;
    }

    [Serializable]
    public class BlendConfData
    {
        public float ConfWeight;
        public string Name;
    }

    public static CompactBlendData operator + (CompactBlendData a, CompactBlendData b)
    {
        CompactBlendData c = new CompactBlendData();
        c.ConfList = new List<BlendConfData>();
        if (a !=null && b != null && a.ID == b.ID)
        {
            c.ID = a.ID;

            if (a.ConfList != null)
            {
                for (int i = 0; i < a.ConfList.Count; i++)
                {
                    BlendConfData blendConf = new BlendConfData();
                    blendConf.Name = a.ConfList[i].Name;
                    blendConf.ConfWeight = a.ConfList[i].ConfWeight;
                    c.ConfList.Add(blendConf);
                }
            }

            if (b.ConfList != null)
            {
                for (int i = 0; i < b.ConfList.Count; i++)
                {
                    if (c.IndexOfName(b.ConfList[i].Name) > -1)
                    {
                        c.ConfList[i].ConfWeight = c.ConfList[i].ConfWeight + b.ConfList[i].ConfWeight;
                    }
                    else
                    {
                        BlendConfData blendConf = new BlendConfData();
                        blendConf.Name = b.ConfList[i].Name;
                        blendConf.ConfWeight = b.ConfList[i].ConfWeight;
                        c.ConfList.Add(blendConf);
                    }
                }
            }
            
        }
        return c;
    }

    public static CompactBlendData operator *(CompactBlendData a, float b)
    {
        CompactBlendData c = new CompactBlendData();
        c.ConfList = new List<BlendConfData>();
        if (a != null)
        {
            c.ID = a.ID;
            for (int i = 0; i < a.ConfList.Count; i++)
            {
                BlendConfData blendConf = new BlendConfData();
                blendConf.Name = a.ConfList[i].Name;
                blendConf.ConfWeight = a.ConfList[i].ConfWeight * b;
                c.ConfList.Add(blendConf);
            }
        }
        
        return c;
    }
}