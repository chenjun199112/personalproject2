﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionMarker
{
    public List<EmotionMarkerData> EmotionMarkerList = new List<EmotionMarkerData>();
    public List<int> EmotionMarkerIndexList;
    private float _totalTime = 1;
    private float _intervalTime = 0.1f;

    public EmotionMarker(float totalTime)
    {
        if (totalTime > 0)
            _totalTime = totalTime;

        GenerateEmotionMarkerList();
        GenerateEmotionMarkerIndexList();
    }

    public void GenerateEmotionMarkerList()
    {
        EmotionMarkerList.Clear();
        int test = 0;
        if (test == 0)
        {
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.DEFAULT, 0, _totalTime / 8));
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.SMILE, _totalTime / 4, _totalTime * 3 / 8));
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.GRID, _totalTime / 2, _totalTime * 5 / 8));
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.SAD, _totalTime * 3 / 4, _totalTime * 7 / 8));
        }
        else
        {
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.DEFAULT, 0, 0));
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.SMILE, _totalTime / 4, 0));
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.GRID, _totalTime / 2, 0));
            EmotionMarkerList.Add(new EmotionMarkerData(DefinedEmotionEnum.SAD, _totalTime * 3 / 4, 0));
        }



    }

    public void GenerateEmotionMarkerIndexList()
    {
        
        if (_totalTime <= 0)
            return;

        if (EmotionMarkerList.Count <= 0)
            return;

        

        _intervalTime = _totalTime * 2 / EmotionMarkerList.Count;

        EmotionMarkerIndexList = new List<int>(new int[EmotionMarkerList.Count]);

        for (int i = 0; i < EmotionMarkerList.Count; i++)
        {
            EmotionMarkerData data = EmotionMarkerList[i];
            EmotionMarkerIndexList[Mathf.CeilToInt(data.startTime / _intervalTime)] = i;
        }
    }

    public int GetIndexByCurTime(float curTime)
    {
        int index = 0;
        if (_intervalTime > 0)
        {
            index = Mathf.FloorToInt(curTime / _intervalTime);
        }

        for (int i = index + 1; i < EmotionMarkerList.Count; i++)
        {
            if (curTime >= EmotionMarkerList[i].startTime)
                index = i;
            else
                break;
        }

        return index;
    }

    public List<EmotionRatioData> GetEmotionRatioDataByCurTime(float curTime)
    {
        List < EmotionRatioData > datas = new List<EmotionRatioData>();

        int index = GetIndexByCurTime(curTime);

        if (index >= 0 && index < EmotionMarkerList.Count)
        {
            EmotionMarkerData data = EmotionMarkerList[index];
            if (index == 0 || curTime > data.strokeTime || data.strokeTime < data.startTime)
            {
                datas.Add(new EmotionRatioData(data.emotion, 1));
            }
            else
            {
                EmotionMarkerData preData = EmotionMarkerList[index - 1];
                datas.Add(new EmotionRatioData(preData.emotion, (curTime - data.startTime) /(data.strokeTime - data.startTime)));
                datas.Add(new EmotionRatioData(data.emotion, (data.strokeTime - curTime) / (data.strokeTime - data.startTime)));
            }
        }

        return datas;
    }
}

public struct EmotionMarkerData
{
    public DefinedEmotionEnum emotion;
    public float startTime;
    public float strokeTime;

    public EmotionMarkerData(DefinedEmotionEnum emotion, float startTime, float strokeTime)
    {
        this.emotion = emotion;
        this.startTime = startTime;
        this.strokeTime = strokeTime;
    }
}

public struct EmotionRatioData
{
    public DefinedEmotionEnum emotion;
    public float ratio;

    public EmotionRatioData(DefinedEmotionEnum emotion, float ratio)
    {
        this.emotion = emotion;
        this.ratio = ratio;
    }
}

