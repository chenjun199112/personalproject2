﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DefinedEmotionEnum
{
    DEFAULT,
    SMILE,
    GRID,
    SAD,

    MAX
}
