using System.Collections.Generic;
using Microsoft.International.Converters.PinYinConverter;
using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using Xunfei;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;


public class Spell
{
    public enum SpellOptions
    {
        FirstLetterOnly = 1,
        EnableUnicodeLetter = 2
    }

    private static readonly Dictionary<char, string> ErrorCharList = new Dictionary<char, string> {
                                                                                                          {'家', "JIA1"},
                                                                                                          {'万', "WAN4"},
                                                                                                          {'说', "SHUO1"},
                                                                                                          {'遗', "YI2"},
                                                                                                          {'熟', "SHU2"},
                                                                                                          {'许', "XU3"},
                                                                                                          {'地', "DI4"},
                                                                                                          {'提', "TI2"},
                                                                                                          {'的', "DE1"},
                                                                                                  };

    public static void EngMakeSpell(string strEnglish, ref BetterList<BetterList<string>> listAlllist, ref BetterList<BetterList<string>> listFinallist,ref BetterList<int> splitindexlist,ref BetterList<string> listphoneformantlist)
    {
        if (!LoadEnSpellData.bLoaded)
            return;

        strEnglish = strEnglish.ToUpper();
        bool g = IsinList(strEnglish.Substring(strEnglish.Length - 1, 1), bd);
        if (g)
        {
            strEnglish = strEnglish;
        }
        else
        {
            strEnglish = strEnglish + ".";
        }
        string new_word = strEnglish;
        int count_vowel = 0;
       
        foreach (var n in strEnglish)
        {
            string new_n = n.ToString();
            bool c = IsinList(new_n, bd);
            if (c)
            {
                int new_word_length = new_word.Length;
                string[] sarray = new_word.Split(new string[] { new_n }, StringSplitOptions.RemoveEmptyEntries);
                new_word = new_word.Substring(sarray[0].Length + 1, new_word_length - sarray[0].Length - 1);
                BetterList<string> splitlist = new BetterList<string>();
                //Debug.LogError(new_word);
                string[] result = Regex.Split(sarray[0], " ", RegexOptions.IgnoreCase);
               
                foreach (string i in result)
                {
                    if (i != "" && i != " ")
                    {


                        BetterList<string> singlelist = new BetterList<string>();
                        BetterList<string> wordphonelist = new BetterList<string>();
                        int phonmeindex = LoadEnSpellData.itemDate.keydata.IndexOf(i);

                        List<string> phonelist = null;
                        if (phonmeindex >= 0)
                        {
                            phonelist = LoadEnSpellData.itemDate.valuedata[phonmeindex].data;
                        }

                        else if (i != "")
                        {

                            Debug.LogError("词典中不存在:" + i );

                          
                            int num_string = i.Length;
                            if (num_string <= 3)
                            {
                                phonelist = new List<string> { "AH" };
                            }
                            else if (num_string > 3 && num_string <= 6)
                            {
                                phonelist = new List<string> { "AA", "L" };

                            }
                            else if (num_string > 6)
                            {
                                phonelist = new List<string> { "CH", "AA", "S" };
                            }

                        }

                        foreach (string item in phonelist)
                        {
                            string new_item;
                            bool b = IsinList(item.Substring(item.Length - 1, 1), numlist);
                            if (b)
                            {
                                new_item = item.Substring(0, item.Length - 1);
                            }
                            else
                            {
                                new_item = item.Substring(0, item.Length);
                            }
                            wordphonelist.Add(new_item);
                            bool a = IsinList(new_item, list_vowel);
                            if (a)
                            {
                                listphoneformantlist.Add(new_item);
                                singlelist.Add(new_item);
                                splitlist.Add(new_item);
                                count_vowel += 1;
                                //Debug.LogError(item);

                            }
                            bool d = IsinList(new_item, list_consonant);
                            if (d)
                            {
                                if (new_item == phonelist[phonelist.Count - 1] && singlelist.size != 0 && count_vowel < 3)
                                {
                                    listphoneformantlist.Add(new_item);
                                    singlelist.Add(new_item);
                                    splitlist.Add(new_item);
                                }
                            }
                            bool e = IsinList(new_item, list_qing);
                            if (e)
                            {
                                if (new_item == phonelist[0])
                                {
                                    listphoneformantlist.Add(new_item);
                                    singlelist.Add(new_item);
                                    splitlist.Add(new_item);
                                }
                            }
                            count_vowel = 0;
                        }

                        listFinallist.Add(singlelist);
                        listAlllist.Add(wordphonelist);
                    }
                }
                
                splitindexlist.Add(splitlist.size);
                //singlelist.Clear();
                //wordphonelist.Clear();

            }
        }

        Debug.LogError("listphoneformantlist.size " + listphoneformantlist.size);
        for (int i = 0; i < listphoneformantlist.size; i++)
        {
            Debug.LogError("listphoneformantlist " + listphoneformantlist[i]);
        }
    }

    public static bool IsinList(string item,
                            string[] needlist)
    {
        for (int i = 0; i < needlist.Length; i++)
        {
            if (item == needlist[i])
            {
                return true;
            }

        }
        return false;
    }
    private static readonly string[] list_vowel ={"AA","AE","AH","AO","AW","AY",
        "EH","ER","EY",
        "IY", "UW", "IH", "UH",  "OW",          
            "OY"
    };
    private static readonly string[] list_consonant ={"D", "T", "K", "L", "Z", "S", "V","N","NG"};
    private static readonly string[] list_qing = { "S", "CH", "TH", "B" ,"P","DH","J"};
    private static readonly string[] bd = { "\\", "/", ":", "：", "•", "，", "。", ".", "、", "!", "！", "；", ";", "？", "*", "?", "\"", "<", ">", "|", "：", ",", "." };
    private static readonly string[] numlist = { "0", "1", "2" };
    //private static readonly BetterList<string> singlelist = new BetterList<string>();
    //private static readonly BetterList<string> wordphonelist = new BetterList<string>();



    public static void MakeSpell(string strChinese, SpellOptions options, ref BetterList<string> resultStringList,
                                 ref BetterList<int> punctuationIndexList)
    {
        for (int i = 0; i < strChinese.Length; i++)
        {
            bool isChineses = ChineseChar.IsValidChar(strChinese[i]);
            if (isChineses)
            {
                string charPinyin = string.Empty;
                using (Dictionary<char, string>.Enumerator enumerator = ErrorCharList.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        if (enumerator.Current.Key == strChinese[i])
                        {
                            charPinyin = enumerator.Current.Value;
                        }
                    }
                }
                if (string.IsNullOrEmpty(charPinyin))
                {
                    ChineseChar chineseChar = new ChineseChar(strChinese[i]);
                    charPinyin = chineseChar.Pinyins[0];
                }
                if (!string.IsNullOrEmpty(charPinyin))
                {
                    if (options == SpellOptions.EnableUnicodeLetter)
                    {
                        resultStringList.Add(charPinyin.ToLower());
                    }
                    else
                    {
                        charPinyin = charPinyin.Substring(0,
                                                          1);
                        resultStringList.Add(charPinyin.ToLower());
                    }
                }
            }
            else
            {
                punctuationIndexList.Add(resultStringList.size - 1);
            }
        }
    }
}