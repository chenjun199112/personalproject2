﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class LoadEnSpellData
{
    [System.Serializable]
    public class RootObject
    {
        public List<string> keydata;
        public List<StringArray> valuedata;
    }
    [System.Serializable]
    public class StringArray
    {
        public List<string> data;
    }

    public static RootObject itemDate { get; private set; }
    public static bool bLoaded { get; private set; }

    public static void Load()
    {

        string path = Application.streamingAssetsPath + "/EnLipSync/test.json";
        string jsonString = File.ReadAllText(path.Replace("/", "//"));
        
        itemDate = JsonUtility.FromJson<RootObject>(jsonString);
        bLoaded = true;

        /* for (int i = 0; i < itemDate.valuedata.Count; i++)
        //for (int i = 0; i < 10; i++)
         {
             string value = "";
             for (int j = 0; j < itemDate.valuedata[i].data.Count; j++)
             {
                 value += itemDate.valuedata[i].data[j];
                 if (j < itemDate.valuedata[i].data.Count - 1)
                 {
                     value += " ";
                 }
             }
            WriteMessage( "\"" + value + "\",");
        }*/
    }

    public static void WriteMessage(string msg)
                {
                    using (FileStream fs = new FileStream(@"d:\value.txt", FileMode.OpenOrCreate, FileAccess.Write))
                      {
                            using (StreamWriter sw = new StreamWriter(fs))
                                {
                                    sw.BaseStream.Seek(0, SeekOrigin.End);
                                    sw.WriteLine("{0}", msg);
                                    sw.Flush();
                                }
                        }
                }

}
