﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;

internal class TempGameData : Singleton<TempGameData>
{
    public Dictionary<CharacterID, ZHEmotionCompactBSConfDataDic> ZHCompactBSConfDataDic { get; private set; }
    public AnimSectionLayoutMask CharacterAnimMaskData { get; private set; }


    protected override void OnInitAction()
    {
        ZHCompactBSConfDataDic = new Dictionary<CharacterID, ZHEmotionCompactBSConfDataDic>();
        for (int i = 1; i < Enum.GetNames(typeof(CharacterID)).Length; i++)
        {
            string path = string.Empty;
            ZHEmotionCompactBSConfDataDic ZHEmotionDic = new ZHEmotionCompactBSConfDataDic();
            for (int j = 0; j < Enum.GetNames(typeof(DefinedEmotionEnum)).Length - 1; j++)
            {
                path = typeof(ZHCompactBSConfData) + "/" + (CharacterID)i + "_" + (DefinedEmotionEnum)j;
                ZHCompactBSConfData bs = Resources.Load<ZHCompactBSConfData>(path);
                if (bs != null)
                {
                    ZHEmotionDic.Dic[(DefinedEmotionEnum) j] = bs;
                }
            }
            ZHCompactBSConfDataDic[(CharacterID)i] = ZHEmotionDic;

        }
        string asPath = typeof(AnimSectionLayoutMask) + "/common";
        AnimSectionLayoutMask mask = Resources.Load<AnimSectionLayoutMask>(asPath);
        if (mask != null)
        {
            CharacterAnimMaskData = mask;
        }

        InitData();
    }

    protected override void OnUninitAction()
    {
        for (int i = 1; i < Enum.GetNames(typeof(CharacterID)).Length - 1; i++)
        {
            if (ZHCompactBSConfDataDic.ContainsKey((CharacterID) i))
            {
                for (int j = 0; j < Enum.GetNames(typeof(DefinedEmotionEnum)).Length - 1; j++)
                {
                    if (ZHCompactBSConfDataDic[(CharacterID)i].Dic.ContainsKey((DefinedEmotionEnum)j))
                        Resources.UnloadAsset(ZHCompactBSConfDataDic[(CharacterID)i].Dic[(DefinedEmotionEnum)j]);
                }   
            }

            if (CharacterAnimMaskData != null)
            {
                Resources.UnloadAsset(CharacterAnimMaskData);
            }
        }
        ZHCompactBSConfDataDic.Clear();
        ZHCompactBSConfDataDic = null;
        CharacterAnimMaskData = null;
        UninitData();
    }

    private void InitData()
    {
        SerializationData<AnimSection>.Instance.Init();
        ///////////////////////////
        VariableLengthArray<float>.Init(20);
        VariableLengthArray<byte>.Init(20);
        BasicClassInstance<AnimationCurve>.Init(100);
        BasicClassInstance<Dictionary<string, AnimationCurve>>.Init(20);
        BasicClassInstance<StringBuilder>.Init(20);
        BasicClassInstance<BetterList<int>>.Init(20);
        FixedLengthArray<AnimationCurve>.Init(1,
                                              1,
                                              100);
        FixedLengthArray<float>.Init(1,
                                     1,
                                     500);
        ClassInstance<GGAnimatorRuntimeTask>.Init(50);
        ClassInstance<GGAnimatorRuntimeTaskData>.Init(50);
        ClassInstance<BlendMarker>.Init(200);
        ClassInstance<BlendMarker>.Init(200);
        ClassInstance<AnimSectionTask>.Init(20);
        GetSerializationData();
    }

    private void GetSerializationData()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        IList<string> dataNames = SectionBuilder.GetAllDataName();
        for (int i = 0; i < dataNames.Count; i++)
        {
            AnimSection section;
            SerializationData<AnimSection>.Instance.Get(CharacterID.ALN.ToString(),
                                                        dataNames[i],
                                                        out section);
        }
        stopwatch.Stop();
        Debug.Log("ss data: ms :" + stopwatch.ElapsedMilliseconds);
    }

    private void UninitData()
    {
        SerializationData<AnimSection>.Instance.Uninit();
        VariableLengthArray<float>.Uninit();
        VariableLengthArray<byte>.Uninit();
        BasicClassInstance<AnimationCurve>.Uninit();
        BasicClassInstance<Dictionary<string, AnimationCurve>>.Uninit();
        BasicClassInstance<StringBuilder>.Uninit();
        BasicClassInstance<BetterList<int>>.Uninit();
        FixedLengthArray<AnimationCurve>.Uninit();
        FixedLengthArray<float>.Uninit();
        ClassInstance<GGAnimatorRuntimeTask>.Uninit();
        ClassInstance<GGAnimatorRuntimeTaskData>.Uninit();
        ClassInstance<BlendMarker>.Uninit();
        ClassInstance<AnimSectionTask>.Uninit();
    }
}