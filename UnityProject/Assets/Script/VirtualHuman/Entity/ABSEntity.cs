﻿using UnityEngine;

public abstract class ABSEntity : DisposableScriptWithUpdate
{
    public delegate void ChatCallback(ChatState state);

    public enum ChatState
    {
        Failed,
        OnSoundRecordEnd,
        OnAIChatNoResponse,
        OnAIChatNetError,
        OnAIChatSuccess,
        OnSpeechAlignEnd,
        OnSpeechEnd
    }

    protected ChatCallback _chatCallback;

    protected ABSEntity(Transform trans)
    {
        SelfTransform = trans;
        SelfGameObject = trans.gameObject;
    }

    public bool IsActive { get; protected set; }

    public Transform SelfTransform { get; private set; }
    public GameObject SelfGameObject { get; private set; }

    protected override void OnDisposeManagedData()
    {
        SelfTransform = null;
        SelfGameObject = null;
    }

    public void Enable()
    {
        if (IsActive)
        {
            return;
        }
        IsActive = true;
        OnEnable();
    }

    protected virtual void OnEnable() { }

    public void Disable()
    {
        if (!IsActive)
        {
            return;
        }
        IsActive = false;
        OnDisable();
    }

    protected virtual void OnDisable() { }

    public virtual void StartListen(ChatCallback callback) { }

    public virtual void StartWalk(Vector3 targetPosition,
                                  Vector3 forward,
                                  IntDelegate endDelegate = null)
    {
    }

    public virtual void StopWalk() { }

    public virtual void KeepGapWithCamera() { }

    public virtual void OnScaleBegan() { }
    public virtual void OnScaleEnded() { }
}