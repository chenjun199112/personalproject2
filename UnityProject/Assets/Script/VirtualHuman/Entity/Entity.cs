﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Entity : ABSEntity
{
    private readonly GGAnimator _animator;
    private readonly XFChat _chat;
    private readonly SectionBuilder _sectionBuilder;
    private readonly List<float> _timeList;
    private readonly ZHWordCurveMaker _zhCurveMaker;

    public CharacterID RoleID { get; private set; }

    public BlendShapeSystem BlendSystem { get; private set; }
    public BoneSystem BoneSystem { get; private set; }

    private string _currentWordSentence;
    private ChatState _nlpState;
    private AudioClipPlayer _audioClip;
    private AudioFilePlayer _audioFile;

    public Entity(CharacterID id,
                  Transform trans) : base(trans)
    {
        _timeList = new List<float>();
        RoleID = id;
        BoneSystem = SelfTransform.GetComponent<BoneSystem>();
        BlendSystem = SelfTransform.GetComponent<BlendShapeSystem>();
        _sectionBuilder = new SectionBuilder(RoleID.ToString());
        _animator = new GGAnimator(RoleID.ToString(),
                                   TempGameData.Instance.CharacterAnimMaskData,
                                   BoneSystem,
                                   BlendSystem) {
                                                        PauseHosting = true
                                                };
        _zhCurveMaker = new ZHWordCurveMaker(TempGameData.Instance.ZHCompactBSConfDataDic[RoleID]);
        _chat = new XFChat(XFChat.RawType.PCM_F32_NORMAL);
        SelfGameObject.SetActive(false);
    }

    protected override void OnUninitAction()
    {
        OnDisable();
        _timeList.Clear();
        _sectionBuilder.Dispose();
        _chat.Dispose();
        _animator.Dispose();
        _zhCurveMaker.Dispose();
    }

    protected override void OnPauseAction(bool state)
    {
        _animator.Pause(state);
        if (_audioFile != null)
        {
            _audioFile.Pause(state);
        }
        if (_audioClip != null)
        {
            _audioClip.Pause(state);
        }
    }

    protected override void OnEnable()
    {
        StartIdleAnim();
        SelfGameObject.SetActive(true);
        //Vector3 forward = GameData.Instance.MainCamera.transform.forward * -1;
        //forward.y = 0;
        //SelfTransform.forward = forward;
    }

    protected override void OnDisable()
    {
        SelfGameObject.SetActive(false);
        if (_audioFile != null)
        {
            _audioFile.Dispose();
            _audioFile = null;
        }
        if (_audioClip != null)
        {
            _audioClip.Dispose();
            _audioClip = null;
        }
    }

#region
    private bool inIdle;
    private bool inListen;
    private float _taskLength;
    private float _time;
    private bool _firstTrigger;

    private float PopIdleAnim()
    {
        AnimSectionTask task = _sectionBuilder.DumpIdleTask();
        float taskLength = task.Length;
        _animator.PlayTask(task);
        _sectionBuilder.ReleaseSectionTask(task);
        return taskLength;
    }

    private float PopListenAnim()
    {
        AnimSectionTask task = _sectionBuilder.DumpListenTask();
        float taskLength = task.Length;
        _animator.PlayTask(task);
        _sectionBuilder.ReleaseSectionTask(task);
        return taskLength;
    }

    protected override void OnUpdateAction()
    {

        if (!IsActive)
        {
            return;
        }
        if (inIdle)
        {
            if (_firstTrigger)
            {
                _firstTrigger = false;
                _taskLength = PopIdleAnim();
                _time = 0;
            }
            _time += Time.deltaTime;
            if (_taskLength-0.5 <= _time)
            {
                _taskLength = PopIdleAnim();
                _time = 0;
            }
            return;
        }
        if (inListen)
        {
            if (_firstTrigger)
            {
                _firstTrigger = false;
                _taskLength = PopListenAnim();
                _time = 0;
            }
            _time += Time.deltaTime;
            if (_taskLength - 0.5 <= _time)
            {
                _taskLength = PopListenAnim();
                _time = 0;
            }
        }
    }

    private void StartIdleAnim()
    {
        inIdle = true;
        inListen = false;
        _firstTrigger = true;
    }

    private void StartListenAnim()
    {
        inIdle = false;
        inListen = true;
        _firstTrigger = true;
    }

    private void StartSpeekAnim()
    {
        inIdle = false;
        inListen = false;
    }
#endregion
#region
    private void OnRecordAudioEnd(bool state,
                                  float[] buff,
                                  int size)
    {
        if (!IsActive || !state)
        {
            RunChatCallback(ChatState.Failed);
            return;
        }
        RunChatCallback(ChatState.OnSoundRecordEnd);
        _chat.AIChat(buff,
                     size,
                     OnAIChatCallback);
    }



    private void OnAIChatCallback(int state,
                                  int hash,
                                  string resultString)
    {
        if (!IsActive)
        {
            RunChatCallback(ChatState.Failed);
            return;
        }
        if (state == (int) XFChat.State.XFTTSSuccess)
        {
            RunChatCallback(ChatState.OnAIChatSuccess);
            _nlpState = ChatState.OnAIChatSuccess;
            _currentWordSentence = resultString;
            _audioFile = new AudioFilePlayer(Application.persistentDataPath + "/tts.wav",
                                             AudioType.WAV,
                                             false,
                                             OnSpeekEnd,
                                             OnExtractAudioSampleOut) {
                                                                              PauseHosting = true
                                                                      };
            return;
        }
        if (state == (int) XFChat.State.NetworkError)
        {
            RunChatCallback(ChatState.OnAIChatNetError);
            _nlpState = ChatState.OnAIChatNetError;
            _currentWordSentence = Common.gameSettings.NetwordErrorAudioClip.name;
            _audioClip = new AudioClipPlayer(Common.gameSettings.NetwordErrorAudioClip,
                                             false,
                                             OnSpeekEnd) {
                                                                 PauseHosting = true
                                                         };
            OnExtractAudioSampleOut(_audioClip.PcmF32Samples);
            return;
        }
        if (Common.gameSettings.NoneResponseClips.Count > 0)
        {
            RunChatCallback(ChatState.OnAIChatNoResponse);
            _nlpState = ChatState.OnAIChatNoResponse;
            int index = UnityEngine.Random.Range(0,
                                     Common.gameSettings.NoneResponseClips.Count);
            _currentWordSentence = Common.gameSettings.NoneResponseClipsName[index];
            _audioClip = new AudioClipPlayer(Common.gameSettings.NoneResponseClips[index],
                                             false,
                                             OnSpeekEnd) {
                                                                 PauseHosting = true
                                                         };
            OnExtractAudioSampleOut(_audioClip.PcmF32Samples);
        }
    }

    private void OnExtractAudioSampleOut(float[] samples)
    {
        if (!IsActive)
        {
            RunChatCallback(ChatState.Failed);
            return;
        }

        float TotalTime = 1;
        if (_audioFile != null)
            TotalTime = _audioFile.TotalTime;
        _zhCurveMaker.Asyn(_currentWordSentence,
                          samples,
                          samples.Length,
                          TotalTime,
                          OnMakeWordCurveEnd);
    }

    private void OnMakeWordCurveEnd(bool state)
    {
        if (!IsActive || !state)
        {
            RunChatCallback(ChatState.Failed);
            return;
        }
        RunChatCallback(ChatState.OnSpeechAlignEnd);
        StartSpeek();
    }

    private void OnSpeekEnd()
    {
        if (!IsActive)
        {
            RunChatCallback(ChatState.Failed);
            return;
        }
        RunChatCallback(ChatState.OnSpeechEnd);
    }

#region
    public override void StartListen(ChatCallback callback)
    {
        if (!IsActive)
        {
            RunChatCallback(ChatState.Failed);
            return;
        }
        _chatCallback = callback;
        if (_audioFile != null)
        {
            _audioFile.Dispose();
            _audioFile = null;
        }
        if (_audioClip != null)
        {
            _audioClip.Dispose();
            _audioClip = null;
        }
        StartListenAnim();
        UnityRecordForVAD.Instance.StartRecord(OnRecordAudioEnd);
    }

    private void StartSpeek()
    {
        float audioLength = 0;
        if (_audioFile != null)
        {
            _audioFile.Play();
            audioLength = _audioFile.TotalTime + 0.1f;
        }
        if (_audioClip != null)
        {
            _audioClip.Play();
            audioLength = _audioClip.TotalTime + 0.1f;
        }
        int startTime = 0;
        //////////////////////////
        _timeList.Clear();
        for (int i = 0; i < _zhCurveMaker.EmphasizeTonePositionList.size; i++)
        {
            _timeList.Add(_zhCurveMaker.EmphasizeTonePositionList[i].StartTime);
        }

        if (_timeList.Count > 0)
        {
            AnimSectionTask task = _sectionBuilder.DumpNodTask(_timeList);
            _animator.PlayTask(task);
            _sectionBuilder.ReleaseSectionTask(task);
        }
        //////////////////////////
        _timeList.Clear();
        for (int i = 0; i < _zhCurveMaker.GesturePositionList.size; i++)
        {
            _timeList.Add(_zhCurveMaker.GesturePositionList[i].StartTime);
        }


        if (_timeList.Count > 0)
        {
            AnimSectionTask task = _sectionBuilder.DumpGestureTask(_timeList);
            _animator.PlayTask(task);
            _sectionBuilder.ReleaseSectionTask(task);
        }
        //////////////////////////
        if (_nlpState == ChatState.OnAIChatNetError || _nlpState == ChatState.OnAIChatNoResponse)
        {
            AnimSectionTask task = _sectionBuilder.DumpYaoTouTask(startTime);
            _animator.PlayTask(task);
            _sectionBuilder.ReleaseSectionTask(task);
        }
        //////////////////////////
        startTime = 0;
        for (float i = startTime; i < audioLength; i = startTime)
        {

            AnimSectionTask task = _sectionBuilder.DumpZhaYanTask(startTime);
            _animator.PlayTask(task);
            _sectionBuilder.ReleaseSectionTask(task);

            startTime += UnityEngine.Random.Range(2,
                                      3);
        }

        _animator.PlayMusleFaceSection(_zhCurveMaker.BlendCurve,
                                       audioLength,
                                       false);

        StartSpeekAnim();
    }

    private void RunChatCallback(ChatState state)
    {
        if (state == ChatState.Failed || state == ChatState.OnSpeechEnd)
        {
            StartIdleAnim();
        }
        if (state == ChatState.OnSoundRecordEnd)
        {
            //_listenBMLPump.StopExecute();
        }
        if (_chatCallback != null)
        {
            _chatCallback(state);
        }
    }
#endregion
#endregion
}

[Serializable]
public enum CharacterID
{
    NONE,
    ALN,
    APPLEMAN,
    CHONGCHONG,
}