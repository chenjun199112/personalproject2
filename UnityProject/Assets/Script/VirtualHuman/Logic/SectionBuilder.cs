﻿#region
using System.Collections.Generic;
using UnityEngine;
#endregion

public class SectionBuilder : DisposableScriptWithUpdate
{
    public static string IDLE_FACE = "sc_em_idle";
    public static readonly string[] TEXT_GESTURE_LIST = {
                                                                "ir_ge_huiys",
                                                                "ir_ge_taiys",
                                                                "ir_ge_sschaoshang"
                                                        };
    public static readonly string[] TEXT_GESTURE_IDLE_LIST = {
                                                                     "ir_ge_idle"
                                                             };
    public static readonly string[] TEXT_HEAD_SECTION = {
                                                                "sc_eb_zuokan",
                                                                "sc_eb_youkan",
                                                                "sc_eb_zuoyoukan",
                                                                "sc_hb_diantou",
                                                                "sc_em_shangtai"
                                                        };

    public static readonly string[] TEXT_HEAD_YAOTOU_SECTION = {
                                                                       "sc_hb_yaotou"
                                                               };

    public static readonly string[] TEXT_HEAD_ZHAYAN_SECTION = {
                                                                       "sc_em_zhayan"
                                                               };

    public static readonly int[][] TEXT_HEAD_SECTION_MASK = {
                                                                    new[] {
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask(),
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadEyeMask(),
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask() |
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadEyeMask()
                                                                          },
                                                                    new[] {
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask(),
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask() |
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadEyeMask()
                                                                          },
                                                                    new[] {
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask(),
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadEyeMask(),
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask() |
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadEyeMask()
                                                                          },
                                                                    new[] {
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadBoneMask()
                                                                          },
                                                                    new[] {
                                                                                  AnimSectionMaskEnumOperation
                                                                                      .GetHeadEyebrowMask()
                                                                          }
                                                            };

    public static readonly string[] TEXT_IDLE_COMBO = {
                                                              "cb_jimeinongyan",
                                                              "cb_minzuiweixiao",
                                                              "cb_zkanykan",
                                                              "cb_xiaorong",
                                                              "cb_tiaomei",
                                                              "cb_luchixiaodt"
                                                      };
    public static readonly string[] TEXT_LISTEN_COMBO = {
                                                                "cb_ceerting"
                                                        };

    private static readonly List<string> _allNameList = new List<string>();

    private readonly List<int> _headUsedIDList = new List<int>();
    private readonly List<int> _idleComboUsedIDList = new List<int>();

    private bool _shouldRandomIdleCombo;

    private bool ShouldUseFirstListen { get; set; }

    public static IList<string> GetAllDataName()
    {
        _allNameList.Clear();
        for (int i = 0; i < TEXT_GESTURE_LIST.Length; i++)
        {
            _allNameList.Add(TEXT_GESTURE_LIST[i]);
        }
        for (int i = 0; i < TEXT_GESTURE_IDLE_LIST.Length; i++)
        {
            _allNameList.Add(TEXT_GESTURE_IDLE_LIST[i]);
        }
        for (int i = 0; i < TEXT_HEAD_SECTION.Length; i++)
        {
            _allNameList.Add(TEXT_HEAD_SECTION[i]);
        }
        for (int i = 0; i < TEXT_IDLE_COMBO.Length; i++)
        {
            _allNameList.Add(TEXT_IDLE_COMBO[i]);
        }
        for (int i = 0; i < TEXT_LISTEN_COMBO.Length; i++)
        {
            _allNameList.Add(TEXT_LISTEN_COMBO[i]);
        }
        for (int i = 0; i < TEXT_HEAD_YAOTOU_SECTION.Length; i++)
        {
            _allNameList.Add(TEXT_HEAD_YAOTOU_SECTION[i]);
        }
        for (int i = 0; i < TEXT_HEAD_ZHAYAN_SECTION.Length; i++)
        {
            _allNameList.Add(TEXT_HEAD_ZHAYAN_SECTION[i]);
        }
        _allNameList.Add(IDLE_FACE);
        return _allNameList;
    }

    private AnimSectionTask DumpIdle()
    {
        AnimSectionTask sectionTask = ClassInstance<AnimSectionTask>.Cache.Alloc();
        _shouldRandomIdleCombo = !_shouldRandomIdleCombo;
        if (_shouldRandomIdleCombo)
        {
            float startTime = 0;
            float idleLength = sectionTask.AddSection(startTime,
                                                      TEXT_GESTURE_IDLE_LIST[0],
                                                      AnimSectionMaskEnumOperation.GetBodyAllMask(),
                                                      AnimSectionMaskEnumOperation.GetBodyAllMask(),
                                                      true,
                                                      ID);
            for (float i = 0; i < 1.2; i = startTime)
            {
                _idTemptList.Clear();
                if (_headUsedIDList.Count == TEXT_HEAD_SECTION.Length)
                {
                    _headUsedIDList.Clear();
                    _idTemptList.Clear();
                }
                for (int item = 0; item < TEXT_HEAD_SECTION.Length; item++)
                {
                    if (!_headUsedIDList.Contains(item))
                    {
                        _idTemptList.Add(item);
                    }
                }
                int range = Random.Range(0,
                                         _idTemptList.Count);
                int maskIndex = Random.Range(0,
                                             TEXT_HEAD_SECTION_MASK[_idTemptList[range]].Length);
                float length = sectionTask.AddSection(i,
                                                      TEXT_HEAD_SECTION[_idTemptList[range]],
                                                      TEXT_HEAD_SECTION_MASK[_idTemptList[range]][maskIndex],
                                                      TEXT_HEAD_SECTION_MASK[_idTemptList[range]][maskIndex],
                                                      false,
                                                      ID);
                _headUsedIDList.Add(_idTemptList[range]);
                startTime += length;
            }
            startTime = 0;
            for (float i = startTime; i < idleLength; i = startTime)
            {
                DumpZhaYanTask(sectionTask,
                               startTime);
                startTime += Random.Range(2,
                                          3);
            }
        }
        else
        {
            _idTemptList.Clear();
            if (_idleComboUsedIDList.Count == TEXT_IDLE_COMBO.Length)
            {
                _idleComboUsedIDList.Clear();
                _idTemptList.Clear();
            }
            for (int item = 0; item < TEXT_IDLE_COMBO.Length; item++)
            {
                if (!_idleComboUsedIDList.Contains(item))
                {
                    _idTemptList.Add(item);
                }
            }
            int range = Random.Range(0,
                                     _idTemptList.Count);
            sectionTask.AddSection(0,
                                   TEXT_IDLE_COMBO[_idTemptList[range]],
                                   AnimSectionMaskEnumOperation.GetAllMask(),
                                   AnimSectionMaskEnumOperation.GetAllMask(),
                                   false,
                                   ID);
            _idleComboUsedIDList.Add(_idTemptList[range]);
        }
        return sectionTask;
    }

    private void DumpZhaYanTask(AnimSectionTask task,
                                float startTime)
    {
        int index = Random.Range(0,
                                 TEXT_HEAD_ZHAYAN_SECTION.Length);
        task.AddSection(startTime,
                        TEXT_HEAD_ZHAYAN_SECTION[index],
                        AnimSectionMaskEnumOperation.GetHeadEyebrowMask(),
                        AnimSectionMaskEnumOperation.GetHeadEyebrowMask(),
                        false,
                        ID);
    }

#region
    public string ID { get; private set; }
    public SectionBuilder(string cid) { ID = cid; }
    public void ReleaseSectionTask(AnimSectionTask task) { ClassInstance<AnimSectionTask>.Cache.Free(task); }

    public AnimSectionTask DumpIdleTask()
    {
        ShouldUseFirstListen = true;
        return DumpIdle();
    }

    public AnimSectionTask DumpListenTask()
    {
        if (ShouldUseFirstListen)
        {
            ShouldUseFirstListen = false;
            AnimSectionTask sectionTask = ClassInstance<AnimSectionTask>.Cache.Alloc();
            float startTime = 0;
            int index = Random.Range(0,
                                     TEXT_LISTEN_COMBO.Length);
            sectionTask.AddSection(startTime,
                                   TEXT_LISTEN_COMBO[index],
                                   AnimSectionMaskEnumOperation.GetAllMask(),
                                   AnimSectionMaskEnumOperation.GetAllMask(),
                                   false,
                                   ID);
            return sectionTask;
        }
        return DumpIdle();
    }

    private readonly List<int> _idleNodUsedIDList = new List<int>();

    public AnimSectionTask DumpNodTask(IList<float> startTime)
    {
        if (startTime.Count == 0)
        {
            return null;
        }
        ShouldUseFirstListen = true;
        AnimSectionTask sectionTask = ClassInstance<AnimSectionTask>.Cache.Alloc();
        for (int i = 0; i < startTime.Count; i++)
        {
            _idTemptList.Clear();
            if (_idleNodUsedIDList.Count == TEXT_HEAD_SECTION.Length - 1)
            {
                _idleNodUsedIDList.Clear();
                _idTemptList.Clear();
            }
            for (int item = 0; item < TEXT_HEAD_SECTION.Length - 1; item++)
            {
                if (!_idleNodUsedIDList.Contains(item))
                {
                    _idTemptList.Add(item);
                }
            }
            int range = Random.Range(0,
                                     _idTemptList.Count);
            //Debug.LogError(TEXT_HEAD_SECTION[_idTemptList[range]]);
            sectionTask.AddSection(startTime[i],
                                   TEXT_HEAD_SECTION[_idTemptList[range]],
                                   TEXT_HEAD_SECTION_MASK[_idTemptList[range]][0],
                                   TEXT_HEAD_SECTION_MASK[_idTemptList[range]][0],
                                   false,
                                   ID);
            _idleNodUsedIDList.Add(_idTemptList[range]);
        }
        return sectionTask;
    }

    private readonly List<int> _gestureUsedIDList = new List<int>();
    private readonly List<int> _idTemptList = new List<int>();

    public AnimSectionTask DumpGestureTask(IList<float> startTime)
    {
        if (startTime.Count == 0)
        {
            return null;
        }
        ShouldUseFirstListen = true;
        AnimSectionTask sectionTask = ClassInstance<AnimSectionTask>.Cache.Alloc();
        for (int i = 0; i < startTime.Count; i++)
        {
            _idTemptList.Clear();
            if (_gestureUsedIDList.Count == TEXT_GESTURE_LIST.Length)
            {
                _gestureUsedIDList.Clear();
                _idTemptList.Clear();
            }
            for (int item = 0; item < TEXT_GESTURE_LIST.Length; item++)
            {
                if (!_gestureUsedIDList.Contains(item))
                {
                    _idTemptList.Add(item);
                }
            }
            int range = Random.Range(0,
                                     _idTemptList.Count);
            sectionTask.AddSection(startTime[i],
                                   TEXT_GESTURE_LIST[_idTemptList[range]],
                                   AnimSectionMaskEnumOperation.GetBodyAllMask(),
                                   AnimSectionMaskEnumOperation.GetBodyAllMask(),
                                   false,
                                   ID);
            _gestureUsedIDList.Add(_idTemptList[range]);
        }
        return sectionTask;
    }

    public AnimSectionTask DumpYaoTouTask(float startTime)
    {
        AnimSectionTask sectionTask = ClassInstance<AnimSectionTask>.Cache.Alloc();
        int index = Random.Range(0,
                                 TEXT_HEAD_YAOTOU_SECTION.Length);
        Debug.LogError(TEXT_HEAD_YAOTOU_SECTION[index]);
        sectionTask.AddSection(startTime,
                               TEXT_HEAD_YAOTOU_SECTION[index],
                               AnimSectionMaskEnumOperation.GetHeadBoneMask(),
                               AnimSectionMaskEnumOperation.GetHeadBoneMask(),
                               false,
                               ID);
        return sectionTask;
    }

    public AnimSectionTask DumpZhaYanTask(float startTime)
    {
        AnimSectionTask sectionTask = ClassInstance<AnimSectionTask>.Cache.Alloc();
        DumpZhaYanTask(sectionTask,
                       startTime);
        return sectionTask;
    }
#endregion
}