﻿// // ************************************************// 
// // File Name:  PingyinSplit.cs
// // Created By Weiya@20181217
// // ************************************************// 

namespace PingyinSplit
{
    public static partial class PingyinSplit
    {
        public static void _split_initial_final(string pinyin, ref string shengmu, ref string yunmu)
        {
            //单纯从拼音层次分离声母和韵母, 如 you => y ou
            //        :params pinyin_without_tone: 无声调的拼音
            //        : returns: 声母，或者 WITHOUT_TONE
            if (pinyin.Length < 2)
            {
                shengmu = WITHOUT_INITIAL;
                yunmu = pinyin;
                return;
            }
            string double_c_initial = pinyin.Substring(0, 2);
            foreach (string t in DOUBLE_C_INITIALS)
            {
                if (double_c_initial == t)
                {
                    shengmu = double_c_initial;
                    yunmu = pinyin.Substring(2, pinyin.Length - 2);
                    return;
                }
            }
            string single_c_initial = pinyin.Substring(0, 1);
            foreach (string t in SINGLE_C_INITIALS)
            {
                if (single_c_initial == t)
                {
                    shengmu = single_c_initial;
                    yunmu = pinyin.Substring(1, pinyin.Length - 1);
                    return;
                }
            }
            foreach (string t in ZERO_RAW_INI_TO_ZERO_INI_KEY)
            {
                if (single_c_initial == t)
                {
                    shengmu = single_c_initial;
                    yunmu = pinyin.Substring(1, pinyin.Length - 1);
                    return;
                }
            }
            shengmu = WITHOUT_INITIAL;
            yunmu = pinyin;
        }

        public static void convert_list(string pinyin, ref string shengmu_phoneme, ref string yunmu_phoneme)
        {
            //单个拼音转化成音素列表

            //# 提前替换拼音, 如 n => en
            for (int i = 0; i < PINYIN_REPLACE_KEY.Length; i++)
            {
                if (PINYIN_REPLACE_KEY[i] == pinyin)
                {
                    pinyin = PINYIN_REPLACE_VALUE[i];
                    break;
                }
            }

            //# 初步分割常规意义上的声韵母，如：'you' => ['y', 'ou'] , 'a' => ['', 'a']
            _split_initial_final(pinyin, ref shengmu_phoneme, ref yunmu_phoneme);
            string initial = shengmu_phoneme;
            string final = yunmu_phoneme;

            //# 声母规范化
            if (initial == WITHOUT_INITIAL)
            {
                //# 零声母情况
                for (int i = 0; i < FIN_TO_ZERO_INI.Length / 2; i++)
                {
                    if (FIN_TO_ZERO_INI[i, 0] == final)
                    {
                        initial = FIN_TO_ZERO_INI[i, 1];
                        break;
                    }
                }
            }
            else
            {
                int index = -1;
                for (int i = 0; i < ZERO_RAW_INI_TO_ZERO_INI_KEY.Length; i++)
                {
                    if (ZERO_RAW_INI_TO_ZERO_INI_KEY[i] == initial)
                    {
                        index = i;
                        break;
                    }
                }
                if (index >= 0)
                {
                    //# 需要转换零声母的情况
                    //# 执行 ZERO_RAW_INI_TO_ZERO_INI 的规则
                    string default_ini = ZERO_RAW_INI_TO_ZERO_INI_VALUE0[index];
                    string[] spec_fin = ZERO_RAW_INI_TO_ZERO_INI_VALUE1[index];
                    string spec_ini = ZERO_RAW_INI_TO_ZERO_INI_VALUE2[index];
                    bool is_spec_fin = false;
                    if (spec_fin != null)
                    {
                        for (int i = 0; i < spec_fin.Length; i++)
                        {
                            if (spec_fin[i] == final)
                            {
                                is_spec_fin = true;
                                initial = spec_ini;
                                break;
                            }
                        }
                    }
                    if (!is_spec_fin)
                    {
                        initial = default_ini;
                    }
                }
            }

            //# 按清华规则调整韵母
            for (int i = 0; i < FIN_MAP_VIA_INI_KEY.Length; i++)
            {
                if (FIN_MAP_VIA_INI_KEY[i] == initial)
                {
                    string[,] valueMap = null;
                    if (i == 0)
                    {
                        valueMap = FIN_MAP_VIA_INI_VALUE_0;
                    }
                    else if (i == 1)
                    {
                        valueMap = FIN_MAP_VIA_INI_VALUE_1;
                    }
                    else if (i == 2)
                    {
                        valueMap = FIN_MAP_VIA_INI_VALUE_2;
                    }
                    else if (i == 3)
                    {
                        valueMap = FIN_MAP_VIA_INI_VALUE_3;
                    }
                    else if (i == 4)
                    {
                        valueMap = FIN_MAP_VIA_INI_VALUE_4;
                    }
                    else if (i == 5)
                    {
                        valueMap = FIN_MAP_VIA_INI_VALUE_5;
                    }
                    else
                    {
                        break;
                    }
                    for (int j = 0; j < valueMap.Length / 2; j++)
                    {
                        if (valueMap[j, 0] == final)
                        {
                            final = valueMap[j, 1];
                            break;
                        }
                    }
                }
            }

            //# 韵母规范化
            //# 针对韵母 i 的处理， 符合thchs30规范
            //# thchs30 下
            //# [zh|ch|sh]    ix[12345]
            //# [z|c|s]       iy[12345]
            //# [r]           iz[12345]
            //# [^z|c|s|zh|ch|sh|r] i[12345]
            if (final == "i")
            {
                bool find = false;
                for (int i = 0; i < THCHS30_IX.Length; i++)
                {
                    if (THCHS30_IX[i] == initial)
                    {
                        final = CONST_IX;
                        find = true;
                        break;
                    }
                }
                if (!find)
                {
                    for (int i = 0; i < THCHS30_IY.Length; i++)
                    {
                        if (THCHS30_IY[i] == initial)
                        {
                            final = CONST_IY;
                            find = true;
                            break;
                        }
                    }
                }
                if (!find)
                {
                    for (int i = 0; i < THCHS30_IZ.Length; i++)
                    {
                        if (THCHS30_IZ[i] == initial)
                        {
                            final = CONST_IZ;
                            find = true;
                            break;
                        }
                    }
                }
            }
            shengmu_phoneme = initial;
            yunmu_phoneme = final;
        }
    }
}