﻿// // ************************************************// 
// // File Name:  PingyinSplitPart.cs
// // Created By Weiya@20181204
// // ************************************************// 

namespace PingyinSplit
{
    public static partial class PingyinSplit
    {
        private static readonly string WITHOUT_INITIAL = "";
        private static readonly string[] PINYIN_REPLACE_KEY = {"n"};
        private static readonly string[] PINYIN_REPLACE_VALUE = {"en"};


        private static readonly string[] SINGLE_C_INITIALS =
                {"b", "p", "m", "f", "d", "t", "n", "l", "g", "k", "h", "j", "q", "x", "r", "z", "c", "s"};
        private static readonly string[] DOUBLE_C_INITIALS = {"zh", "ch", "sh"};

#region
        private static readonly string[,] FIN_TO_ZERO_INI = {
                                                                    {"a", "aa"}, {"ai", "aa"}, {"ao", "aa"},
                                                                    {"an", "aa"}, {"ang", "aa"}, {"e", "ee"},
                                                                    {"er", "ee"}, {"en", "ee"}, {"ei", "ee"}, {"eng", "ee"}, {"o", "oo"},
                                                                    {"ou", "oo"}
                                                            };
#endregion
#region
        private static readonly string[] ZERO_RAW_INI_TO_ZERO_INI_KEY = {"y", "w"};
        private static readonly string[] ZERO_RAW_INI_TO_ZERO_INI_VALUE0 = {"ii", "uu"};
        private static readonly string[][] ZERO_RAW_INI_TO_ZERO_INI_VALUE1 = {
                                                                                     new[] {"u", "ue", "un", "uan"},
                                                                                     null
                                                                             };
        private static readonly string[] ZERO_RAW_INI_TO_ZERO_INI_VALUE2 = {"vv", ""};
#endregion

#region
        private static readonly string[] FIN_MAP_VIA_INI_KEY = {
                                                                       "ii", "uu", "vv", "x", "q", "j"
                                                               };

        private static readonly string[,] FIN_MAP_VIA_INI_VALUE_0 = {
                                                                            {"a", "ia"}, {"an", "ian"}, {"ang", "iang"},
                                                                            {"ao", "iao"}, {"e", "ie"}, {"ou", "iu"},
                                                                            {"ong", "iong"}, {"o", "io"}
                                                                    };

        private static readonly string[,] FIN_MAP_VIA_INI_VALUE_1 = {
                                                                            {"a", "ua"}, {"ai", "uai"}, {"an", "uan"},
                                                                            {"ang", "uang"}, {"ei", "ui"}, {"en", "un"},
                                                                            {"eng", "ueng"}, {"o", "uo"}
                                                                    };

        private static readonly string[,] FIN_MAP_VIA_INI_VALUE_2 = {
                                                                            {"u", "v"}, {"uan", "van"}, {"ue", "ve"},
                                                                            {"un", "vn"}
                                                                    };

        private static readonly string[,] FIN_MAP_VIA_INI_VALUE_3 = {
                                                                            {"u", "v"}, {"uan", "van"}, {"ue", "ve"},
                                                                            {"un", "vn"}
                                                                    };
        private static readonly string[,] FIN_MAP_VIA_INI_VALUE_4 = {
                                                                            {"u", "v"}, {"uan", "van"}, {"ue", "ve"},
                                                                            {"un", "vn"}
                                                                    };

        private static readonly string[,] FIN_MAP_VIA_INI_VALUE_5 = {
                                                                            {"u", "v"}, {"uan", "van"}, {"ue", "ve"},
                                                                            {"un", "vn"}
                                                                    };
#endregion

#region
        private static readonly string CONST_IX = "ix";
        private static readonly string CONST_IY = "iy";
        private static readonly string CONST_IZ = "iz";
        private static readonly string[] THCHS30_IX = {"zh", "ch", "sh"};
        private static readonly string[] THCHS30_IY = {"z", "c", "s"};
        private static readonly string[] THCHS30_IZ = {"r"};
#endregion
    }
}