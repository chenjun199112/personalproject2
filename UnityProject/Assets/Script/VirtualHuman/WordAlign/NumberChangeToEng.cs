﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public static class NumberChangeToEng 
{
    public static string ProcessString(string str)
    {
        //Debug.LogError(str);
        string result = "";
        string temp = "";
        bool isinmeiyuan = false;
        string numeng;
        for (int i = 0; i < str.Length; i++)
        {
            char eng = str[i];
          
            if (Char.IsNumber(eng))
            {
                temp = temp + eng.ToString();
                
                if (i!=str.Length-1 &&Char.IsNumber(str[i + 1]) !=true && str[i+1].ToString() != ".")
                {
                    
                    if (isinmeiyuan)
                    {
                        numeng = numbertoenglish(temp) + " dollars";
                        result = result + " " + numeng + " ";
                        temp = "";
                    }
                    else
                    {
                        if (temp.Length < 7)
                        {
                            numeng = numbertoenglish(temp);

                            temp = "";
                            result = result + " " + numeng + " ";
                        }
                        else
                        {
                            for(int o = 0; o < temp.Length; o++)
                            {
                                numeng = numbertoenglish(temp[o].ToString());
                                result = result + " " + numeng + " ";
                            }
                            temp = "";
                        }
              
                    }
                }
                else if (i == str.Length - 1)
                {
                    
                    if (isinmeiyuan)
                    {
                        numeng = numbertoenglish(temp) + " dollars";
                        result = result + " " + numeng ;
                        temp = "";
                    }
                    else
                    {
                        if (temp.Length < 7)
                        {
                            numeng = numbertoenglish(temp);

                            temp = "";
                            result = result + " " + numeng ;
                        }
                        else
                        {
                            for (int o = 0; o < temp.Length; o++)
                            {
                                numeng = numbertoenglish(temp[o].ToString());
                                result = result + " " + numeng ;
                            }
                            temp = "";
                        }
                    }
                }
            }
            else if(eng.ToString()==" " && i != str.Length - 1 && Char.IsNumber(str[i + 1]))
            {
                temp = temp+"";
            }
            else if (eng.ToString() == "." && i != str.Length - 1 && Char.IsNumber(str[i + 1]) && Char.IsNumber(str[i - 1]))
            {
                temp = temp + eng.ToString();
            }
            else if (eng.ToString() == "$" && i != str.Length - 1 && Char.IsNumber(str[i + 1]))
            {
                isinmeiyuan = true;
            }
            else
            {
                result = result + eng.ToString();
            }           
        }
       // Debug.LogError(result);
        return result;
    }

    public static string numbertoenglish(string number_2)
    {
        string output = "";
        int number;
        String[] valSplit = number_2.Split('.');
        number = Convert.ToInt32(valSplit[0]);

        string zhengshu = NumberToEnglishString(number);
        output = output + zhengshu;
        string xiaoshu = "";
        if (valSplit.Length > 1)
        {

            xiaoshu = numbertoenglishxiaoshu(valSplit[1]);
            output = output + " " + "point" + " ";
        }
        output = output + xiaoshu;
        return output;
    }
    public static string numbertoenglishxiaoshu(string number_1)
    {
        string xiaoshu = "";

        for (int i = 0; i < number_1.Length; i++)
        {

            string A = number_1[i].ToString();
            int engxiaoshu = Convert.ToInt32(A);
            string engxiaoshustring = NumberToEnglishString(engxiaoshu);

            if (i == number_1.Length - 1)
            {
                xiaoshu = xiaoshu + engxiaoshustring;
            }
            else
            {
                xiaoshu = xiaoshu + engxiaoshustring + " ";
            }
        }
        return xiaoshu;
    }

    public static List<string> ReadFile(string path)
    {
        List<string> list = new List<string>();
        FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
        StreamReader sr = new StreamReader(fs, System.Text.Encoding.GetEncoding("gb2312"));
        string s = sr.ReadLine();
        while (s != null)
        {
            if (s.Length > 0)
            {
                list.Add(s);
            }
            s = sr.ReadLine();
        }
        sr.Close();
        fs.Close();
        return list;
    }
    /// <summary>
    /// 数字转化为英文字符串 递归算法即可
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static string NumberToEnglishString(int number)
    {

        if (number < 0) //暂不考虑负数
        {
            return "";
        }
        if (number < 20) //0到19
        {
            switch (number)
            {
                case 0:
                    return "zero";
                case 1:
                    return "one";
                case 2:
                    return "two";
                case 3:
                    return "three";
                case 4:
                    return "four";
                case 5:
                    return "five";
                case 6:
                    return "six";
                case 7:
                    return "seven";
                case 8:
                    return "eight";
                case 9:
                    return "nine";
                case 10:
                    return "ten";
                case 11:
                    return "eleven";
                case 12:
                    return "twelve";
                case 13:
                    return "thirteen";
                case 14:
                    return "fourteen";
                case 15:
                    return "fifteen";
                case 16:
                    return "sixteen";
                case 17:
                    return "seventeen";
                case 18:
                    return "eighteen";
                case 19:
                    return "nineteen";
                default:
                    return "";
            }
        }
        if (number < 100) //20到99
        {
            if (number % 10 == 0) //20,30,40,...90的输出
            {
                switch (number)
                {
                    case 20:
                        return "twenty";
                    case 30:
                        return "thirty";
                    case 40:
                        return "forty";
                    case 50:
                        return "fifty";
                    case 60:
                        return "sixty";
                    case 70:
                        return "seventy";
                    case 80:
                        return "eighty";
                    case 90:
                        return "ninety";
                    default:
                        return "";
                }
            }
            else //21.22,....99 思路：26=20+6
            {
                return string.Format("{0} {1}", NumberToEnglishString(10 * (number / 10)),
                    NumberToEnglishString(number % 10));
            }
        }
        if (number < 1000) //100到999  百级
        {
            if (number % 100 == 0)
            {
                return string.Format("{0} hundred", NumberToEnglishString(number / 100));
            }
            else
            {
                return string.Format("{0} hundred and {1}", NumberToEnglishString(number / 100),
                    NumberToEnglishString(number % 100));
            }
        }
        if (number < 1000000) //1000到999999 千级
        {
            if (number % 1000 == 0)
            {
                return string.Format("{0} thousand", NumberToEnglishString(number / 1000));
            }
            else
            {
                return string.Format("{0} thousand and {1}", NumberToEnglishString(number / 1000),
                    NumberToEnglishString(number % 1000));
            }
        }
        if (number < 1000000000) //1000 000到999 999 999 百万级
        {
            if (number % 1000 == 0)
            {
                return string.Format("{0} million", NumberToEnglishString(number / 1000000));
            }
            else
            {
                return string.Format("{0} million and {1}", NumberToEnglishString(number / 1000000),
                    NumberToEnglishString(number % 1000000));
            }
        }
        if (number <= int.MaxValue) //十亿 级
        {
            if (number % 1000000000 == 0)
            {
                return string.Format("{0} billion", NumberToEnglishString(number / 1000000000));
            }
            else
            {
                return string.Format("{0} billion and {1}", NumberToEnglishString(number / 1000000000),
                    NumberToEnglishString(number % 1000000000));
            }
        }
        return "";
    }
}
