﻿using System;
using System.Diagnostics;
using System.Text;

public static class NumberChangeToChinese
{
    public static string ProcessString(string str)
    {
        StringBuilder resultSb = new StringBuilder();
        StringBuilder integerSb = new StringBuilder();
        long integer = 0; //整数
        long maxValue = 10000000000;
        StringBuilder decimalSb = new StringBuilder(); //小数
        bool bHasInteger = false;
        bool bHasDecimal = false;
        
        for (int i = 0; i < str.Length; i++)
        {
            char ch = str[i];
            bool bEndNum = false;
            if (Char.IsNumber(ch))
            {
                int temp = int.Parse(ch.ToString());
                bHasInteger = true;
                if (!bHasDecimal)
                {
                    if (integer < maxValue)
                        integer = integer * 10 + temp;
                    integerSb.Append(Tool.chnNumChar[temp]);
                }
                else
                {
                    decimalSb.Append(Tool.chnNumChar[temp]);
                }
            }
            else if (ch == '.' && bHasInteger && !bHasDecimal)
                bHasDecimal = true;
            else
                bEndNum = true;

            if (bEndNum || i == str.Length - 1)
            {
                if (bHasInteger)
                {
                    if (ch != '年' && integer < maxValue)
                        resultSb.Append(NumberToChinese(integer));
                    else
                        resultSb.Append(integerSb);
                    
                    integer = 0;
                    integerSb.Remove(0, integerSb.Length);
                    if (bHasDecimal)
                    {
                        if (decimalSb.Length > 0)
                        {
                            resultSb.Append(Tool.chnDotChar);
                            resultSb.Append(decimalSb);
                            decimalSb.Remove(0, decimalSb.Length);
                        }
                        else
                        {
                            resultSb.Append(".");
                        }
                    }
                }

                bHasInteger = false;
                bHasDecimal = false;

                if (bEndNum)
                   resultSb.Append(ch);
            }
        }

        return resultSb.ToString();
    }

    public static string NumberToChinese(long num)
    {
        //转化一个阿拉伯数字为中文字符串
        if (num == 0)
        {
            return "零";
        }

        int unitPos = 0;//节权位标识
        string all = "";
        string chineseNum = "";//中文数字字符串
        bool needZero = false;//下一小结是否需要补零
        
        while (num > 0)
        {
            string strIns;
            long section = num % 10000;//取最后面的那一个小节
            bool firstSection = ((num / 10000) == 0);
            if (needZero)
            {
                //判断上一小节千位是否为零，为零就要加上零
                all = Tool.chnNumChar[0] + all;
            }
            chineseNum = SectionToChinese(section, chineseNum, firstSection);//处理当前小节的数字,然后用chineseNum记录当前小节数字
            if (section != 0)
            {
                //此处用if else 选择语句来执行加节权位
                strIns = Tool.chnUnitSection[unitPos];//当小节不为0，就加上节权位
                chineseNum = chineseNum + strIns;
            }
            else
            {
                strIns = Tool.chnUnitSection[0];//否则不用加
                chineseNum = strIns + chineseNum;
            }
            all = chineseNum + all;
            chineseNum = "";
            needZero = (section < 1000) && (section > 0);
            num = num / 10000;
            unitPos++;
        }
        return all;
    }
    public static string SectionToChinese(long section, string chineseNum, bool firstSection)
    {
        string setionChinese;//小节部分用独立函数操作
        int unitPos = 0;//小节内部的权值计数器
        bool zero = true;//小节内部的制零判断，每个小节内只能出现一个零
        while (section > 0)
        {  
            long v = section % 10;//取当前最末位的值
            if (v == 0)
            {
                if (!zero)
                {
                    zero = true;//需要补零的操作，确保对连续多个零只是输出一个
                    chineseNum = Tool.chnNumChar[0] + chineseNum;
                }
            }
            else
            {
                zero = false;//有非零的数字，就把制零开关打开
                if (firstSection && unitPos == 1 && section / 10 == 0 && section % 10 == 1)
                    setionChinese = string.Empty;//首部分的无更高伟的十位前为1，不显示
                else
                    setionChinese = Tool.chnNumChar[v];//对应中文数字位
                setionChinese = setionChinese + Tool.chnUnitChar[unitPos];//对应中文权位
                chineseNum = setionChinese + chineseNum;
            }
            unitPos++;
            section = section / 10;
        }
        return chineseNum;
    }
}

public static class Tool
{
    //节权位
    public static string[] chnUnitSection = { "", "万", "亿", "万亿" };
    //权位
    public static string[] chnUnitChar = { "", "十", "百", "千" };
    //数字位
    public static string[] chnNumChar = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
    //小数点
    public static string chnDotChar = "点";
}
