﻿using System;
using UnityEngine;


public static class WordAlign
{
    private static bool InitFlag { get; set; }

    public static bool Init(int audiodataSampleRate)
    {
        if (InitFlag)
        {
            return true;
        }
        string logPath = Application.persistentDataPath + "/" + DateTime.Now.ToFileTimeUtc() + "_low.log";
        //Debug.LogError(logPath);
        InitFlag = WordAlignBridge.Init(logPath,
                                        audiodataSampleRate);
        return InitFlag;
    }

    public static void Uninit()
    {
        if (!InitFlag)
        {
            return;
        }
        InitFlag = false;
        WordAlignBridge.Uninit();
    }
    //中文对齐
    public static bool AlignPCMF32N(string zhSentence,
                                    float[] sampleFloats,
                                    int sampleSize,
                                    ref BetterList<ZHWordData> wordsData,
                                    out BetterList<int> splitIndexList)
    {
        wordsData.Clear();
        splitIndexList = null;
        if (!InitFlag)
        {
            return false;
        }
        if (string.IsNullOrEmpty(zhSentence))
        {
            return false;
        }
        if (sampleSize == 0)
        {
            return false;
        }
        BetterList<int> splitIndex;
        BetterList<string> shengmuList;
        BetterList<string> yummuList;
        BetterList<int> shengDiaoList;
        BetterList<float> startTime;
        BetterList<float> endTime;
        int formanttype = 1;
        WordAlignBridge.SplitZHWord(zhSentence,
                                    out splitIndex,
                                    out shengmuList,
                                    out yummuList,
                                    out shengDiaoList);
        splitIndexList = splitIndex;
        Debug.Log("  >>>align data:" + shengmuList.size + " sd:" + shengDiaoList.size + "  split:" + splitIndex.size);
        bool state = WordAlignBridge.RunWithAudioFloatNBuff(sampleFloats,
                                                            sampleSize,
                                                            shengmuList.size,
                                                            splitIndex,
                                                            yummuList,
                                                            formanttype,
                                                            out startTime,
                                                            out endTime);
        if (state)
        {
            for (int i = 0; i < startTime.size; i++)
            {
                wordsData.Add(new ZHWordData(shengmuList[i],
                                             yummuList[i],
                                             shengDiaoList[i],
                                             startTime[i],
                                             endTime[i]));
            }
        }
        return state;
    }

    public static bool AlignFile(string zhSentence,
                                 string filePath,
                                 PCM_TYPE pcmType,
                                 ref BetterList<ZHWordData> wordsData,
                                 out BetterList<int> splitIndexList)
    {
        wordsData.Clear();
        splitIndexList = null;
        if (!InitFlag)
        {
            return false;
        }
        BetterList<int> splitIndex;
        BetterList<string> shengmuList;
        BetterList<string> yummuList;
        BetterList<int> shengDiaoList;
        BetterList<float> startTime;
        BetterList<float> endTime;
        WordAlignBridge.SplitZHWord(zhSentence,
                                    out splitIndex,
                                    out shengmuList,
                                    out yummuList,
                                    out shengDiaoList);
        splitIndexList = splitIndex;
        Debug.Log("  >>>align file data:" +
                  shengmuList.size +
                  " sd:" +
                  shengDiaoList.size +
                  "  split:" +
                  splitIndex.size);
        bool state = WordAlignBridge.RunWithAudioFile(filePath,
                                                      PCM_TYPE.PCM_S16,
                                                      shengmuList.size,
                                                      splitIndex,
                                                      yummuList,
                                                      out startTime,
                                                      out endTime);
        if (state)
        {
            for (int i = 0; i < startTime.size; i++)
            {
                wordsData.Add(new ZHWordData(shengmuList[i],
                                             yummuList[i],
                                             shengDiaoList[i],
                                             startTime[i],
                                             endTime[i]));
            }
        }
        return state;
    }

    public class ZHWordData
    {
        public float EndTime;
        public int ShengDiao;
        public string ShengMu;
        public float StartTime;
        public string YunMu;

        public ZHWordData(string shengMu,
                          string yunMu,
                          int shengDiao,
                          float startTime,
                          float endTime)
        {
            ShengMu = shengMu;
            YunMu = yunMu;
            ShengDiao = shengDiao;
            StartTime = startTime;
            EndTime = endTime;
        }
    }
    //英文对齐
    public static bool EngALIGN(string zhSentence,
                                float[] sampleFloats,
                                int sampleSize,
                                ref BetterList<ENWordData> wordsData,
                                out BetterList<int> splitIndexList)
    {
        wordsData.Clear();
        splitIndexList = null;
        if (!InitFlag)
        {
            return false;
        }
        if (string.IsNullOrEmpty(zhSentence))
        {
            return false;
        }
        if (sampleSize == 0)
        {
            return false;
        }
        BetterList<int> splitIndex;//分句后每句包含的元音浊辅音个数 [2,5,6]
        BetterList<BetterList<string>> listAll;//每个单词包含的所用音素 二维数组 [['AY1'], ['N', 'EH1', 'V', 'ER0'], ['TH', 'AO1', 'T']]
        BetterList<BetterList<string>> listFinal;//每个单词包含的元音 浊辅音 二维数组 [['AY1'], ['EH1', 'ER0'], ['TH', 'AO1', 'T']]
        BetterList<string> listPhoneFormant;//用于计算共振峰的音素
        BetterList<float> startTime;
        BetterList<float> endTime;
        BetterList<BetterList<float>> PhoneStartTime;
        BetterList<BetterList<float>> PhoneEndTime;
        int formanttype = 0;
        WordAlignBridge.SplitEngPhoneme(zhSentence,
                                        out splitIndex,
                                        out listAll,
                                        out listFinal,
                                        out listPhoneFormant);
        //for (int j = 0; j < listPhoneFormant.size; j++)
        //{
        //    Debug.LogError(listPhoneFormant[j]);
        //}
        splitIndexList = splitIndex;
        int word_size = 0;
        foreach(int i in splitIndex)
        {
             word_size+= i;
        }
        bool state = WordAlignBridge.RunWithAudioFloatNBuff(sampleFloats,
                                                            sampleSize,
                                                            word_size,
                                                            splitIndex,
                                                            listPhoneFormant,
                                                            formanttype,
                                                            out startTime,
                                                            out endTime);

        //for (int j = 0; j < startTime.size; j++)
        //{
        //    Debug.LogError((startTime[j], endTime[j]));
        //}
        splitIndex.Clear();
        word_size = 0;
        WordAlignBridge.PhoneAlign(listAll,
                                    listFinal,
                                    startTime,
                                    endTime,
                                    out PhoneStartTime,
                                    out PhoneEndTime);

        if (state)
        {
            for (int p = 0; p < listAll.size; p++)

            {
             
                for(int o = 0; o < listAll[p].size; o++)
                {
                   //Debug.LogError((listAll[p][o],PhoneStartTime[p][o], PhoneEndTime[p][o]));
                    wordsData.Add(new ENWordData(PhoneStartTime[p][o],
                                                PhoneEndTime[p][o],
                                                listAll[p][o]));
                }
            }
        }
        return state;
    }
    public class ENWordData
    {
        public float StartTime;
        public float EndTime;
        public string Phoneme;

        public ENWordData(float startTime,
            float endTime,
            string phoneme)
        {
            StartTime = startTime;
            EndTime = endTime;
            Phoneme = phoneme;
        }
    }

}

