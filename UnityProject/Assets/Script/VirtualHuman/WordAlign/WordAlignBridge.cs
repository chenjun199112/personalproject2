﻿// // ************************************************// 
#region
using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using System.Collections.Generic;
using Xunfei;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
#endregion


public static class WordAlignBridge
{
#region
#if (UNITY_IOS && !UNITY_EDITOR)
	const string LIBNAME = "__Internal";
#elif ((UNITY_ANDROID || UNITY_STANDALONE_LINUX) && !UNITY_EDITOR)
	const string LIBNAME = "word_align";
#else
    private const string LIBNAME = "libword_align";
#endif
    [DllImport(LIBNAME)]
    public static extern int gg_wrap_init(IntPtr ptrLogPath, int logLevel, int sampleRate, int ttsSpeed);

    [DllImport(LIBNAME)]
    public static extern int noise_suppression(IntPtr buffer, uint sampleRate, int samplesCount, int level);

    [DllImport(LIBNAME)]
    public static extern int gg_wrap_uninit();

    [DllImport(LIBNAME)]
    public static extern int gg_wrap_zhmatch_run_file(IntPtr ptrFilepath, int filedataType, int zhWordLength,
                                                      IntPtr ptrSplitwordFirstindex, int splitSize, IntPtr formantIndex, int formantSplitSize,int formanttype);

    [DllImport(LIBNAME)]
    public static extern int gg_wrap_zhmatch_run(int zhWordLength, IntPtr ptrSplitwordFirstindex, int splitSize,
                                                 IntPtr ptrZhAudiodata, int audiodataSampleSize, int audiodataType, IntPtr formantIndex, int formantSplitSize,int formanttype);

    [DllImport(LIBNAME)]
    public static extern int gg_wrap_zhmatch_get_marker(out int returnListSize, out IntPtr returnStartList,
                                                        out IntPtr returnEndList);

    [DllImport(LIBNAME)]
    public static extern int gg_wrap_zhmatch_get_marker_timestamp(out int returnListSize, out IntPtr returnStartList,
                                                                  out IntPtr returnEndList);
#endregion

    public static int c_sharp_noise_suppression(short[] buffer, uint sampleRate, int samplesCount, int level)
    {
        int returnValue = 0;
        unsafe
        {
            fixed (short* ptrBuffer = buffer)
            {
                returnValue = noise_suppression((IntPtr) ptrBuffer,
                                           sampleRate,
                                           samplesCount,
                                           level);
            }
        }
        
        return returnValue;
    }
    //英语  输出元音列表
    public static void SplitEngPhoneme(string zhsentence,
                                        out BetterList<int> splitindex,
                                        out BetterList<BetterList<string>> listAll,
                                        out BetterList<BetterList<string>> listFinal,
                                         out BetterList<string> listphoneformant)
    {
        

        listAlllist.Clear();
        listFinallist.Clear();
        SplitIndexList.Clear();
        listphoneformantlist.Clear();
        splitindex = SplitIndexList;
        listAll = listAlllist;
        listFinal = listFinallist;
        listphoneformant = listphoneformantlist;

        string realWords = PrepareENGword(zhsentence);
        Spell.EngMakeSpell(realWords,
                          ref listAlllist,
                           ref listFinallist,
                           ref SplitIndexList,
                           ref listphoneformantlist);
    }


    public static void SplitZHWord(string words, out BetterList<int> firstSplitIndex,
                                   out BetterList<string> shengmuList, out BetterList<string> yummuList,
                                   out BetterList<int> shengDiaoList)
    {
        ShengDiaoList.Clear();
        SplitIndexList.Clear();
        ShengmuList.Clear();
        YummuList.Clear();
        string realWords = PrepareZHWord(words);
        //文字转拼音
        Spell.MakeSpell(realWords,
                        Spell.SpellOptions.EnableUnicodeLetter,
                        ref ShengmuList,
                        ref SplitIndexList);
        string shengmu = string.Empty;
        string yunmu = string.Empty;
        string shenDiao = string.Empty;
        for (int i = 0; i < ShengmuList.size; i++)
        {
            string pingying = ShengmuList[i].Substring(0,
                                                       ShengmuList[i].Length - 1);
            shenDiao = ShengmuList[i].Substring(ShengmuList[i].Length - 1,
                                                1);
            //拼音分离声母和韵母
            PingyinSplit.PingyinSplit.convert_list(pingying,
                                                           ref shengmu,
                                                           ref yunmu);

            int shenDiaoInt;
            if (int.TryParse(shenDiao,
                             out shenDiaoInt))
            {
//                Debug.Log(ShengmuList[i] + "->" + shengmu + "->" + yunmu + "->" + shenDiaoInt);
                ShengDiaoList.Add(shenDiaoInt);
            }
            else
            {
                Debug.LogError("[ERROR] SplitZHWord:" + ShengmuList[i] + " null sheng diao");
            }
            ShengmuList[i] = shengmu;
            YummuList.Add(yunmu);
        }
        firstSplitIndex = SplitIndexList;
        shengmuList = ShengmuList;
        yummuList = YummuList;
        shengDiaoList = ShengDiaoList;
    }

    //替换字符串
#region
    private static readonly string[,] replaceMap = {
        {"[k3]", ""}, {"[k0]", ""}, {"-", "至"},
        {"℃", "摄氏度"}, {" ", ""}, 
        {"~~", ","}, {"。。。", ","}, {"《", ""}, 
        {"》", ""}, {"【", ""}, {"】", ""},
        {" ", ""}, {"→", ""}, {"”", ""}, 
        {"“", ""}, {"）", ""}, {"（", ""}, 
        {"•", ""}, {"<", ""}, {">", ""}, 
        {"|", ""}, {"~", ""}, {"α", "阿发"} , 
        {"β", "贝塔"}, {"%", "百分之"},
        {"♍", ""},{"♌", ""},{"♉", ""},
        {"♈", ""},{"♊", ""},{"♋", ""},
        {"♎", ""},{"♏", ""},{"♐", ""},
        {"♑", ""},{"♒", ""},{"♓", ""},
        {"①", "一"},{"②", "二"},{"③", "三"},
        {"④", "四"},{"⑤", "五"},{"⑥", "六"},
        {"⑦", "七"},{"⑧", "八"},{"⑨", "九"},
        {"⑩", "十"}
    };
    private static readonly string[,] replaceEngMap = {
        {"[k3]", ""}, {"[k0]", ""}, {"-", " "},
        {"℃", " Celsius "},{"...", " "},
        {"~~", ","}, {"。。。", ","}, {"《", " "},
        {"》", " "}, {"【", " "}, {"】", " "},
         {"→", ""}, {"”", " "},{"’", " "},{"'", " "},
        {"“", " "}, {")", " "}, {"(", " "},{"["," "},{"]"," "},
        {"•", " "}, {"<", " "}, {">", " "},{",", " "},
        {"|", " "}, {"~", " "}, {"α", " Alpha"} ,
        {"β", " beta"}, {"%", " percent"},
        {"♍", ""},{"♌", ""},{"♉", ""},
        {"♈", ""},{"♊", ""},{"♋", ""},
        {"♎", ""},{"♏", ""},{"♐", ""},
        {"♑", ""},{"♒", ""},{"♓", ""},
        {"①", "one"},{"②", "two"},{"③", "three"},
        {"④", "four"},{"⑤", "five"},{"⑥", "six"},
        {"⑦", "seven"},{"⑧", "eigh"},{"⑨", "nine"},
        {"⑩", "ten"}
    };
   
    public static string PrepareZHWord(string strChinese)
    {
        //度量衡 特殊字符->中文

        StringBuilder sb = new StringBuilder(strChinese);
        

        for (int i = 0; i < replaceMap.Length / 2; i++ )
        {
            sb.Replace(replaceMap[i, 0], replaceMap[i, 1]);
        }
       

        //数字->中文
        string result = NumberChangeToChinese.ProcessString(sb.ToString());

        //英文字母->中文
        result = RemoveEnglishWord(result);

        return result;
    }
    public static string PrepareENGword(string strEnglish)
    {
        //特殊符号-》英文
        string strenglish = "";
        for (int j = 0; j < strEnglish.Length; j++)
        {
            if (strEnglish[j].ToString() == "," && j != strEnglish.Length - 1 && Char.IsNumber(strEnglish[j + 1]))
            {
                strenglish = strenglish + "";
            }
            else
            {
                strenglish = strenglish + strEnglish[j].ToString();
            }
        }
        StringBuilder sbeng = new StringBuilder(strEnglish);
        for (int i = 0; i < replaceEngMap.Length / 2; i++)
        {
            sbeng.Replace(replaceEngMap[i, 0], replaceEngMap[i, 1]);
        }
        sbeng.Replace("  ", " ");
        if(sbeng[sbeng.Length-1].ToString()==" ")
        {
            sbeng.Remove(sbeng.Length - 1, 1);
            
        }

        string result = NumberChangeToEng.ProcessString(sbeng.ToString());
      
        return result;
    }
    public static string RemoveEnglishWord(string str)
    {
        StringBuilder resultSb = new StringBuilder();
        StringBuilder wordSb = new StringBuilder();
        bool existLower = false;
        for (int i = 0; i < str.Length; i++)
        {
            char ch = str[i];
            bool bEndWord = false;
            if (ch >= 'a' && ch <= 'z')
            {
                wordSb.Append(ch);
                existLower = true;
            }
            else if (ch >= 'A' && ch <= 'Z')
            {
                wordSb.Append(ch);
            }
            else
            {
                bEndWord = true; 
            }

            if (bEndWord || i == str.Length - 1)
            {
                if (existLower)
                    resultSb.Append(EnglishWordToChinese(wordSb.Length));
                else
                    resultSb.Append(wordSb);

                existLower = false;
                wordSb.Remove(0, wordSb.Length);

                if (bEndWord)
                  resultSb.Append(ch);
            }
        }

        return resultSb.ToString();
    }

    public static string EnglishWordToChinese(int length)
    {
        string result;
        if (length <= 0)
            result = string.Empty;
        else if (length <= 2)
            result = "沃";
        else if (length <= 5)
            result = "都沃";
        else
            result = "都沃看";
        return result;
    }

    #endregion

    public static void Uninit() { gg_wrap_uninit(); }

    public static bool Init(string logPath, int audiodataSampleRate)
    {
        byte[] logPathBytes = Encoding.UTF8.GetBytes(logPath);
        int returnValue = 0;
        unsafe
        {
            fixed (byte* ptrLogPath = logPathBytes)
            {
                returnValue = gg_wrap_init((IntPtr) ptrLogPath,
                                           (int) LOG_LEVEL.DEBUG,
                                           audiodataSampleRate,
                                           XFTTSWebClient.TTS_SPEED);
            }
        }
        logPathBytes = null;
        if (returnValue != 0)
        {
            Debug.LogError("[ERROR]WordAlign Init:" + returnValue);
            return false;
        }
        return true;
    }

    public static bool RunWithAudioFile(string rawFilePath, PCM_TYPE fileDataType, int wordSize,
                                        BetterList<int> splitwordFirstindexList, BetterList<string> yummuList, out BetterList<float> retStartList,
                                        out BetterList<float> retEndList)
    {
        StartTimeList.Clear();
        EndTImeList.Clear();
        retStartList = StartTimeList;
        retEndList = EndTImeList;
        byte[] pathBytes = Encoding.UTF8.GetBytes(rawFilePath);
        int returnValue;
        int[] ints = new int[splitwordFirstindexList.size];
        for (int i = 0; i < splitwordFirstindexList.size; i++)
        {
            if (i == 0)
                ints[i] = splitwordFirstindexList[i] + 1;
            else
            {
                ints[i] = splitwordFirstindexList[i] - splitwordFirstindexList[i - 1];
            }
        }

        int[] yummuIndex = new int[yummuList.size];
        for (int i = 0; i < yummuList.size; i++)
        {
            yummuIndex[i] = GetFormantIndex(yummuList[i]);
        }

        int spliteSize = splitwordFirstindexList.size;
        unsafe
        {
            fixed (byte* ptrLogPath = pathBytes)
            {
                fixed (int* ptrInts = ints)
                {
                    fixed (int* ptrYummuIndex = yummuIndex)
                    { 
                    returnValue = gg_wrap_zhmatch_run_file((IntPtr) ptrLogPath,
                                                           (int) fileDataType,
                                                           wordSize,
                                                           (IntPtr) ptrInts,
                                                           spliteSize,
                                                           (IntPtr)ptrYummuIndex,
                                                           yummuList.size,
                                                           0);
                    }
                }
            }
        }
        if (returnValue != 0)
        {
            Debug.LogError("[ERROR]WordAlign RunWithAudioFile1:" + returnValue);
            return false;
        }
        pathBytes = null;
        bool state = get_timestamp();
        return state;
    }

    private static bool get_timestamp()
    {
        StartTimeList.Clear();
        EndTImeList.Clear();
        IntPtr ptrMarkerStartList;
        IntPtr ptrMarkerEndList;
        int size;
        int returnValue = gg_wrap_zhmatch_get_marker_timestamp(out size,
                                                               out ptrMarkerStartList,
                                                               out ptrMarkerEndList);
        if (returnValue != 0)
        {
            Debug.LogError("[ERROR]WordAlign get_timestamp:" + returnValue);
            return false;
        }
        unsafe
        {
            float* ptrStartMarker = (float*) ptrMarkerStartList;
            float* ptrEndMarker = (float*) ptrMarkerEndList;
            for (int i = 0; i < size; i++)
            {
                StartTimeList.Add(*(ptrStartMarker + i));
                EndTImeList.Add(*(ptrEndMarker + i));
            }
        }
        return true;
    }

    public static bool RunWithAudioByteBuff(byte[] buff, int totalSamplesize, PCM_TYPE fileDataType, int wordSize,
                                            BetterList<int> splitwordFirstindexList, BetterList<string> yummuList, out BetterList<float> retStartList,
                                            out BetterList<float> retEndList)
    {
        StartTimeList.Clear();
        EndTImeList.Clear();
        retStartList = StartTimeList;
        retEndList = EndTImeList;
        int returnValue = 0;
        int[] ints = new int[splitwordFirstindexList.size];
        for (int i = 0; i < splitwordFirstindexList.size; i++)
        {
            if (i == 0)
                ints[i] = splitwordFirstindexList[i] + 1;
            else
            {
                ints[i] = splitwordFirstindexList[i] - splitwordFirstindexList[i - 1];
            }
        }

        int[] yummuIndex = new int[yummuList.size];
        for (int i = 0; i < yummuList.size; i++)
        {
            yummuIndex[i] = GetFormantIndex(yummuList[i]);
        }

        int spliteSize = splitwordFirstindexList.size;
        unsafe
        {
            fixed (byte* ptr = buff)
            {
                fixed (int* ptrInts = ints)
                {
                    fixed (int* ptrYummuIndex = yummuIndex)
                    {
                        returnValue = gg_wrap_zhmatch_run(wordSize,
                            (IntPtr) ptrInts,
                            spliteSize,
                            (IntPtr) ptr,
                            totalSamplesize,
                            (int) fileDataType,
                            (IntPtr)ptrYummuIndex,
                            yummuList.size,
                            0);
                    }
                }
            }
        }
        if (returnValue != 0)
        {
            Debug.LogError("[ERROR]WordAlign RunWithAudioFile Byte:" + returnValue);
            return false;
        }
        bool state = get_timestamp();
        return state;
    }

    public static bool RunWithAudioShort16Buff(short[] buff, int totalSamplesize, int wordSize,
                                               BetterList<int> splitwordFirstindexList, BetterList<string> yummuList,
                                               out BetterList<float> retStartList, out BetterList<float> retEndList)
    {
        StartTimeList.Clear();
        EndTImeList.Clear();
        retStartList = StartTimeList;
        retEndList = EndTImeList;
        int returnValue = 0;
        int[] ints = new int[splitwordFirstindexList.size];
        for (int i = 0; i < splitwordFirstindexList.size; i++)
        {
            if (i == 0)
                ints[i] = splitwordFirstindexList[i] + 1;
            else
            {
                ints[i] = splitwordFirstindexList[i] - splitwordFirstindexList[i - 1];
            }
        }

        int[] yummuIndex = new int[yummuList.size];
        for (int i = 0; i < yummuList.size; i++)
        {
            yummuIndex[i] = GetFormantIndex(yummuList[i]);
        }

        int spliteSize = splitwordFirstindexList.size;
        unsafe
        {
            fixed (short* ptr = buff)
            {
                fixed (int* ptrInts = ints)
                {
                    fixed (int* ptrYummuIndex = yummuIndex)
                    {
                        returnValue = gg_wrap_zhmatch_run(wordSize,
                            (IntPtr) ptrInts,
                            spliteSize,
                            (IntPtr) ptr,
                            totalSamplesize,
                            (int) PCM_TYPE.PCM_S16,
                            (IntPtr)ptrYummuIndex,
                            yummuList.size,
                            0);
                    }
                }
            }
        }
        if (returnValue != 0)
        {
            Debug.LogError("[ERROR]WordAlign RunWithAudioFile Short16:" + returnValue);
            return false;
        }
        bool state = get_timestamp();
        return state;
    }

    public static bool RunWithAudioFloatNBuff(float[] buff, int totalSamplesize, int wordSize,
                                              BetterList<int> splitwordFirstindexList, BetterList<string> yummuList,int formanttype,
                                              out BetterList<float> retStartList, out BetterList<float> retEndList)
    {
        StartTimeList.Clear();
        EndTImeList.Clear();
        retStartList = StartTimeList;
        retEndList = EndTImeList;
        int returnValue = 0;
        int[] ints = new int[splitwordFirstindexList.size];
        for (int i = 0; i < splitwordFirstindexList.size; i++)
        {
            if (i == 0)
                ints[i] = splitwordFirstindexList[i] + 1;
            else
            {
                ints[i] = splitwordFirstindexList[i] - splitwordFirstindexList[i - 1];
            }
        }

        int[] yummuIndex = new int[yummuList.size];
        for (int i = 0; i < yummuList.size; i++)
        {
            yummuIndex[i] = GetFormantIndex(yummuList[i]);
           // Debug.LogError(yummuIndex[i]);
        }


        int spliteSize = splitwordFirstindexList.size;
        unsafe
        {
            fixed (float* ptr = buff)
            {
                fixed (int* ptrInts = ints)
                {
                    fixed (int* ptrYummuIndex = yummuIndex)
                    {
                        returnValue = gg_wrap_zhmatch_run(wordSize,
                            (IntPtr) ptrInts,
                            spliteSize,
                            (IntPtr) ptr,
                            totalSamplesize,
                            (int) PCM_TYPE.PCM_FLOAT32_NORMAL,
                            (IntPtr)ptrYummuIndex,
                            yummuList.size,
                            formanttype);
                    }
                }
            }
        }
        if (returnValue != 0)
        {
            Debug.LogError("[ERROR]WordAlign RunWithAudioFile FloatN:" + returnValue);
            return false;
        }
        bool state = get_timestamp();
        return state;
    }

    private static int GetFormantIndex(string str)
    {
        if (!_bSetFormantList)
        {
            for (int i = 0; i < FORMANTENG.Length; i++)
            {
                _formantList.Add(FORMANTENG[i]);
            }
        }

        return _formantList.IndexOf(str);
    }
    //二次对齐
    public static void PhoneAlign(BetterList<BetterList<string>> listAll,
                                  BetterList<BetterList<string>> listFinal,
                                  BetterList<float> startTime,
                                  BetterList<float> endTime,
                                  out BetterList<BetterList<float>> PhoneStartTime,
                                  out BetterList<BetterList<float>> PhoneEndTime)
    {
        PhoneStartTimeList.Clear();
        PhoneEndTimeList.Clear();
        PhoneStartTime = PhoneStartTimeList;
        PhoneEndTime = PhoneEndTimeList;
        int count = 0;
        int vowel_geshu = 0;
        //单词对齐
        foreach (var vowel in listFinal)
        {
            count += 1;
            vowel_geshu = vowel.size;
            BetterList<float> singleStartTimeList = new BetterList<float>();
            BetterList<float> singleEndTimeList = new BetterList<float>();
            if (vowel_geshu == 1)
            {
                //一个列表只包含一个单词时间，单词时间列表再组成一个新的列表
                singleStartTimeList.Clear();
                singleEndTimeList.Clear();
                singleStartTimeList.Add(startTime[0]);
                singleEndTimeList.Add(endTime[0]);
                PhoneStartTimeList.Add(singleStartTimeList);
                PhoneEndTimeList.Add(singleEndTimeList);
                startTime.RemoveAt(0);
                endTime.RemoveAt(0);
            }
            else
            {
                singleStartTimeList.Clear();
                singleEndTimeList.Clear();
                for (int i = 0; i < vowel_geshu; i++)
                {
                    singleStartTimeList.Add(startTime[i]);
                    singleEndTimeList.Add(endTime[i]);
                }
                PhoneStartTimeList.Add(singleStartTimeList);
                PhoneEndTimeList.Add(singleEndTimeList);
                for(int j = 0; j < vowel_geshu; j++)
                {
                    startTime.RemoveAt(0);
                    endTime.RemoveAt(0);
                }
                
            }
        }
        //音素对齐  
        for(int w = 0; w < PhoneStartTime.size; w++)
        {
            int chazhi = listAll[w].size - listFinal[w].size;//单词包含多少清音
            if(chazhi != 0)
            {
                float qing = 0.03f;
                for(int b=0;b < chazhi; b++)
                {
                    listFinal[w].Add("0");
                }
                for(int i = 0; i < listAll[w].size; i++)
                {
                    if (listAll[w][i] !=listFinal[w][i] &&listFinal[w][i]!="0")
                    {
                        float phoneme = PhoneStartTimeList[w][i]+qing;
                        listFinal[w].Insert(i, listAll[w][i]);
                        PhoneStartTimeList[w].Insert(i + 1, phoneme);
                        PhoneEndTimeList[w].Insert(i, phoneme);
                        listFinal[w].RemoveAt(listFinal[w].size-1);
                        chazhi = chazhi - 1;
                    }
                    else if(listAll[w][i] != listFinal[w][i] && listFinal[w][i] == "0")
                    {
                        listFinal[w].Insert(i, listAll[w][i]);
                        PhoneStartTimeList[w].Insert(i, PhoneEndTimeList[w][i - 1] -qing*chazhi);
                        PhoneEndTimeList[w].Insert(i - 1, PhoneEndTimeList[w][i - 1] - qing*chazhi);
                        listFinal[w].RemoveAt(listFinal[w].size -1);
                        chazhi = chazhi - 1;
                    }
                }
            }
        }

    }

    #region
    private static  BetterList<BetterList<string>> listAlllist = new BetterList<BetterList<string>>();
    private static  BetterList<BetterList<string>> listFinallist = new BetterList<BetterList<string>>();
    private static readonly BetterList<int> splitindexlist = new BetterList<int>();
    private static  BetterList<string> listphoneformantlist = new BetterList<string>();

    private static readonly BetterList<string> singlelist = new BetterList<string>();
    private static readonly BetterList<BetterList<float>> PhoneStartTimeList = new BetterList<BetterList<float>>();
    private static readonly BetterList<BetterList<float>> PhoneEndTimeList = new BetterList<BetterList<float>>();
    //private static readonly BetterList<float> singleStartTimeList = new BetterList<float>();
    //private static readonly BetterList<float> singleEndTimeList = new BetterList<float>();

    private static readonly BetterList<float> StartTimeList = new BetterList<float>();
    private static readonly BetterList<float> EndTImeList = new BetterList<float>();
    private static BetterList<int> SplitIndexList = new BetterList<int>();
    private static readonly BetterList<int> ShengDiaoList = new BetterList<int>();
    private static BetterList<string> ShengmuList = new BetterList<string>();
    private static readonly BetterList<string> YummuList = new BetterList<string>();
    private static readonly string[] FORMANT =
    {
        "a", "e", "i", "ü", "u", "ai", "an", "ang", "ao", "ei", "en", "eng", "er", "ia", "ian", "iang", "iao", "ie",
        "in","ing","iong","iu","o","ong","ou","ua","uai","uan","üan","uang","üe","ueng","ui","un","ün","uo"
    };
    private static readonly string[] FORMANTENG =
    {
        "AA", "AE", "AH", "AO", "AW", "AY", "B", "CH", "D", "DH", "EH", "ER", "EY", "F", "G", "HH", "IH", "IY",
        "JH","K","L","M","N","NG","OW","OY","P","R","S","SH","T","TH","UH","UW","V","W","Y","Z","ZH"
    };
    private static BetterList<string> _formantList = new BetterList<string>{};
    private static bool _bSetFormantList = false;

    #endregion
}