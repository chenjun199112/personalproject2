﻿using Data;
using Extensions;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

public class ConfigManager : BaseObject
{
    private static ConfigManager instance;
    private Dictionary<uint, RoleData> roleDatas = new Dictionary<uint, RoleData>();

    public static ConfigManager Create()
    {
        if (instance == null)
        {
            instance = new ConfigManager();
        }
        return instance;
    }


    public override void Initialize()
    {
        //LoadNpcData();
    }


    /// <summary>
    /// 初始化NPC数据
    /// </summary>
    void LoadNpcData()
    {
        string tankDataPath = "datas/Npcs.xml";
        var asset = XmlHelper.LoadXml(tankDataPath);
        if (asset != null)
        {
            for (int i = 0; i < asset.Children.Count; i++)
            {
                var item = asset.Children[i] as SecurityElement;
                RoleData data = new RoleData();
                data.id = item.Attribute("id").ToUint();
                data.name = item.Attribute("name");
                data.nick = item.Attribute("nick");

                var scale_str = item.Attribute("scale");
                if (scale_str == null)
                {
                    scale_str = "1,1,1";
                }
                var scale = scale_str.Split(',');
                data.scale = new Vector3(scale[0].ToFloat(), scale[1].ToFloat(), scale[2].ToFloat());

                data.job = (JobType)item.Attribute("job").ToUint();
                var clips_str = item.Attribute("clips");
                if (clips_str != null)
                {
                    data.clips = clips_str.Split('_');
                }
                var skillid = item.Attribute("skill").ToUint();
                //data.skills = GetSkillData(skillid);
                roleDatas.Add(data.id, data);
            }
        }
    }

    public RoleData GetRoleData(uint roleid)
    {
        RoleData data = null;
        roleDatas.TryGetValue(roleid, out data);
        return data;
    }

    public override void OnUpdate(float deltaTime)
    {
    }

    public override void OnDispose()
    {
        throw new System.NotImplementedException();
    }
}