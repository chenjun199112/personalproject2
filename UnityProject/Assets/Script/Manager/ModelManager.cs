﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class ModelManager : BaseManager
    {
        static Dictionary<string, BaseModel> Models = new Dictionary<string, BaseModel>();

        public override void Initialize()
        {
            InitModel();

            isOnUpdate = true;
        }

        public override void OnUpdate(float deltaTime)
        {
            throw new System.NotImplementedException();
        }

        public override void OnDispose()
        {
            throw new System.NotImplementedException();
        }

        static void InitModel()
        {
            AddModel<MainModel>();

        }

        static T AddModel<T>() where T : BaseModel, new()
        {
            var type = typeof(T);
            var obj = new T();
            Models.Add(type.Name, obj);
            return obj;
        }

        public static T GetModel<T>() where T : class
        {
            var type = typeof(T);
            if (!Models.ContainsKey(type.Name))
            {
                return null;
            }
            return Models[type.Name] as T;
        }

        public static BaseModel GetModel(string modelName)
        {
            if (!Models.ContainsKey(modelName))
            {
                return null;
            }
            return Models[modelName];
        }

        public static bool RemoveModel<T>() where T : class
        {
            var type = typeof(T);
            if (!Models.ContainsKey(type.Name))
            {
                return false;
            }
            Models.Remove(type.Name);
            return true;
        }
    }
}

