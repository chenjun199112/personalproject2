﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class CtrlManager : BaseManager
    {
        static Dictionary<string, BaseCtrl> Ctrls = new Dictionary<string, BaseCtrl>();
        public override void Initialize()
        {
            InitCtrl();

            isOnUpdate = true;
        }

        public override void OnUpdate(float deltaTime)
        {
            
        }

        public override void OnDispose()
        {
            
        }

        static void InitCtrl()
        {
            AddCtrl<MainCtrl>();
            
        }

        static T AddCtrl<T>() where T : BaseCtrl, new()
        {
            var type = typeof(T);
            var obj = new T();
            Ctrls.Add(type.Name, obj);
            return obj;
        }

        public static T GetCtrl<T>() where T : class
        {
            var type = typeof(T);
            if (!Ctrls.ContainsKey(type.Name))
            {
                return null;
            }
            return Ctrls[type.Name] as T;
        }

        public static BaseCtrl GetCtrl(string ctrlName)
        {
            if (!Ctrls.ContainsKey(ctrlName))
            {
                return null;
            }
            return Ctrls[ctrlName];
        }

        public static bool RemoveCtrl<T>() where T : class
        {
            var type = typeof(T);
            if (!Ctrls.ContainsKey(type.Name))
            {
                return false;
            }
            Ctrls.Remove(type.Name);
            return true;
        }
    }
}



