﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class AppSceneManager : BaseManager, IScene
    {
        public string SceneName { get; protected set; }
        public bool IsShow { get; protected set; }

        public GameObject LightRoot;

        public Camera MainCamera;

        public static bool IsAsynDoingScene { get; private set; }

        public override void Initialize()
        {
            SceneName = "places";
            LoadScene(SceneName, LoadSceneMode.Single, OnLoadingEnd);

            isOnUpdate = true;
        }

        public override void OnUpdate(float deltaTime)
        {

        }

        public override void OnDispose()
        {

        }

        public void OnLoadingEnd()
        {

        }

        private static void LoadScene(string scene,
                                  LoadSceneMode mode,
                                  VoidDelegate endDelegate)
        {
            IsAsynDoingScene = true;
            Pump.Instance.SubscribeCoroutine(AsyncLoadScene(scene,
                                                            mode),
                                             endDelegate);
        }

        private static IEnumerator AsyncLoadScene(string scene,
                                              LoadSceneMode mode)
        {
            AsyncOperation asyn = SceneManager.LoadSceneAsync(scene,
                                                              mode);
            while (!asyn.isDone)
            {
                yield return Yielders.EndOfFrame;
            }
            IsAsynDoingScene = false;
        }

        public void Hide()
        {
            if (!IsShow)
            {
                return;
            }
            IsShow = false;
            if (LightRoot != null)
            {
                LightRoot.SetActive(false);
            }
            if (MainCamera != null)
            {
                MainCamera.gameObject.SetActive(false);
            }
        }

        public bool Show()
        {
            if (IsShow)
            {
                return true;
            }
            IsShow = true;

            if (LightRoot != null)
            {
                LightRoot.SetActive(true);
                //todo: 设置light
            }
            if (MainCamera != null)
            {
                MainCamera.gameObject.SetActive(true);
                //todo: 设置camera
            }

            return true;
        }
    }
}
