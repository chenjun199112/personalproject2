﻿using FairyGUI;
using UnityEngine;

public class TestFGUI: MonoBehaviour
{
    GComponent m_root;
    GTextInput m_inputAccount;
    GTextInput m_inputPwd;
    GButton m_confirmBtn;
    GButton m_cancelBtn;

    void Start()
    {
        //加载包
        UIPackage.AddPackage("UI/CommonPackage");
        UIPackage.AddPackage("UI/LoginPackage");

        //创建UIPanel
        UIPanel panel = gameObject.AddComponent<UIPanel>();
        panel.packageName = "LoginPackage";
        panel.componentName = "LoginPanel";
        //设置renderMode的方式
        panel.container.renderMode = RenderMode.ScreenSpaceOverlay;
        //设置fairyBatching的方式
        panel.container.fairyBatching = true;
        //设置sortingOrder的方式
        panel.SetSortingOrder(1, true);
        //设置hitTestMode的方式
        panel.SetHitTestMode(HitTestMode.Default);
        panel.fitScreen = FitScreen.FitSize;
        //最后，创建出UI
        panel.CreateUI();

        //根据FairyGUI中设置的名称找到对应的组件（注意输入框的查找）
        m_root = panel.ui;
        m_inputAccount = m_root.GetChild("account_input").asCom.GetChild("input").asTextInput;
        m_inputPwd = m_root.GetChild("password_input").asCom.GetChild("input").asTextInput;
        m_confirmBtn = m_root.GetChild("confirm_btn").asButton;
        m_cancelBtn = m_root.GetChild("cancel_btn").asButton;

        //密码框显示*号
        m_inputPwd.displayAsPassword = true;

        //添加点击事件
        m_confirmBtn.onClick.Add(OnClickConfirm);
    }

    void OnClickConfirm()
    {
        Debug.Log("account:" + m_inputAccount.text + "     pwd:" + m_inputPwd.text);
    }
}