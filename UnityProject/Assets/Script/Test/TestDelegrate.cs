﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TestDelegrate : MonoBehaviour
{
    // Start is called before the first frame update

    
    void Start()
    {
        Action myDele = new Action(delegate() { });
        for (int i =0;i <5;i++)
        {
            myDele += delegate ()
            {
                Debug.LogError(i);
            };
            
        }
        myDele();
    }

     Action<int> TestCreateActionInstance()
    {
        int count = 0;
        Action<int> action = delegate (int num)
         {
             count += num;
             Debug.Log(count);
         };
        action(1);
        return action;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
