﻿using UnityEngine;

//自定义测试Debug，支持开关
public class CJDebug
{
#if OpenDebug
    private static bool _bSwitch = true;
#else
    private static bool _bSwitch = false;
#endif
    private static string _prefix = "【CJ Debug】";

    public static void Log(object message)
    {
        if (_bSwitch)
            Debug.Log(_prefix + message);
    }

    public static void Log(object message, Object context)
    {
        if (_bSwitch)
            Debug.Log(_prefix + message, context);
    }

    public static void LogWarning(object message)
    {
        if (_bSwitch)
            Debug.LogWarning(_prefix + message);
    }

    public static void LogWarning(object message, Object context)
    {
        if (_bSwitch)
            Debug.LogWarning(_prefix + message, context);
    }

    public static void LogError(object message)
    {
        if (_bSwitch)
            Debug.LogError(_prefix + message);
    }

    public static void LogError(object message, Object context)
    {
        if (_bSwitch)
            Debug.LogError(_prefix + message, context);
    }

    public static void White(object message)
    {
        Log("<color=white>" + message + "</color>");
    }

    public static void Green(object message)
    {
        Log("<color=green>" + message + "</color>");
    }

    public static void Purple(object message)
    {
        Log("<color=#9400D3>" + message + "</color>");
    }

    public static void Yellow(object message)
    {
        Log("<color=yellow>" + message + "</color>");
    }

    public static void Red(object message)
    {
        Log("<color=red>" + message + "</color>");
    }

    public static void Blue(object message)
    {
        Log("<color=blue>" + message + "</color>");
    }

    public static void Magenta(object message)
    {
        Log("<color=magenta>" + message + "</color>");
        
    }
    public static void Gray(object message)
    {
        Log("<color=gray>" + message + "</color>");
    }

    public static void Cyan(object message)
    {
        Log("<color=cyan>" + message + "</color>");
    }

    

}
