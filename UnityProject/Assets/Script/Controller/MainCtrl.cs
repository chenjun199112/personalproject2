﻿using Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCtrl : BaseCtrl
{
    MainModel mainModel;
    public override void Initialize()
    {
        base.Initialize();
        mainModel = ModelManager.GetModel<MainModel>();
        uiName = "UI_Main";
        
        
    }
    public override void ShowUI(Action OnShowOK)
    {

    }

    public void ShowOK()
    { 

    }

}
