﻿
public interface IScene
{
    string SceneName { get; }
    bool IsShow { get; }
    void Hide();
    bool Show();
}